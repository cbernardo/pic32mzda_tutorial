/**
 * File : main.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Demonstrates a debug console I/O implementation
 *               based on a generic UART driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "console.h"
#include "events.h"
#include "sched_simple.h"
#include "startup.h"
#include "syserr.h"
#include "sysutil.h"


/* persistent task */
static void console_task(void);

/* soft scheduled task functions */
static unsigned int led_blink(void);
static unsigned int task50ms(void);
static unsigned int task250ms(void);
static unsigned int task500ms(void);


TaskPersistent_e g_tasks_persist[] =
{
    /* console */
    {
        .task = console_task,
        .name = "console",
        .min_execution_cycles = 0,
        .max_execution_cycles = 0
    },
    /* end marker; must have NULL task pointer */
    {
        .task = 0,
        .name = 0,
        .min_execution_cycles = 0,
        .max_execution_cycles = 0
    },
}; /* persistent task list */

/* soft scheduled tasks */
TaskSoftSched_e  g_tasks_soft[] =
{
    /* toggle the LED at 500ms intervals */
    {
        .task = led_blink,
        .name = "led",
        .min_execution_cycles = 0,
        .max_execution_cycles = 0,
        .max_latency_ms = 0,
        .interval_default = 500,
        .interval_counter = 0,
    },
    /* task executed at 50ms intervals */
    {
        .task = task50ms,
        .name = "task50ms",
        .min_execution_cycles = 0,
        .max_execution_cycles = 0,
        .max_latency_ms = 0,
        .interval_default = 50,
        .interval_counter = 50,
    },
    /* task executed at 250ms intervals */
    {
        .task = task250ms,
        .name = "task250ms",
        .min_execution_cycles = 0,
        .max_execution_cycles = 0,
        .max_latency_ms = 0,
        .interval_default = 250,
        .interval_counter = 250,
    },
    /* task executed at 500ms intervals */
    {
        .task = task500ms,
        .name = "task500ms",
        .min_execution_cycles = 0,
        .max_execution_cycles = 0,
        .max_latency_ms = 0,
        .interval_default = 500,
        .interval_counter = 500,
    },
    /* end marker; must have NULL task pointer */
    {
        .task = 0,
        .name = 0,
        .min_execution_cycles = 0,
        .max_execution_cycles = 0,
        .max_latency_ms = 0,
        .interval_default = 0,
        .interval_counter = 0,
    },
}; /* persistent task list */


/* counts number of times the persistent task has been invoked */
static unsigned int m_taskCounter;
static unsigned int m_counter50;
static unsigned int m_counter250;


int main(void)
{
    _on_start();        /* complete microprocessor init */
    Sys.Init();         /* initialize system utilities */
    schedule_init();    /* initialize the scheduler */

    while(1)
    {
        schedule();
    }

    __builtin_unreachable();
    return 0;
}

static void console_task(void)
{
    /* Read and echo any incoming data */
    char inbuf[200];
    int  length = (int) (sizeof(inbuf) - 1U);
    int  rxresult = Console.ReceiveString(inbuf, &length);

    if (0 == rxresult)
    {
        inbuf[length] = 0;      /* terminate the string */
        printf("%s", inbuf);    /* echo the data via the 'printf' function */
    }
    else if (ERR_AGAIN != rxresult)
    {
        Console.ClearError();
    }

    ++m_taskCounter;
    return;
}

static unsigned int led_blink(void)
{
    /* toggle the LED at the default rate */
    LATHINV = (1UL << 11);
    return 0;   /* 0 = use default interval for schedule */
}

/**
 * @function task50ms
 * @detail   On first invocation report the value of m_taskCounter;
 *
 * @return   0 (use default task interval)
 */
static unsigned int task50ms(void)
{
    static bool once = false;
    ++m_counter50;

    if (!once)
    {
        once = true;
        printf("* 50ms task: persistent task has run %u times\n", m_taskCounter);
    }

    return 0;   /* 0 = use default interval for schedule */
} /* task50ms */

/**
 * @function task250ms
 * @detail   On first invocation report the value of m_taskCounter
 *           and number of invocations of 50ms task, then print the
 *           schedule summary and delay 100ms to force a large
 *           latency on the 50ms task.
 *
 * @return   0 (use default task interval)
 */
static unsigned int task250ms(void)
{
    static bool once = false;
    ++m_counter250;

    if (!once)
    {
        once = true;
        printf("* 250ms task:\n");
        printf("  persistent task has run %u times\n", m_taskCounter);
        printf("  50ms task has run %u times\n", m_counter50);
        schedule_print_summary();
        Sys.WaitMillisec(100);
    }

    return 0;   /* 0 = use default interval for schedule */
} /* task250ms */

/**
 * @function task250ms
 * @detail   On first invocation report the value of m_taskCounter
 *           and number of invocations of 50ms and 250ms tasks,
 *           then print the scheduler summary.
 *
 * @return   0 (use default task interval)
 */
static unsigned int task500ms(void)
{
    static bool once = false;

    if (!once)
    {
        once = true;
        printf("* 500ms task:\n");
        printf("  persistent task has run %u times\n", m_taskCounter);
        printf("  50ms  task has run %u times\n", m_counter50);
        printf("  250ms task has run %u times\n", m_counter250);
        printf("  Note: summary is printed from within the 500ms task;\n");
        printf("        the 500ms task will be reported as never run.\n");
        schedule_print_summary();
    }

    return 0;   /* 0 = use default interval for schedule */
} /* task500ms */
