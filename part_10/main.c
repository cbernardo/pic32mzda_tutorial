/**
 * File : main.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Demonstrates a debug console I/O implementation
 *               based on a generic UART driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "startup.h"
#include "console.h"
#include "syserr.h"
#include "sysutil.h"
#include "memutil.h"

/* Declare an array in the cached memory region; the array is aligned to 16 bytes
 * since that is the boundary of a cache line.
 */
static char m_testArray[4096] __attribute__((__aligned__ (16)));

static char const m_msg1[] = "The quick brown fox jumps over the lazy dog.";
static char const m_msg2[] = "How much wood would a woodchuck chuck if a woodchuck could chuck wood?";

int main(void)
{
    _on_start();    /* complete microprocessor init */
    Sys.Init();     /* initialize system utilities */
    char     inbuf[200];
    int32_t  length;
    uint32_t timer = Sys.MillisecTime();

    /* note: printf invokes _mon_putc which in turn should automatically
     *       initialize the debug console for I/O.
     */
    printf("* Prefetch module configuration:\n");
    printf("  SEC interrupt: %s\n", PRECONbits.PFMSECEN ? "yes" : "no");
    printf("  enabled      : %s\n", PRECONbits.PREFEN ? "yes" : "no");
    printf("  wait states  : %d\n", PRECONbits.PFMWS);
    printf("* Prefetch module status:\n");
    printf("  double error detected : %s\n", PRESTATbits.PFMDED ? "yes" : "no");
    printf("  single error corrected: %s\n", PRESTATbits.PFMSEC ? "yes" : "no");
    printf("  single error counter  : %d\n\n", PRESTATbits.PFMSECCNT);

    /* fill non-cache text buffer with zero to avoid unexpected console text */
    char *cp = (char *)(Mem.GetNonCachedAddress(m_testArray));
    memset(cp, 0, sizeof(m_testArray));

    /* write and display a message in cache memory
     */
    printf("* Cache example 1: a write to non-cache memory is not seen in cache memory.\n");
    printf("  A cache flush will write data from the cache back to physical memory.\n");
    snprintf(m_testArray, sizeof(m_testArray), "%s", m_msg1);
    printf("  message written to cache memory    :\n  %s\n", m_testArray);
    printf("  message as seen by non-cache memory:\n  %s\n", cp);
    /* flush to cache */
    Mem.FlushRange(m_testArray, sizeof(m_testArray));
    printf("  flushed message in non-cache memory:\n  %s\n", cp);

    /* write and display a message in non-cache memory
     */
    printf("* Cache example 2: a write to cache memory is not seen in non-cache memory.\n");
    printf("  A cache evict will discard data currently in cache memory.\n");
    printf("  Upon access of cache memory, data will be reloaded from physical memory.\n");
    snprintf(cp, sizeof(m_testArray), "%s", m_msg2);
    printf("  message written to non-cache memory   :\n  %s\n", cp);
    printf("  message as seen by cache memory       :\n  %s\n", m_testArray);
    /* evict cache */
    Mem.EvictRange(m_testArray, sizeof(m_testArray));
    printf("  post-evict message in non-cache memory:\n  %s\n", m_testArray);

    while(1)
    {
        /* toggle the LED every 500 milliseconds */
        if (500 <= (Sys.MillisecTime() - timer))
        {
            timer = Sys.MillisecTime();
            LATHINV = (1UL << 11);
        }

        /* Read and echo any incoming data */
        length = 199;   /* take in up to 199 bytes and allow space for a zero terminator */
        int32_t rxresult = Console.ReceiveString(inbuf, &length);

        if (0 == rxresult)
        {
            inbuf[length] = 0;      /* terminate the string */
            printf("%s", inbuf);    /* echo the data via the 'printf' function */
        }
        else if (ERR_AGAIN != rxresult)
        {
            Console.ClearError();
        }
    }

    __builtin_unreachable();
    return 0;
}
