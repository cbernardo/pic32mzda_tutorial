/**
 * File : main.c
 * Copyright (C) 2023 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Demonstrates implementation of a serial command line.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "commandline.h"
#include "console.h"
#include "events.h"
#include "sched_simple.h"
#include "startup.h"
#include "syserr.h"
#include "sysutil.h"


/* soft scheduled task function */
static unsigned int    led_blink(void);
/* command examples */
static int             cmd_login(int const argc, char const **argv);
static int             cmd_secret(int const argc, char const **argv);
static CmdPermission_e cmdcon_secret(void);
static int             cmd_run(int const argc, char const **argv);
static CmdPermission_e cmdcon_run(void);

ConsoleCmd_t g_serial_commands[] =
{
    {
        .name = "login",
        .obscure = 1,
        .condition = 0,
        .function = cmd_login,      /* all text following 'login' should be obscured */
    },
    {
        .name = "defect",
        .obscure = 0,
        .condition = 0,
        .function = 0,              /* defective structure; no command function */
    },
    {
        .name = "secret",
        .obscure = 0,
        .condition = cmdcon_secret, /* function always returns CLI_NO_ACCESS */
        .function = cmd_secret,     /* the function should never be executed by the CLI */
    },
    {
        .name = "run",
        .obscure = 0,
        .condition = cmdcon_run,    /* function always returns CLI_NOT_AVAIL */
        .function = cmd_run,        /* the function should never be executed by the CLI */
    },
    /* end marker */
    {
        .name = 0,
        .obscure = 0,
        .condition = 0,
        .function = 0,
    },
};


TaskPersistent_e g_tasks_persist[] =
{
    /* console */
    {
        .task = cmd_task,
        .name = "cli",
        .min_execution_cycles = 0,
        .max_execution_cycles = 0
    },
    /* end marker; must have NULL task pointer */
    {
        .task = 0,
        .name = 0,
        .min_execution_cycles = 0,
        .max_execution_cycles = 0
    },
}; /* persistent task list */

/* soft scheduled tasks */
TaskSoftSched_e  g_tasks_soft[] =
{
    /* toggle the LED at 500ms intervals */
    {
        .task = led_blink,
        .name = "led",
        .min_execution_cycles = 0,
        .max_execution_cycles = 0,
        .max_latency_ms = 0,
        .interval_default = 500,
        .interval_counter = 0,
    },
    /* end marker; must have NULL task pointer */
    {
        .task = 0,
        .name = 0,
        .min_execution_cycles = 0,
        .max_execution_cycles = 0,
        .max_latency_ms = 0,
        .interval_default = 0,
        .interval_counter = 0,
    },
}; /* persistent task list */


int main(void)
{
    _on_start();        /* complete microprocessor init */
    Sys.Init();         /* initialize system utilities */
    schedule_init();    /* initialize the scheduler */

    while(1)
    {
        schedule();
    }

    __builtin_unreachable();
    return 0;
}

static unsigned int led_blink(void)
{
    /* toggle the LED at the default rate */
    LATHINV = (1UL << 11);
    return 0;   /* 0 = use default interval for schedule */
}

/**
 * @function cmd_login
 * @detail   Demonstrates obfuscation of command arguments.
 *
 * @return   0
 */
static int cmd_login(int const argc, char const **argv)
{
    if (1 == argc)
    {
        printf("* Usage:  login <password>.\n");
        printf("  Note: password should be obfuscated as you type.\n");
    }
    else if (1 < argc)
    {
        printf("* Your secret password is: %s\n", argv[1]);
    }

    return 0;
}

/**
 * @function cmd_secret
 * @detail   Demonstrates hiding of commands depending on condition.
 *
 * @return   -1
 */
static int cmd_secret(int const argc, char const **argv)
{
    (void) argc;
    (void) argv;
    printf("[bug] 'secret' command should not have been executed\n");
    return -1;
}

/**
 * @function cmdcon_secret
 * @detail   Always returns CLI_NO_ACCESS which should in turn
 *           result in a message that the command was not found.
 *
 * @return   CLI_NO_ACCESS
 */
static CmdPermission_e cmdcon_secret(void)
{
    printf("* [info] 'secret' command is hidden; expect a 'command not found' message\n");
    return CLI_NO_ACCESS;
}

/**
 * @function cmd_run
 * @detail   Demonstrates suppression of command when conditions not met.
 *
 * @return   -1
 */
static int cmd_run(int const argc, char const **argv)
{
    (void) argc;
    (void) argv;
    printf("[bug] 'run' command should not have been executed\n");
    return -1;
}

/**
 * @function cmdcon_run
 * @detail   Always returns CLI_NOT_AVAIL which should in turn
 *           prevent execution of cmd_run.
 *
 * @return   CLI_NOT_AVAIL
 */
static CmdPermission_e cmdcon_run(void)
{
    printf("* [info] 'run' command cannot currently be run (CLI_NOT_AVAIL)\n");
    return CLI_NOT_AVAIL;
}
