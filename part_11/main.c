/**
 * File : main.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Demonstrates a debug console I/O implementation
 *               based on a generic UART driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "console.h"
#include "events.h"
#include "startup.h"
#include "syserr.h"
#include "sysutil.h"


int main(void)
{
    _on_start();    /* complete microprocessor init */
    Sys.Init();     /* initialize system utilities */
    char     inbuf[200];
    int32_t  length;
    uint32_t timer = Sys.MillisecTime();

    /* note: printf invokes _mon_putc which in turn should automatically
     *       initialize the debug console for I/O.
     */
    printf("* event demo 1: set an event, print and flush, then display event data\n");
    Event.Set(0);
    printf("  <event processing is delayed as this message is written>\n");
    Console.TxFlush();

    if (Event.Test(0))
    {
        EventPerf_t const *perf = 0;
        int evtEntries = 0;
        Event.GetPerfData(&perf, &evtEntries);
        /* note: for a 200MHz system clock 1 cycle = 10ns;
         * to output in ns just add a '0' to the output string
         */
        printf("  <success> : event delayed for %u0 nanoseconds\n", perf->max_latency);
    }
    else
    {
        printf("  <failure> : event was mysteriously lost\n");
    }

    printf("* event demo 2: set an event twice, test then display event data\n");
    Event.Set(0);
    Event.Set(0);

    if (Event.Test(0))
    {
        EventPerf_t const *perf = 0;
        int evtEntries = 0;
        Event.GetPerfData(&perf, &evtEntries);
        /* note: for a 200MHz system clock 1 cycle = 10ns;
         * to output in ns just add a '0' to the output string
         */
        printf("  <success> note: latencies shown in nanoseconds\n");
        printf("  counts      : %u\n", perf->count);
        printf("  missed      : %u\n", perf->missed);
        printf("  min. latency: %u0\n", perf->min_latency);
        printf("  max. latency: %u0\n", perf->max_latency);
    }
    else
    {
        printf("  <failure> : event was mysteriously lost\n");
    }

    while(1)
    {
        /* toggle the LED every 500 milliseconds */
        if (500 <= (Sys.MillisecTime() - timer))
        {
            timer = Sys.MillisecTime();
            LATHINV = (1UL << 11);
        }

        /* Read and echo any incoming data */
        length = 199;   /* take in up to 199 bytes and allow space for a zero terminator */
        int32_t rxresult = Console.ReceiveString(inbuf, &length);

        if (0 == rxresult)
        {
            inbuf[length] = 0;      /* terminate the string */
            printf("%s", inbuf);    /* echo the data via the 'printf' function */
        }
        else if (ERR_AGAIN != rxresult)
        {
            Console.ClearError();
        }
    }

    __builtin_unreachable();
    return 0;
}
