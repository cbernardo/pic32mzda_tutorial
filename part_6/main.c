/**
 * File : main.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Implements the system timer and commonly used
 * system utilities. Demonstrates the use of the timer in making
 * the MEB2 LED5 blink on and off at a rate of 1Hz.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdint.h>
#include <stdio.h>

#include "startup.h"
#include "sysclk.h"
#include "timer.h"
#include "uart_simple.h"

static DrvTimer_t const *timer[9] =
{
    &Timer1,
    &Timer2,
    &Timer3,
    &Timer4,
    &Timer5,
    &Timer6,
    &Timer7,
    &Timer8,
    &Timer9,
};

static volatile int32_t timerIdx = 0;
static volatile int32_t timerCtr = 0;
static volatile bool    timerChange = true;

/* timer callback routine for blinking LED */
static void BlinkLED(void)
{
    static uint32_t counter = 0;
    ++counter;

    if (500 <= counter)
    {
        counter = 0;
        LATHINV = (1UL << 11);   /* toggle LED 5 (D7) */
        ++timerCtr;

        if (20 <= timerCtr)
        {
            timerChange = true;
            timer[timerIdx]->Enable(false);
            timer[timerIdx]->SetISR(0);
            ++timerIdx;

            if (8 < timerIdx)
            {
                timerIdx = 0;
            }
        }
    }

    return;
}

int main(void)
{
    _on_start();    /* complete microprocessor init */
    uart_init();    /* set up the UART for use by the printf() function */
    printf("\nDemonstration: using each of the 9 timers to blink the LED.\n");

    while(1)
    {
        if (timerChange)
        {
            /* Note: Demonstrate blinking LED on TimerX interrupt */
            printf("* Blink LED 5 using Timer %d interrupt\n", (timerIdx + 1));
            timerChange = false;
            timerCtr = 0;
            timer[timerIdx]->SetFrequency(1000UL);  /* millisecond timing */
            timer[timerIdx]->SetISR(BlinkLED);
            timer[timerIdx]->Enable(true);
        }
    }

    __builtin_unreachable();
    return 0;
}
