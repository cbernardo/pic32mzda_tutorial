06 Sep 2020

This project is intended to provide a learning resource for hobbyists
interested in learning embedded programming. The target processor is
the Microchip PIC32MZ2064DAH169 which is obsoleted but compatible with
the current PIC32MZ2064DAS169. For the moment the intended board is the
PIC32 Development Kit DM320010-C (PIC32 with stacked DRAM and
cryptography hardware). The DM320010 (no cryptography hardware) can also
be used. For those using the DM320008 or DM320008-C the DDR driver may
require modifications to suit the timing requirements of the Micron
DDR2-SDRAM (MT47H64M16NF-25E).

The PIC32MZ-DA family of processors were chosen due to the relative
simplicity of the microprocessor which makes it suitable for teaching
the basics of programming a microprocessor.  The tutorials will proceed
with simple tasks and slowly build up the framework needed to support
a typical bare metal embedded development project. In the later
tutorials the reader will be introduced to the internationalization
framework and ultimately a graphical system with touch control.
