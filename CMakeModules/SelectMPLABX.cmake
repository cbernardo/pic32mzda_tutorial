################################################################################
# File: SelectMPLABX.cmake
#
# Description: Selects build tools from available MPLABX installations.
#
# Input parameters:
#   PREF_MPLAB : [optional] Version number for MPLABX (example: 5.25)
#
#   If an MPLABX version is specified then that version will be used,
#   otherwise the latest tool found is used. Multiple tool versions may be
#   specified in order of preference using a semicolon separated list.
#
#   Variables set on exit:
#   1. CFG_VERSION_MPLABX  : selected MPLABX version
#   2. CFG_PATH_HEXMATE   : path to Microchip hexmate tool
#   3. CMAKE_MAKE_PROGRAM : path to GNU make

# function: select_mplab
# searches the given path to select an installed version of MPLABX
function (select_mplab INSTALL_PATH_MCHP)
	if ((NOT EXISTS "${INSTALL_PATH_MCHP}") OR (NOT IS_DIRECTORY "${INSTALL_PATH_MCHP}"))
		return ()
	endif ()

	set (DIR_BIN_GNU "gnuBins/GnuWin32/bin")
	set (DIR_BIN_MCHP "mplab_platform/bin")

	# Find all versioned MPLABX installations
	file(GLOB MPLAB_VERSIONS
		RELATIVE "${INSTALL_PATH_MCHP}/mplabx"
		"${INSTALL_PATH_MCHP}/mplabx/v[0-9]\.[0-9]*?"
	)

	# Find GNU MAKE in MPLABX installation
	unset (MPLAB_INSTALLED_VERSION)
	unset (CFG_VERSION_MPLABX)

	if (NOT DEFINED PREF_MPLAB)
		# Select the last MPLABX version in the list
		if (NOT("${MPLAB_VERSIONS}" STREQUAL ""))
			# Installation path is versioned
			list(LENGTH MPLAB_VERSIONS VLEN)
			math(EXPR VLEN "${VLEN} - 1")
			list(GET MPLAB_VERSIONS ${VLEN} CFG_VERSION_MPLABX)
			set (CFG_VERSION_MPLABX ${CFG_VERSION_MPLABX} PARENT_SCOPE)
			set (MPLAB_INSTALLED_VERSION "${INSTALL_PATH_MCHP}/mplabx/${CFG_VERSION_MPLABX}")
		endif ()
	else ()
		# Select from a list of preferred MPLABX versions
		if (NOT("${MPLAB_VERSIONS}" STREQUAL "") AND NOT ("${PREF_MPLAB}" STREQUAL ""))
			foreach (PREF_VER ${PREF_MPLAB})
				string (CONCAT PREF_VER "v" "${PREF_VER}")
				foreach (MPLAB_CANDIDATE ${MPLAB_VERSIONS})
					if ("${MPLAB_CANDIDATE}" STREQUAL "${PREF_VER}")
						set (CFG_VERSION_MPLABX "${MPLAB_CANDIDATE}" PARENT_SCOPE)
						set (MPLAB_INSTALLED_VERSION "${INSTALL_PATH_MCHP}/mplabx/${MPLAB_CANDIDATE}")
						break()
					endif()
				endforeach ()
				if (MPLAB_INSTALLED_VERSION)
					break()
				endif()
			endforeach ()
		endif ()
	endif ()

	if (NOT DEFINED MPLAB_INSTALLED_VERSION OR NOT EXISTS "${MPLAB_INSTALLED_VERSION}" OR NOT IS_DIRECTORY "${MPLAB_INSTALLED_VERSION}")
		# No suitable MPLABX found
		return ()
	endif ()

	if (CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")
		find_program (CMAKE_MAKE_PROGRAM make PATHS "${MPLAB_INSTALLED_VERSION}" PATH_SUFFIXES "${DIR_BIN_GNU}" NO_DEFAULT_PATH)
	elseif ((CMAKE_HOST_SYSTEM_NAME STREQUAL "Linux") AND (NOT DEFINED CMAKE_MAKE_PROGRAM))
		# Assume 'make' exists and is accessible on Linux
		set (CMAKE_MAKE_PROGRAM "make" CACHE)
	endif ()

	if ("${CMAKE_MAKE_PROGRAM}" STREQUAL "CMAKE_MAKE_PROGRAM-NOTFOUND")
		# MPLABX GNU make not found
		unset (CMAKE_MAKE_PROGRAM CACHE)
		return ()
	endif ()

	# Find the Microchip hexmate tool
	find_program (CFG_PATH_HEXMATE hexmate PATHS "${MPLAB_INSTALLED_VERSION}" PATH_SUFFIXES mplab_platform/bin NO_DEFAULT_PATH)

	if ("${CFG_PATH_HEXMATE}" STREQUAL "CFG_PATH_HEXMATE-NOTFOUND")
		# MPLABX hexmate tool not found
		unset (CFG_PATH_HEXMATE CACHE)
		return ()
	endif()

endfunction ()
# end : select_mplab()

# Find required build tools
unset (CMAKE_MAKE_PROGRAM CACHE)
unset (CFG_VERSION_MPLABX CACHE)
unset (CFG_PATH_HEXMATE CACHE)

if (CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")
	if ("$ENV{PROCESSOR_ARCHITECTURE}" STREQUAL "x86")
		# 32-bit build machine
		select_mplab ("$ENV{ProgramFiles}/Microchip")
	elseif ("$ENV{PROCESSOR_ARCHITECTURE}" STREQUAL "AMD64")
		# 64-bit build machine
		# search the 64-bit program path first
		select_mplab ("$ENV{ProgramFiles}/Microchip")

		if ((NOT DEFINED CMAKE_MAKE_PROGRAM) OR (NOT DEFINED CFG_PATH_HEXMATE))
			# no tools found; search the 32-bit path
			unset (CMAKE_MAKE_PROGRAM CACHE)
			unset (CFG_PATH_HEXMATE CACHE)
			select_mplab ("$ENV{ProgramFiles\(x86\)}/Microchip")
		endif ()
		set (INSTALL_PATH_MCHP)
	endif ()
elseif (CMAKE_HOST_SYSTEM_NAME STREQUAL "Linux")
	select_mplab ("/opt/microchip")
else()
	message (FATAL_ERROR Unsupported host)
endif()

if (NOT DEFINED CMAKE_MAKE_PROGRAM)
	message ( FATAL_ERROR "GNU make not found" )
endif ()

if (NOT DEFINED CFG_PATH_HEXMATE)
	message (FATAL_ERROR "hexmate tool not found")
endif()

message (STATUS "Selected MPLABX version: ${CFG_VERSION_MPLABX}")
