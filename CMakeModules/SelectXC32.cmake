################################################################################
#   Set up PIC32 toolchain
#
#   Input parameters:
#   PREF_XC32  : [optional] Version number for XC32 compiler (example: 2.41)
#
#   If a compiler version is specified then that version will be used,
#   otherwise the latest found will be used. Multiple versions may be
#   specified in order of preference using a semicolon separated list.
#
#   Variables set on exit:
#   1. CFG_VERSION_XC32   : version string of selected XC32 compiler
#   2. CFG_PATH_BIN2HEX	  : path to XC32 compiler suite bin2hex tool
#

# Set up the build environment (toolchain)

# Find installed XC32 compilers
# Caveat: GLOB expressions differ from REGEX, in particular ? == [a-z]?
function (select_xc32 INSTALL_PATH_MCHP)
	file(GLOB LIST_XC32
		RELATIVE "${INSTALL_PATH_MCHP}/xc32"
		"${INSTALL_PATH_MCHP}/xc32/v[0-9]\.[0-9]*?"
	)

	unset (XC32_INSTALL_DIR)
	unset (CFG_VERSION_XC32)
	if (NOT DEFINED PREF_XC32)
		# Select the latest version or else an unversioned installation
		if (NOT("${LIST_XC32}" STREQUAL ""))
			list(LENGTH LIST_XC32 NUM_XC32)
			math(EXPR NUM_XC32 "${NUM_XC32} - 1")
			list(GET LIST_XC32 ${NUM_XC32} CFG_VERSION_XC32)
			set (CFG_VERSION_XC32 "${CFG_VERSION_XC32}" PARENT_SCOPE)
			set (XC32_INSTALL_DIR "${INSTALL_PATH_MCHP}/xc32/${CFG_VERSION_XC32}")
		endif ()
	else ()
		# Note: a versioned path is required to support version selection
		if (NOT("${LIST_XC32}" STREQUAL ""))
			foreach (PREF_VER ${PREF_XC32})
				string (CONCAT PREF_VER "v" "${PREF_VER}")
				foreach (XC32_CANDIDATE ${LIST_XC32})
					if ("${XC32_CANDIDATE}" STREQUAL "${PREF_VER}")
						set (CFG_VERSION_XC32 "${XC32_CANDIDATE}" PARENT_SCOPE)
						set (XC32_INSTALL_DIR "${INSTALL_PATH_MCHP}/xc32/${XC32_CANDIDATE}")
						break()
					endif()
				endforeach ()
				if (DEFINED XC32_INSTALL_DIR)
					break()
				endif()
			endforeach ()
		endif ()
	endif ()

	find_program (CMAKE_C_COMPILER xc32-gcc PATHS "${XC32_INSTALL_DIR}" PATH_SUFFIXES "bin" NO_DEFAULT_PATH)

	if ("${CMAKE_C_COMPILER}" STREQUAL "CMAKE_C_COMPILER-NOTFOUND")
		return ()
	endif ()

	# Find the bin2hex tool:
	find_program (CFG_PATH_BIN2HEX xc32-bin2hex PATHS "${XC32_INSTALL_DIR}" PATH_SUFFIXES "bin" NO_DEFAULT_PATH)

	if ("${CFG_PATH_BIN2HEX}" STREQUAL "CFG_PATH_BIN2HEX-NOTFOUND")
		unset (CFG_PATH_BIN2HEX CACHE)
	else ()
	    set (TOOLCHAIN_ROOT "${XC32_INSTALL_DIR}/bin" PARENT_SCOPE)
	endif()

endfunction()

# Check possible installation paths for XC32 and bin2hex
unset (CFG_PATH_BIN2HEX CACHE)
unset (CFG_VERSION_XC32 CACHE)

if (CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")
	if ("$ENV{PROCESSOR_ARCHITECTURE}" STREQUAL "x86")
		# 32-bit build machine
		select_xc32 ("$ENV{ProgramFiles}/Microchip")
	elseif ("$ENV{PROCESSOR_ARCHITECTURE}" STREQUAL "AMD64")
		# 64-bit build machine; search the 64-bit program path first
		select_xc32 ("$ENV{ProgramFiles}/Microchip")

		if ((NOT DEFINED CFG_VERSION_XC32) OR (NOT DEFINED CFG_PATH_BIN2HEX))
			# no tools found; search the 32-bit path
			select_xc32 ("$ENV{ProgramFiles\(x86\)}/Microchip")
		endif ()
	endif ()
elseif (CMAKE_HOST_SYSTEM_NAME STREQUAL "Linux")
	select_mplab ("/opt/microchip")
else()
	message (FATAL_ERROR Unsupported host)
endif()

if (NOT DEFINED CFG_PATH_BIN2HEX)
	message (FATAL_ERROR "bin2hex tool not found")
endif()

if (NOT DEFINED CFG_VERSION_XC32)
	message (FATAL_ERROR "No suitable XC32 found")
endif()

message (STATUS "Selected XC32 version: ${CFG_VERSION_XC32}")

set_property(GLOBAL PROPERTY TARGET_SUPPORTS_SHARED_LIBS FALSE)
set (CMAKE_SYSTEM_NAME      Generic)
set (CMAKE_SYSTEM_PROCESSOR PIC32)
set (CMAKE_FIND_ROOT_PATH "${TOOLCHAIN_ROOT}")
set (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set (CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
