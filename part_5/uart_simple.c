/**
 * File : uart_simple.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : simple UART transmitter implementing functions required by printf().
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#include "uart_simple.h"

/*******************************************************************************
 *              EXTERNAL FUNCTION DECLARATIONS
 ******************************************************************************/
void _mon_putc(char c);             /* required for printf() */


/*******************************************************************************
 *              PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/
static void uart_putc(char c);      /* transmit a single character */


/* @function _mon_putc
 * @detail   transmits a single character. End of line translations are performed;
 *           '\n' is translated to "\r\n". This operation blocks until the character
 *           is queued for transmission.
 */
void _mon_putc(char c)
{
    static bool has_lf = false; /* true if preceeding char was '\r' */

    if ('\n' == c)
    {
        if (!has_lf)
        {
            uart_putc('\r');
            uart_putc(c);
        }

        has_lf = false;
    }
    else if ('\r' == c)
    {
        has_lf = true;
        uart_putc(c);
        uart_putc('\n');
    }
    else
    {
        uart_putc(c);
    }

    return;
}


/* @function uart_putc
 * @detail   transmits a single character without end-of-line conversions.
 *           This operation blocks until at least one byte is available in
 *           the transmitter FIFO.
 */
static void uart_putc(char c)
{
    while (U2STAbits.UTXBF)
    {
        ;   /* wait for space on the tx buffer */
    }

    U2TXREG = (uint32_t)c;
    return;
}


/* @function uart_init
 * @detail   initializes the UART for operation at 115200bps,8N1; assumes 100MHz PBClk
 */
void  uart_init(void)
{
    while ((U2MODEbits.ON) && (U2MODEbits.ACTIVE))
    {
        ;   /* wait until the UART is idle */
    }

    U2MODE = 0;
    U2BRG = 53;                 /* Target speed: 115200; actual is 115740.741 (+0.47%) */
    U2STAbits.UTXEN = 1;        /* Enable transmitter */
    U2MODEbits.ON = 1;          /* Enable UART */
    return;
}


/* @function uart_txflush
 * @detail   waits until the UART transmit shift register is empty.
 */
void  uart_txflush(void)
{
    while (1 != U2STAbits.TRMT)
    {
        ;
    }

    return;
}
