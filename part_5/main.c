/**
 * File : main.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Implements the RefClk driver and shows how the
 * frequency can be changed. The frequency cannot be independently
 * verified via an oscilloscope since the RefClk4 output pin is not
 * selected or enabled. Occasionally toggles the MEB2 LED5 to indicate
 * activity.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdint.h>
#include <stdio.h>

#include "startup.h"
#include "uart_simple.h"
#include "sysclk.h"
#include "pbclk.h"
#include "refclk.h"

/**
 * @function calc_reflck_freq
 *
 * @param    srcFreq [IN] is the source frequency, 1..252000000.
 * @param    div     [IN] is the RODIV value, 0..32767.
 * @param    trim    [IN] is the ROTRIM value, 0..511.
 * @return   reference clock output frequency rounded to nearest integer.
 */
static uint32_t calc_reflck_freq(uint32_t srcFreq, uint32_t div, uint32_t trim);
static uint32_t calc_reflck_freq(uint32_t srcFreq, uint32_t div, uint32_t trim)
{
    uint32_t result = 0;

    /* notes:
     * 1. datasheet does not specify the upper limit to the RefClk input and
     * output frequencies; the only output frequency limit stated is for the external
     * output pins.
     * 2. If div = 0, output freq. = source freq.
     * 3. If trim = 0, output freq. = (source freq.) / (2 * div)
     * 4. In all other cases, output freq. = (source freq.) / (2 * (div + (trim / 512)))
     */
    if ((0 < srcFreq) && (252000000 >= srcFreq) && (512 > trim) && (((1UL << 15) - 1UL) >= div))
    {
        if (0 == div)
        {
            result = srcFreq;
        }
        else if (0 == trim)
        {
            result = (srcFreq + div) / (div << 1);
        }
        else
        {
            uint64_t tmpf = ((uint64_t) srcFreq) << 9;
            uint64_t tmpd = (((uint64_t) div) << 9) + (uint64_t)trim;
            tmpf += tmpd;
            tmpd <<= 1;
            result = (uint32_t)(tmpf / tmpd);
        }
    }

    return result;
}

/**
 * @function check_refck
 * @detail   Sets the RODIV and ROTRIM registers of RefClk4 and displays the
 *           expected frequency rounded to the nearest integer.
 *
 * @param    srcFreq [IN] is the source frequency, 1..252000000.
 * @param    div     [IN] is the RODIV value, 0..32767.
 * @param    trim    [IN] is the ROTRIM value, 0..511.
 * @return   reference clock output frequency rounded to nearest integer.
 */
static void check_refck(uint32_t div, uint32_t trim);
static void check_refck(uint32_t div, uint32_t trim)
{
    RefClk4.SetDivisor(div, trim);
    printf("* check_refck(%d, %d) -> freq = %d\n", div, trim, RefClk4.GetFrequency());
    uart_txflush();
    return;
}

int main(void)
{
    _on_start();    /* complete microprocessor init */
    uart_init();    /* set up the UART for use by the printf() function */
    printf("\n\n* SYSCLK frequency is %lu MHz.\n", (long unsigned int)SysClk.GetFrequency());
    printf("* PBCLK1 frequency is %d.\n", PbClk1.GetFrequency());
    uart_txflush();

    /* Note: Demonstrate REFCLK driver on REFCLK4 (nominally used by SDHC module) */
    printf("* REFCLK4 tests with PBCLK1 source\n");
    uart_txflush();

    RefClk4.SetSourceClk(REFCLK_SRC_PB1);
    printf("* REFCLK4 Test #1, expecting frequency = %d\n",
            calc_reflck_freq(PbClk1.GetFrequency(), 0, 0));
    check_refck(0, 0);

    printf("* REFCLK4 Test #2, expecting frequency = %d\n",
            calc_reflck_freq(PbClk1.GetFrequency(), 1, 256));
    check_refck(1, 256);

    printf("* REFCLK4 Test #3, expecting frequency = %d\n",
            calc_reflck_freq(PbClk1.GetFrequency(), 32767, 511));
    check_refck(32767, 511);

    do
    {
        int32_t i;

        for (i = 0; i < 50000000; ++i)
        {
            ; /* waste time */
        }

        LATHINV = (1UL << 11);   /* toggle LED 5 (D7) */
    } while (1);

    while(1)
    {
        ; /* do nothing */
    }

    __builtin_unreachable();
    return 0;
}
