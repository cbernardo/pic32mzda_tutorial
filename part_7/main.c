/**
 * File : main.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Implements the system timer and commonly used
 * system utilities. Demonstrates the use of the timer in making
 * the MEB2 LED5 blink on and off at a rate of 1Hz.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdint.h>
#include <stdio.h>

#include "startup.h"
#include "sysutil.h"
#include "uart_simple.h"


int main(void)
{
    _on_start();    /* complete microprocessor init */
    Sys.Init();     /* initialize system utilities */
    uart_init();    /* set up the UART for use by the printf() function */
    uint32_t timer500ms = Sys.MillisecTime();
    uint32_t lapsedCycles = Sys.CycleCounter();
    uint32_t lapsedMillisec = Sys.MillisecTime();
    printf("\nDemonstration: cycle counter and millisecond timer.\n");
    uart_txflush();
    lapsedCycles = Sys.CycleCounter() - lapsedCycles;
    lapsedMillisec = Sys.MillisecTime() - lapsedMillisec;
    printf("* it took %d milliseconds (%d cycles) to print the previous message\n",
            lapsedMillisec, lapsedCycles);
    printf("* Now blinking LED 5 at a rate of once per second.\n");

    while(1)
    {
        if (Sys.TimeoutMillisec(timer500ms, 500))
        {
            /* 500ms has elapsed; reset the start time and toggle the LED */
            timer500ms = Sys.MillisecTime();
            LATHINV = (1UL << 11);
        }
    }

    __builtin_unreachable();
    return 0;
}
