/**
 * File : main.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Demonstrates a debug console I/O implementation
 *               based on a generic UART driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "startup.h"
#include "console.h"
#include "syserr.h"
#include "sysutil.h"

int main(void)
{
    _on_start();    /* complete microprocessor init */
    Sys.Init();     /* initialize system utilities */
    char     inbuf[200];
    int      length;
    SYSTICK  timer = Sys.MillisecTime();

    /* note: printf invokes _mon_putc which in turn should automatically
     *       initialize the debug console for I/O.
     */
    printf("* This message is brought to you by the\n"
           "  new and improved UART and Console drivers.\n\n");

    while(1)
    {
        /* toggle the LED every 500 milliseconds */
        if (500 <= (Sys.MillisecTime() - timer))
        {
            timer = Sys.MillisecTime();
            LATHINV = (1UL << 11);
        }

        /* Read and echo any incoming data */
        length = 199;   /* take in up to 199 bytes and allow space for a zero terminator */
        int rxresult = Console.ReceiveString(inbuf, &length);

        if (0 == rxresult)
        {
            inbuf[length] = 0;      /* terminate the string */
            printf("%s", inbuf);    /* echo the data via the 'printf' function */
        }
        else if (ERR_AGAIN != rxresult)
        {
            //printf("UART Rx error: %d\n", rxresult);
            Console.ClearError();
        }
    }

    __builtin_unreachable();
    return 0;
}
