/**
 * File : sysclk.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides control of the System Clock.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* Note:
 * For the moment the clock shall only report the current operating frequency;
 * frequency and clock source changes may be implemented in the future if
 * required for lower power standby modes.
 */

#ifndef DRV_SYSCLK_H
#define DRV_SYSCLK_H

typedef struct
{
    /**
     * @function GetFrequency
     * @detail   Returns the system clock frequency in Hertz. In the event of an unhandled
     *           configuration setting the return value will be 0.
     */
    uint32_t  (*GetFrequency)(void);

    /**
     * @function GetSPLLFrequency
     * @detail   Returns the System PLL Frequency in Hertz. In the event of an unhandled
     *           configuration setting the return value will be 0.
     */
    uint32_t  (*GetSPLLFrequency)(void);
} const DrvSysClk_t;

extern DrvSysClk_t SysClk;

#endif /* DRV_SYSCLK_H */
