/**
 * File : events.c
 * Copyright (C) 2022 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Defines the EVENT signaling system.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <limits.h>
#include <stdint.h>

#include "events.h"
#include "sysutil.h"


/*******************************************************************************
 *          PRIVATE VARIABLES
 ******************************************************************************/
static unsigned int m_events[(EVT_END_MARKER + 31) / 32];   /* assume 32-bit registers */

#if defined(CFG_PERF_EVENTS) && (1 == (CFG_PERF_EVENTS))
static unsigned int m_setTime[EVT_END_MARKER];  /* core counter at set() */
static EventPerf_t  m_perf[EVT_END_MARKER];
#endif


/*******************************************************************************
 *          PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/
void evt_set(int const event);
bool evt_test(int const event);
bool evt_peek(int const event);
#if defined(CFG_PERF_EVENTS) && (1 == (CFG_PERF_EVENTS))
void evt_get_perf_data(EventPerf_t const **performanceData, int *numberOfEntries);
void evt_init_perf_data(void);
#endif


/*******************************************************************************
 *              API
 ******************************************************************************/
SysEvent_t const Event =
{
    .Set = evt_set,
    .Test = evt_test,
    .Peek = evt_peek,
    #if defined(CFG_PERF_EVENTS) && (1 == (CFG_PERF_EVENTS))
    .GetPerfData = evt_get_perf_data,
    #endif
};


/*******************************************************************************
 *          PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/

void evt_set(int const event)
{
    if ((0 <= event) && (EVT_END_MARKER > event))
    {
        int idx = (event >> 5);     /* index into m_events */
        unsigned int bitval = (1UL << (event & 31));
        unsigned int intStat = __builtin_disable_interrupts();

        #if defined(CFG_PERF_EVENTS) && (1 == (CFG_PERF_EVENTS))
        m_setTime[idx] = Sys.CycleCounter();
        ++m_perf[idx].count;

        if (m_events[idx] & bitval)
        {
            ++m_perf[idx].missed;
        }
        #endif

        m_events[idx] |= bitval;

        if (intStat)
        {
            __builtin_enable_interrupts();
        }
    }

    return;
} /* evt_set */


bool evt_test(int const event)
{
    bool result = false;

    if ((0 <= event) && (EVT_END_MARKER > event))
    {
        int idx = (event >> 5);     /* index into m_events */
        unsigned int bitval = (1UL << (event & 31));
        unsigned int intStat = __builtin_disable_interrupts();

        if (m_events[idx] & bitval)
        {
            m_events[idx] &= (~bitval);
            result = true;

            #if defined(CFG_PERF_EVENTS) && (1 == (CFG_PERF_EVENTS))
            unsigned int cycleCount = Sys.CycleCounter() - m_setTime[idx];

            if (1 == m_perf[idx].count)
            {
                m_perf[idx].min_latency = cycleCount;
                m_perf[idx].max_latency = cycleCount;
            }
            else if (cycleCount > m_perf[idx].max_latency)
            {
                m_perf[idx].max_latency = cycleCount;
            }
            else if (cycleCount < m_perf[idx].min_latency)
            {
                m_perf[idx].min_latency = cycleCount;
            }
            #endif
        }

        if (intStat)
        {
            __builtin_enable_interrupts();
        }
    }

    return result;
} /* evt_test */


bool evt_peek(int const event)
{
    bool result = false;

    if ((0 <= event) && (EVT_END_MARKER > event))
    {
        int idx = (event >> 5);     /* index into m_events */
        unsigned int bitval = (1UL << (event & 31));

        if (m_events[idx] & bitval)
        {
            result = true;
        }
    }

    return result;
} /* evt_peek */


#if defined(CFG_PERF_EVENTS) && (1 == (CFG_PERF_EVENTS))
void evt_get_perf_data(EventPerf_t const **performanceData, int *numberOfEntries)
{
    if (0 != performanceData)
    {
        *performanceData = m_perf;
    }

    if (0 != numberOfEntries)
    {
        *numberOfEntries = EVT_END_MARKER;
    }

    return;
} /* evt_get_perf_data */
#endif
