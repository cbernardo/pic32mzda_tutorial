/**
 * File : cp0_config.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides Microprocessor Configuration settings.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

/* Halt compilation if the processor is not supported */
#if !defined(__32MZ2064DAH169__)
#error Unsupported processor
#endif

/* Device Configuration Registers */
/* DEVCFG0 */
#pragma config EJTAGBEN = NORMAL        /* JTAG Boot Enable */
#pragma config POSCAGC = OFF            /* Disable POsc Auto Gain (non-functional) */
#pragma config POSCTYPE = CRYSTAL_12MHZ	/* CRYSTAL_12MHZ (default, not used) */
#pragma config POSCBOOST = OFF          /* OFF: not required; POsc is a clock */
#pragma config POSCGAIN = GAIN_LEVEL_0  /* POSCGAIN not used */
#pragma config SOSCBOOST = OFF          /* OFF: SOsc is not used */
#pragma config SOSCGAIN = GAIN_LEVEL_0	/* SOSCGAIN not used */
#pragma config SMCLR = MCLR_NORM        /* MCLR_NORM: #MCLR generates a normal reset */
#pragma config DBGPER = ALLOW_PG2       /* ALLOW_PG2: debugger has PG2 access level */
#pragma config FSLEEP = OFF             /* OFF: Flash is powered down in sleep mode */
#pragma config FECCCON = DYNAMIC        /* Dynamic Flash ECC enabled */

/* Note on Instruction Set Architecture:
 * Some care must be taken when using MICROMIPS; be sure that any libraries and builtin
 * functions used and the compiler's Interrupt Prolog/Epilog are also compiled for MICROMIPS.
 */
#ifdef __mips_micromips
#pragma config BOOTISA = MICROMIPS      /* MIPS32: Boot and ISR code is microMIPS */
#else
#pragma config BOOTISA = MIPS32         /* MIPS32: Boot and ISR code is MIPS32 */
#endif

#pragma config TRCEN = OFF              /* CPU trace features disabled */
#pragma config ICESEL = ICS_PGx2        /* Programmer uses pin set 2 */
#pragma config JTAGEN = OFF             /* disable JTAG and prevent modification */
#pragma config DEBUG = ON               /* enable background debug (forced off if CP is enabled) */

/* DEVCFG1 */
#pragma config FDMTEN = OFF             /* Deadman Timer Enable */
#pragma config DMTCNT = DMT31           /* DMT31 = maximum delay, Dead Man Timer */
#pragma config FWDTWINSZ = WINSZ_25     /* Watchdog Timer Window Size (25%) */
#pragma config FWDTEN = OFF             /* Watchdog Timer Enable */
#pragma config WINDIS = NORMAL          /* NORMAL: WDT in non-windowed mode */
#pragma config WDTSPGM = STOP           /* Stop WDT during programming */
#pragma config WDTPS = PS1048576        /* PS1048576 = maximum delay, Watch Dog Timer */
#pragma config FCKSM = CSECME           /* Clock Switch and Clock Monitoring enabled */
#pragma config OSCIOFNC = OFF           /* no clock output on CLKO */
#pragma config POSCMOD = EC             /* Primary Oscillator = External Clock */
#pragma config IESO = ON                /* Internal/External Switch Over */
#pragma config FSOSCEN = OFF            /* Secondary Oscillator Disabled */
#pragma config DMTINTV = WIN_127_128    /* DMT Count Window Interval = MAX */
#pragma config FNOSC = SPLL             /* Oscillator Source = SPLL */

/* DEVCFG2 */
#pragma config UPLLFSEL = FREQ_24MHZ    /* USB PLL Input Frequency Selection; 12 or 24MHz */
#pragma config FDSEN = OFF              /* OFF: WAIT instruction enters normal (not DEEP) SLEEP */
#pragma config DSWDTEN = OFF            /* Deep Sleep WDT Disabled */
#pragma config DSWDTOSC = LPRC          /* DS WDT clock source */
#pragma config DSWDTPS = DSPS32         /* DS WDT period: DSPS32 = max. delay */
#pragma config DSBOREN = OFF            /* Disable BOR in DS mode */
#pragma config VBATBOREN = OFF          /* Disable BOR in VBat mode */
/* PLL : configured for 200MHz operation; low-power mode will not be implemented
 *       PLL input range (Fin): (ICLK / IDIV) 50MHz to 64 MHz; lower frequencies are preferred
 *       VCO range (Fvco): (Fin * MUL) 350MHz to 700MHz
 *       PLL output range (Fpll): (Fvco / ODIV) 10MHz to 200MHz
 *       Settings: (24MHz / 3) * 50 / 2 = 200MHz
 */
#pragma config FPLLICLK = PLL_POSC      /* System PLL Input Clock = Primary Osc (24MHz) */
#pragma config FPLLIDIV = DIV_3         /* Divide PLL input by 3 */
#pragma config FPLLRNG = RANGE_5_10_MHZ /* System PLL Input Range (5-10 MHz Input) */
#pragma config FPLLMULT = MUL_50        /* System PLL Multiplier = 50 */
#pragma config FPLLODIV = DIV_2         /* Divide PLL out by 2 */

/* DEVCFG3 */
#pragma config IOL1WAY = OFF            /* Pin Select Lock */
#pragma config PMDL1WAY = OFF           /* Peripheral Disable */
#pragma config PGL1WAY = OFF            /* Permission Group Lock */
#pragma config FETHIO = ON              /* Ethernet I/O Pins: use default pin assignment */
#pragma config FMIIEN = OFF             /* Ethernet RMII/MII Selection : OFF = use RMII */
/* Note: for XC32v2.15 EXTDDRSIZE must be specified for the PIC32MZ2064DAH169 */
#if (__XC32_VERSION == 2150)
#pragma config EXTDDRSIZE = DDR_SIZE_32MB   /* DDR is 32MiB */
#endif
#pragma config USERID = 0x0000          /* User ID (may be read by ICSP or JTAG */

/* DEVCFG4 */
#pragma config SWDTPS = SPS1048576      /* Sleep WDT Scaler: SPS1048576 = max. delay */

/* DEVCP0 */
#pragma config CP = OFF                 /* Code Protect (OFF: disabled, ON: enabled) */
