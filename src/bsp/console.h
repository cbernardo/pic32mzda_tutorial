/**
 * File : console.h
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Declares the debug console API.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdint.h>
#include "uart.h"

/* API Summary
 * -----------
 * Note: the console driver is essentially a minor extension of the
 *       UART driver and as such shares all the API functions.
 *
 *  int           (*Init)            (void);
 *  void          (*Shutdown)        (void);
 *  int           (*SendByte)        (int const byte);
 *  int           (*SendString)      (void const *buffer, int const length);
 *  int           (*ReceiveByte)     (void *byte);
 *  int           (*ReceiveString)   (void *buffer, int *length);
 *  int           (*TxFlush)         (void);
 *  int           (*RxFlush)         (void);
 *  UartStatus_t  (*GetStatus)       (void);
 *  void          (*ClearError)      (void);
 *  int           (*Loopback)        (bool const enable);
 */

typedef struct
{
    int           (*Init)            (void);
    void          (*Shutdown)        (void);
    int           (*SendByte)        (int const byte);
    int           (*SendString)      (void const *buffer, int const length);
    int           (*ReceiveByte)     (void *byte);
    int           (*ReceiveString)   (void *buffer, int *length);
    int           (*TxFlush)         (void);
    int           (*RxFlush)         (void);
    UartStatus_t  (*GetStatus)       (void);
    void          (*ClearError)      (void);
    int           (*Loopback)        (bool const enable);
} const DrvConsole_t;

extern DrvConsole_t Console;

#endif /* CONSOLE_H */
