/**
 * File : timer.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Implements the Timer module driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/*
 * Design notes:
 * + Timer1 is special and is reserved for use as the system millisecond timer.
 * + T2..T7 are typically used for PWM.
 * + Although timers 2..9 can be cascaded to produce up to 4 32-bit timers this is not
 *   supported by the driver since there is rarely a genuine need for a 32-bit timer.
 */

#include <xc.h>
#include <sys/attribs.h>
#include <stdbool.h>
#include <stdint.h>

#include "mcu_reg.h"
#include "pbclk.h"
#include "syserr.h"
#include "timer.h"


/*******************************************************************************
 *              PRIVATE MACROS
 ******************************************************************************/
#define TIMER_BIT_ON    (1UL << 15)


/*******************************************************************************
 *              PRIVATE TYPES
 ******************************************************************************/

typedef struct
{
    uint32_t                : 1;
    uint32_t TCS            : 1;    /* Timer Clock Source, 1 = external clock */
    uint32_t TSYNC          : 1;    /* 1: use external clock input synchronization */
    uint32_t                : 1;
    uint32_t TCKPS          : 2;    /* Prescale: 1, 8, 64, 256 */
    uint32_t                : 1;
    uint32_t TGATE          : 1;    /* 1: gated time accumulation enabled if TCS = 0 */
    uint32_t TECS           : 2;    /* External Clock Selection */
    uint32_t                : 1;
    uint32_t const TWIP     : 1;    /* 1: async write to TMR1 is in progress */
    uint32_t TWDIS          : 1;    /* 1: writes to TMR1 are disabled until pending write completes */
    uint32_t TSIDL          : 1;    /* 1: stop in idle mode */
    uint32_t                : 1;
    uint32_t ON             : 1;    /* 1: enabled */
    uint32_t                : 16;
} Timer1Ctl_t;

typedef struct
{
    uint32_t                : 1;
    uint32_t TCS            : 1;    /* Timer Clock Source, 1 = external clock */
    uint32_t                : 2;
    uint32_t TCKPS          : 3;    /* Prescale: 1, 2, 4, 8, 16, 32, 64, 256 */
    uint32_t TGATE          : 1;    /* 1: gated time accumulation enabled if TCS = 0 */
    uint32_t                : 5;
    uint32_t TSIDL          : 1;    /* 1: stop in idle mode */
    uint32_t                : 1;
    uint32_t ON             : 1;    /* 1: enabled */
    uint32_t                : 16;
} TimerXCtl_t;

typedef struct
{
    int32_t  const         maxDivisors;
    uint32_t const * const divisor;
} TimerScaler_t;

typedef struct
{
    uint32_t  const  ibit;          /* interrupt enable and interrupt flag bit */
    uint32_t  const  ipbasepos;     /* interrupt priority base position (0, 8, 16, 24) */
    uint32_t  const  iplvl;         /* interrupt priority and sub-priority */
    Register_t *ifsr;               /* interrupt flag status register */
    Register_t *iecr;               /* interrupt enable control register */
    Register_t *iplr;               /* interrupt priority level register */
} TimerICtl_t;


/*******************************************************************************
 *              PRIVATE VARIABLES AND CONSTANTS
 ******************************************************************************/
static Register_t * const m_ctrl[9] =
{
    (Register_t *)&T1CON,
    (Register_t *)&T2CON,
    (Register_t *)&T3CON,
    (Register_t *)&T4CON,
    (Register_t *)&T5CON,
    (Register_t *)&T6CON,
    (Register_t *)&T7CON,
    (Register_t *)&T8CON,
    (Register_t *)&T9CON,
};

static Register_t * const m_counter[9] =
{
    (Register_t *)&TMR1,
    (Register_t *)&TMR2,
    (Register_t *)&TMR3,
    (Register_t *)&TMR4,
    (Register_t *)&TMR5,
    (Register_t *)&TMR6,
    (Register_t *)&TMR7,
    (Register_t *)&TMR8,
    (Register_t *)&TMR9,
};

static Register_t * const m_period[9] =
{
    (Register_t *)&PR1,
    (Register_t *)&PR2,
    (Register_t *)&PR3,
    (Register_t *)&PR4,
    (Register_t *)&PR5,
    (Register_t *)&PR6,
    (Register_t *)&PR7,
    (Register_t *)&PR8,
    (Register_t *)&PR9,
};

static TimerICtl_t const m_int[9] =
{
    /* Timer 1 */
    {
        .ibit = (1UL << 4),
        .ipbasepos = 0UL,
        .iplvl = (((CFG_T1_IPL) << 2) | (CFG_T1_ISL)),
        .ifsr = (Register_t *)&IFS0,
        .iecr = (Register_t *)&IEC0,
        .iplr = (Register_t *)&IPC1,
    },

    /* Timer 2 */
    {
        .ibit = (1UL << 9),
        .ipbasepos = 8UL,
        .iplvl = (((CFG_T2_IPL) << 2) | (CFG_T2_ISL)),
        .ifsr = (Register_t *)&IFS0,
        .iecr = (Register_t *)&IEC0,
        .iplr = (Register_t *)&IPC2,
    },

    /* Timer 3 */
    {
        .ibit = (1UL << 14),
        .ipbasepos = 16UL,
        .iplvl = (((CFG_T3_IPL) << 2) | (CFG_T3_ISL)),
        .ifsr = (Register_t *)&IFS0,
        .iecr = (Register_t *)&IEC0,
        .iplr = (Register_t *)&IPC3,
    },

    /* Timer 4 */
    {
        .ibit = (1UL << 19),
        .ipbasepos = 24UL,
        .iplvl = (((CFG_T4_IPL) << 2) | (CFG_T4_ISL)),
        .ifsr = (Register_t *)&IFS0,
        .iecr = (Register_t *)&IEC0,
        .iplr = (Register_t *)&IPC4,
    },

    /* Timer 5 */
    {
        .ibit = (1UL << 24),
        .ipbasepos = 0UL,
        .iplvl = (((CFG_T5_IPL) << 2) | (CFG_T5_ISL)),
        .ifsr = (Register_t *)&IFS0,
        .iecr = (Register_t *)&IEC0,
        .iplr = (Register_t *)&IPC6,
    },

    /* Timer 6 */
    {
        .ibit = (1UL << 28),
        .ipbasepos = 0UL,
        .iplvl = (((CFG_T6_IPL) << 2) | (CFG_T6_ISL)),
        .ifsr = (Register_t *)&IFS0,
        .iecr = (Register_t *)&IEC0,
        .iplr = (Register_t *)&IPC7,
    },

    /* Timer 7 */
    {
        .ibit = (1UL << 0),
        .ipbasepos = 0UL,
        .iplvl = (((CFG_T7_IPL) << 2) | (CFG_T7_ISL)),
        .ifsr = (Register_t *)&IFS1,
        .iecr = (Register_t *)&IEC1,
        .iplr = (Register_t *)&IPC8,
    },

    /* Timer 8 */
    {
        .ibit = (1UL << 4),
        .ipbasepos = 0UL,
        .iplvl = (((CFG_T8_IPL) << 2) | (CFG_T8_ISL)),
        .ifsr = (Register_t *)&IFS1,
        .iecr = (Register_t *)&IEC1,
        .iplr = (Register_t *)&IPC9,
    },

    /* Timer 9 */
    {
        .ibit = (1UL << 8),
        .ipbasepos = 0UL,
        .iplvl = (((CFG_T9_IPL) << 2) | (CFG_T9_ISL)),
        .ifsr = (Register_t *)&IFS1,
        .iecr = (Register_t *)&IEC1,
        .iplr = (Register_t *)&IPC10,
    },
};

static uint32_t const m_t1divisors[] = {1, 8, 64, 256};
static uint32_t const m_tXdivisors[] = {1, 2, 4, 8, 16, 32, 64, 256};

TimerScaler_t const m_t1scaler =
{
    .maxDivisors = 4,
    .divisor  = m_t1divisors,
};

TimerScaler_t const m_tXscaler =
{
    .maxDivisors = 8,
    .divisor  = m_tXdivisors,
};

static TimerHandler_t m_isr[9];         /* interrupt handlers */
static uint32_t       m_freqNom[9];     /* nominal frequency; 0 if not previously set */
static uint32_t       m_freqTrue[9];    /* true frequency rounded to nearest integer */
static uint32_t       m_offset[9];      /* initial counter offset when enabling */

/*******************************************************************************
 *              PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/

/* GENERAL TIMER FUNCTIONS */
static int      timer_enable(int const idx, const bool enable);
static bool     timer_enabled(int const idx);
static void     timer_set_isr(int const idx, void (*isr)(void));
static int      timer_set_freq(int const idx, uint32_t const frequency);
static int      timer_get_freq(int const idx, uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer_get_period(int const idx);
static int      timer_set_offset(int const idx, uint32_t const offset);

/* TIMER 1 FUNCTIONS */
static int      timer1_enable(const bool enable);
static bool     timer1_enabled(void);
static void     timer1_set_isr(void (*isr)(void));
static int      timer1_set_freq(uint32_t const frequency);
static int      timer1_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer1_get_period(void);
static int      timer1_set_offset(uint32_t const offset);

/* TIMER 2 FUNCTIONS */
static int      timer2_enable(const bool enable);
static bool     timer2_enabled(void);
static void     timer2_set_isr(void (*isr)(void));
static int      timer2_set_freq(uint32_t const frequency);
static int      timer2_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer2_get_period(void);
static int      timer2_set_offset(uint32_t const offset);

/* TIMER 3 FUNCTIONS */
static int      timer3_enable(const bool enable);
static bool     timer3_enabled(void);
static void     timer3_set_isr(void (*isr)(void));
static int      timer3_set_freq(uint32_t const frequency);
static int      timer3_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer3_get_period(void);
static int      timer3_set_offset(uint32_t const offset);

/* TIMER 4 FUNCTIONS */
static int      timer4_enable(const bool enable);
static bool     timer4_enabled(void);
static void     timer4_set_isr(void (*isr)(void));
static int      timer4_set_freq(uint32_t const frequency);
static int      timer4_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer4_get_period(void);
static int      timer4_set_offset(uint32_t const offset);

/* TIMER 5 FUNCTIONS */
static int      timer5_enable(const bool enable);
static bool     timer5_enabled(void);
static void     timer5_set_isr(void (*isr)(void));
static int      timer5_set_freq(uint32_t const frequency);
static int      timer5_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer5_get_period(void);
static int      timer5_set_offset(uint32_t const offset);

/* TIMER 6 FUNCTIONS */
static int      timer6_enable(const bool enable);
static bool     timer6_enabled(void);
static void     timer6_set_isr(void (*isr)(void));
static int      timer6_set_freq(uint32_t const frequency);
static int      timer6_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer6_get_period(void);
static int      timer6_set_offset(uint32_t const offset);

/* TIMER 7 FUNCTIONS */
static int      timer7_enable(const bool enable);
static bool     timer7_enabled(void);
static void     timer7_set_isr(void (*isr)(void));
static int      timer7_set_freq(uint32_t const frequency);
static int      timer7_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer7_get_period(void);
static int      timer7_set_offset(uint32_t const offset);

/* TIMER 8 FUNCTIONS */
static int      timer8_enable(const bool enable);
static bool     timer8_enabled(void);
static void     timer8_set_isr(void (*isr)(void));
static int      timer8_set_freq(uint32_t const frequency);
static int      timer8_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer8_get_period(void);
static int      timer8_set_offset(uint32_t const offset);

/* TIMER 9 FUNCTIONS */
static int      timer9_enable(const bool enable);
static bool     timer9_enabled(void);
static void     timer9_set_isr(void (*isr)(void));
static int      timer9_set_freq(uint32_t const frequency);
static int      timer9_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq);
static uint32_t timer9_get_period(void);
static int      timer9_set_offset(uint32_t const offset);

/* Generic Timer Handler */
static void TxHandler(int const idx);

/* TIMER ISR HANDLERS - do not declare as static */
void __ISR(_TIMER_1_VECTOR, CFG_T1_SRS) T1Handler(void);
void __ISR(_TIMER_2_VECTOR, CFG_T2_SRS) T2Handler(void);
void __ISR(_TIMER_3_VECTOR, CFG_T3_SRS) T3Handler(void);
void __ISR(_TIMER_4_VECTOR, CFG_T4_SRS) T4Handler(void);
void __ISR(_TIMER_5_VECTOR, CFG_T5_SRS) T5Handler(void);
void __ISR(_TIMER_6_VECTOR, CFG_T6_SRS) T6Handler(void);
void __ISR(_TIMER_7_VECTOR, CFG_T7_SRS) T7Handler(void);
void __ISR(_TIMER_8_VECTOR, CFG_T8_SRS) T8Handler(void);
void __ISR(_TIMER_9_VECTOR, CFG_T9_SRS) T9Handler(void);


/*******************************************************************************
 *              TIMER API
 ******************************************************************************/

DrvTimer_t Timer1 =
{
    .Enable = timer1_enable,
    .Enabled = timer1_enabled,
    .SetISR = timer1_set_isr,
    .SetFrequency = timer1_set_freq,
    .GetFrequency = timer1_get_freq,
    .GetPeriod = timer1_get_period,
    .SetOffset = timer1_set_offset,
};

DrvTimer_t Timer2 =
{
    .Enable = timer2_enable,
    .Enabled = timer2_enabled,
    .SetISR = timer2_set_isr,
    .SetFrequency = timer2_set_freq,
    .GetFrequency = timer2_get_freq,
    .GetPeriod = timer2_get_period,
    .SetOffset = timer2_set_offset,
};

DrvTimer_t Timer3 =
{
    .Enable = timer3_enable,
    .Enabled = timer3_enabled,
    .SetISR = timer3_set_isr,
    .SetFrequency = timer3_set_freq,
    .GetFrequency = timer3_get_freq,
    .GetPeriod = timer3_get_period,
    .SetOffset = timer3_set_offset,
};

DrvTimer_t Timer4 =
{
    .Enable = timer4_enable,
    .Enabled = timer4_enabled,
    .SetISR = timer4_set_isr,
    .SetFrequency = timer4_set_freq,
    .GetFrequency = timer4_get_freq,
    .GetPeriod = timer4_get_period,
    .SetOffset = timer4_set_offset,
};

DrvTimer_t Timer5 =
{
    .Enable = timer5_enable,
    .Enabled = timer5_enabled,
    .SetISR = timer5_set_isr,
    .SetFrequency = timer5_set_freq,
    .GetFrequency = timer5_get_freq,
    .GetPeriod = timer5_get_period,
    .SetOffset = timer5_set_offset,
};

DrvTimer_t Timer6 =
{
    .Enable = timer6_enable,
    .Enabled = timer6_enabled,
    .SetISR = timer6_set_isr,
    .SetFrequency = timer6_set_freq,
    .GetFrequency = timer6_get_freq,
    .GetPeriod = timer6_get_period,
    .SetOffset = timer6_set_offset,
};

DrvTimer_t Timer7 =
{
    .Enable = timer7_enable,
    .Enabled = timer7_enabled,
    .SetISR = timer7_set_isr,
    .SetFrequency = timer7_set_freq,
    .GetFrequency = timer7_get_freq,
    .GetPeriod = timer7_get_period,
    .SetOffset = timer7_set_offset,
};

DrvTimer_t Timer8 =
{
    .Enable = timer8_enable,
    .Enabled = timer8_enabled,
    .SetISR = timer8_set_isr,
    .SetFrequency = timer8_set_freq,
    .GetFrequency = timer8_get_freq,
    .GetPeriod = timer8_get_period,
    .SetOffset = timer8_set_offset,
};

DrvTimer_t Timer9 =
{
    .Enable = timer9_enable,
    .Enabled = timer9_enabled,
    .SetISR = timer9_set_isr,
    .SetFrequency = timer9_set_freq,
    .GetFrequency = timer9_get_freq,
    .GetPeriod = timer9_get_period,
    .SetOffset = timer9_set_offset,
};


/*******************************************************************************
 *              PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/

/* GENERAL TIMER FUNCTIONS */
static int  timer_enable(int const idx, const bool enable)
{
    int result = 0;

    if (enable)
    {
        Register_t *ctrl = m_ctrl[idx];
        Register_t *period = m_period[idx];
        Register_t *count = m_counter[idx];

        if (0 == (ctrl->reg & (TIMER_BIT_ON)))
        {
            /* note: period register must be non-zero, Module Disable bit must be zero,
             * and PBClk3 must be enabled */
            if ((0 != period->reg) && (0 == (PMD4 & (1UL << idx))) && (PbClk3.Enabled()))
            {
                if (0 != m_isr[idx])
                {
                    TimerICtl_t const *intctl = &m_int[idx];
                    intctl->ifsr->clr = intctl->ibit;
                    intctl->iplr->clr = (0x1FUL << intctl->ipbasepos);
                    intctl->iplr->set = (intctl->iplvl << intctl->ipbasepos);
                    intctl->iecr->set = intctl->ibit;
                }

                uint32_t offset = m_offset[idx];

                if (period->reg > offset)
                {
                    count->reg = offset;
                }

                m_ctrl[idx]->set = (TIMER_BIT_ON);
            }
            else
            {
                /* invalid period register value, timer disabled, or PBClk3 disabled */
                if (0 == period->reg)
                {
                    result = ERR_PARAM;
                }
                else
                {
                    result = ERR_NODEV;
                }
            }
        }
    }
    else
    {
        m_ctrl[idx]->clr = (TIMER_BIT_ON);
    }

    return result;
}

static bool  timer_enabled(int const idx)
{
    bool result = false;

    if (0 != (m_ctrl[idx]->reg & (TIMER_BIT_ON)))
    {
        result = true;
    }

    return result;
}

static void  timer_set_isr(int const idx, void (*isr)(void))
{
    m_isr[idx] = isr;

    if (0 != (m_ctrl[idx]->reg & (TIMER_BIT_ON)))
    {
        /* enable interrupts if necessary */
        TimerICtl_t const *intctl = &m_int[idx];

        if (0 == (intctl->iecr->reg & (intctl->ibit)))
        {
            intctl->iplr->clr = (0x1FUL << intctl->ipbasepos);
            intctl->iplr->set = (intctl->iplvl << intctl->ipbasepos);
            intctl->ifsr->clr = intctl->ibit;
            intctl->iecr->set = intctl->ibit;
        }
    }

    return;
} /* timer_set_isr */

static int  timer_set_freq(int const idx, uint32_t const frequency)
{
    /* Operational notes:
     * + check module and PB3 are not disabled
     * + check frequency is achievable
     * + set prescaler and period register
     * + switch ON if necessary; if already on then handle overflow if necessary
     */
    int result = 0;

    if ((0 != (PMD4 & (1UL << idx))) || (!PbClk3.Enabled()))
    {
        /* module disabled */
        result = ERR_NODEV;
    }
    else
    {
        /* calculate nearest frequency that does not exceed the target */
        uint32_t  divisor;
        uint32_t  minFreq;
        int32_t   maxIdx = m_tXscaler.maxDivisors;
        uint32_t  const *divisors = m_tXscaler.divisor;
        uint32_t  srcclk = PbClk3.GetFrequency();
        bool      restart = false;  /* true if module was turned off for frequency change */

        if (0 == idx)
        {
            maxIdx = m_t1scaler.maxDivisors;
            divisors = m_t1scaler.divisor;
        }

        /* note: (divisor << 16UL) will never exceed 32 bits */
        divisor = divisors[maxIdx - 1] << 16UL;
        minFreq = (srcclk + (divisor - 1UL)) / divisor;

        if ((frequency < minFreq) || (frequency > (srcclk >> 1UL)))
        {
            result = ERR_PARAM;
        }
        else
        {
            /* Calculate divisor to obtain exact or nearest lower frequency.
             * divisor = ((clock + ((prescaler * freq) - 1)) / (prescaler * freq)) - 1UL;
             */
            uint32_t  currentDivIdx;
            uint32_t  tmpdiv;
            int32_t   divIndex = 0;
            union
            {
                uint32_t    value;
                Timer1Ctl_t t1ctl;
                TimerXCtl_t txctl;
            } ctlreg;
            divisor = 0;

            while ((0 == divisor) && (divIndex < maxIdx))
            {
                tmpdiv = divisors[divIndex] << 16UL;

                if (frequency >= (srcclk + (tmpdiv - 1)) / tmpdiv)
                {
                    tmpdiv = divisors[divIndex] * frequency;
                    divisor = (srcclk + (tmpdiv - 1UL)) / (tmpdiv);
                }
                else
                {
                    ++divIndex;
                }
            }

            /* assign the nominal and true frequencies */
            m_freqNom[idx] = frequency;
            tmpdiv = divisor * divisors[divIndex];
            m_freqTrue[idx] = (srcclk + (tmpdiv - 1UL)) / tmpdiv;
            /* adjust divisor to suit Period Register setting */
            --divisor;
            ctlreg.value = m_ctrl[idx]->reg;

            if (0 == idx)
            {
                currentDivIdx = ctlreg.t1ctl.TCKPS;
            }
            else
            {
                currentDivIdx = ctlreg.txctl.TCKPS;
            }

            if ((currentDivIdx != (uint32_t)divIndex) && timer_enabled(idx))
            {
                /* switch the timer off and flag a restart */
                ctlreg.txctl.ON = 0;
                m_ctrl[idx]->reg = ctlreg.value;
                restart = true;
            }

            if (timer_enabled(idx))
            {
                /* module is active; care must be taken when changing period register */
                m_period[idx]->reg = divisor;

                if (m_counter[idx]->reg >= divisor)
                {
                    m_counter[idx]->reg = (divisor - 1);
                }
            }
            else
            {
                /* module is inactive */
                if (0 == idx)
                {
                    /* note: T1 has a 2-bit prescale selector */
                    ctlreg.t1ctl.TCKPS = ((uint32_t)(divIndex)) & 3UL;
                }
                else
                {
                    /* note: T2..9 have a 3-bit prescale selector */
                    ctlreg.txctl.TCKPS = ((uint32_t)divIndex) & 7UL;
                }

                m_ctrl[idx]->reg = ctlreg.value;
                m_counter[idx]->reg = 0;
                m_period[idx]->reg = divisor;
            }

            if (restart)
            {
                ctlreg.value = 0;
                ctlreg.txctl.ON = 1;
                m_ctrl[idx]->set = ctlreg.value;
            }
        }
    }

    return result;
}

static int timer_get_freq(int const idx, uint32_t *nominalFreq, uint32_t *actualFreq)
{
    int result = ERR_PARAM;

    if (0 != nominalFreq)
    {
        *nominalFreq = m_freqNom[idx];
        result = 0;
    }

    if (0 != actualFreq)
    {
        *actualFreq = m_freqTrue[idx];
        result = 0;
    }

    return result;
}

static uint32_t timer_get_period(int const idx)
{
    return (m_period[idx]->reg + 1UL);
}

static int  timer_set_offset(int const idx, uint32_t const offset)
{
    int result = ERR_FAIL;

    if ((1UL << 16) > offset)
    {
        m_offset[idx] = offset;
        result = 0;
    }

    return result;
}


/* TIMER 1 FUNCTIONS */
static int  timer1_enable(const bool enable)
{
    return timer_enable(0, enable);
}

static bool  timer1_enabled(void)
{
    return timer_enabled(0);
}

static void  timer1_set_isr(void (*isr)(void))
{
    timer_set_isr(0, isr);
    return;
}

static int  timer1_set_freq(uint32_t const frequency)
{
    return timer_set_freq(0, frequency);
}

static int  timer1_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(0, nominalFreq, actualFreq);
}

static uint32_t timer1_get_period(void)
{
    return timer_get_period(0);
}

static int  timer1_set_offset(uint32_t const offset)
{
    return timer_set_offset(0, offset);
}

/* TIMER 2 FUNCTIONS */
static int  timer2_enable(const bool enable)
{
    return timer_enable(1, enable);
}

static bool  timer2_enabled(void)
{
    return timer_enabled(1);
}

static void  timer2_set_isr(void (*isr)(void))
{
    timer_set_isr(1, isr);
    return;
}

static int  timer2_set_freq(uint32_t const frequency)
{
    return timer_set_freq(1, frequency);
}

static int  timer2_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(1, nominalFreq, actualFreq);
}

static uint32_t timer2_get_period(void)
{
    return timer_get_period(1);
}

static int  timer2_set_offset(uint32_t const offset)
{
    return timer_set_offset(1, offset);
}


/* TIMER 3 FUNCTIONS */
static int  timer3_enable(const bool enable)
{
    return timer_enable(2, enable);
}

static bool  timer3_enabled(void)
{
    return timer_enabled(2);
}

static void  timer3_set_isr(void (*isr)(void))
{
    timer_set_isr(2, isr);
    return;
}

static int  timer3_set_freq(uint32_t const frequency)
{
    return timer_set_freq(2, frequency);
}

static int  timer3_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(2, nominalFreq, actualFreq);
}

static uint32_t timer3_get_period(void)
{
    return timer_get_period(2);
}

static int  timer3_set_offset(uint32_t const offset)
{
    return timer_set_offset(2, offset);
}

/* TIMER 4 FUNCTIONS */
static int  timer4_enable(const bool enable)
{
    return timer_enable(3, enable);
}

static bool  timer4_enabled(void)
{
    return timer_enabled(3);
}

static void timer4_set_isr(void (*isr)(void))
{
    timer_set_isr(3, isr);
    return;
}

static int  timer4_set_freq(uint32_t const frequency)
{
    return timer_set_freq(3, frequency);
}

static int  timer4_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(3, nominalFreq, actualFreq);
}

static uint32_t timer4_get_period(void)
{
    return timer_get_period(3);
}

static int  timer4_set_offset(uint32_t const offset)
{
    return timer_set_offset(3, offset);
}


/* TIMER 5 FUNCTIONS */
static int  timer5_enable(const bool enable)
{
    return timer_enable(4, enable);
}

static bool  timer5_enabled(void)
{
    return timer_enabled(4);
}

static void  timer5_set_isr(void (*isr)(void))
{
    timer_set_isr(4, isr);
    return;
}

static int  timer5_set_freq(uint32_t const frequency)
{
    return timer_set_freq(4, frequency);
}

static int  timer5_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(4, nominalFreq, actualFreq);
}

static uint32_t timer5_get_period(void)
{
    return timer_get_period(4);
}

static int  timer5_set_offset(uint32_t const offset)
{
    return timer_set_offset(4, offset);
}


/* TIMER 6 FUNCTIONS */
static int  timer6_enable(const bool enable)
{
    return timer_enable(5, enable);
}

static bool  timer6_enabled(void)
{
    return timer_enabled(5);
}

static void  timer6_set_isr(void (*isr)(void))
{
    timer_set_isr(5, isr);
    return;
}

static int  timer6_set_freq(uint32_t const frequency)
{
    return timer_set_freq(5, frequency);
}

static int  timer6_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(5, nominalFreq, actualFreq);
}

static uint32_t timer6_get_period(void)
{
    return timer_get_period(5);
}

static int  timer6_set_offset(uint32_t const offset)
{
    return timer_set_offset(5, offset);
}


/* TIMER 7 FUNCTIONS */
static int  timer7_enable(const bool enable)
{
    return timer_enable(6, enable);
}

static bool  timer7_enabled(void)
{
    return timer_enabled(6);
}

static void  timer7_set_isr(void (*isr)(void))
{
    timer_set_isr(6, isr);
    return;
}

static int  timer7_set_freq(uint32_t const frequency)
{
    return timer_set_freq(6, frequency);
}

static int  timer7_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(6, nominalFreq, actualFreq);
}

static uint32_t timer7_get_period(void)
{
    return timer_get_period(6);
}

static int  timer7_set_offset(uint32_t const offset)
{
    return timer_set_offset(6, offset);
}


/* TIMER 8 FUNCTIONS */
static int  timer8_enable(const bool enable)
{
    return timer_enable(7, enable);
}

static bool  timer8_enabled(void)
{
    return timer_enabled(7);
}

static void  timer8_set_isr(void (*isr)(void))
{
    timer_set_isr(7, isr);
    return;
}

static int  timer8_set_freq(uint32_t const frequency)
{
    return timer_set_freq(7, frequency);
}

static int  timer8_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(7, nominalFreq, actualFreq);
}

static uint32_t timer8_get_period(void)
{
    return timer_get_period(7);
}

static int  timer8_set_offset(uint32_t const offset)
{
    return timer_set_offset(7, offset);
}


/* TIMER 9 FUNCTIONS */
static int  timer9_enable(const bool enable)
{
    return timer_enable(8, enable);
}

static bool  timer9_enabled(void)
{
    return timer_enabled(8);
}

static void timer9_set_isr(void (*isr)(void))
{
    timer_set_isr(8, isr);
    return;
}

static int  timer9_set_freq(uint32_t const frequency)
{
    return timer_set_freq(8, frequency);
}

static int  timer9_get_freq(uint32_t *nominalFreq, uint32_t *actualFreq)
{
    return timer_get_freq(8, nominalFreq, actualFreq);
}

static uint32_t timer9_get_period(void)
{
    return timer_get_period(8);
}

static int  timer9_set_offset(uint32_t const offset)
{
    return timer_set_offset(8, offset);
}


/* Generic Timer Handler */
static void  TxHandler(int const idx)
{
    TimerHandler_t func = m_isr[idx];
    m_int[idx].ifsr->clr = m_int[idx].ibit;

    if (0 != func)
    {
        func();
    }
    else
    {
        /* no handler; disable the interrupt */
        m_int[idx].iecr->clr = m_int[idx].ibit;
    }

    return;
} /* TxHandler */


/* TIMER ISR HANDLERS */
void __ISR(_TIMER_1_VECTOR, CFG_T1_SRS) T1Handler(void)
{
    TxHandler(0);
}

void __ISR(_TIMER_2_VECTOR, CFG_T2_SRS) T2Handler(void)
{
    TxHandler(1);
}

void __ISR(_TIMER_3_VECTOR, CFG_T3_SRS) T3Handler(void)
{
    TxHandler(2);
}

void __ISR(_TIMER_4_VECTOR, CFG_T4_SRS) T4Handler(void)
{
    TxHandler(3);
}

void __ISR(_TIMER_5_VECTOR, CFG_T5_SRS) T5Handler(void)
{
    TxHandler(4);
}

void __ISR(_TIMER_6_VECTOR, CFG_T6_SRS) T6Handler(void)
{
    TxHandler(5);
}

void __ISR(_TIMER_7_VECTOR, CFG_T7_SRS) T7Handler(void)
{
    TxHandler(6);
}

void __ISR(_TIMER_8_VECTOR, CFG_T8_SRS) T8Handler(void)
{
    TxHandler(7);
}

void __ISR(_TIMER_9_VECTOR, CFG_T9_SRS) T9Handler(void)
{
    TxHandler(8);
}
