/**
 * File : sched.h
 * Copyright (C) 2023 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Cooperative scheduler declaration and data types.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef COOPERATIVE_SCHED_H
#define COOPERATIVE_SCHED_H

#include "sysutil.h"

typedef struct
{
    void       (*task) (void);              /* task function to be executed */
    char const  *name;                      /* task name */
    SYSCYCLE     min_execution_cycles;      /* min task execution time */
    SYSCYCLE     max_execution_cycles;      /* max task execution time */
} TaskPersistent_e;

typedef struct
{
    unsigned int (*task) (void);            /* task function to be executed */
    char const    *name;                    /* task name */
    SYSCYCLE       min_execution_cycles;    /* min task execution time */
    SYSCYCLE       max_execution_cycles;    /* max task execution time */
    SYSTICK        max_latency_ms;          /* max latency to task execution */
    unsigned int   interval_default;        /* default interval */
    unsigned int   interval_counter;        /* current interval counter */
} TaskSoftSched_e;

/* initialize scheduler execution data */
void schedule_init(void);

/* print summary of scheduler data */
void schedule_print_summary(void);

/* scheduler task; to be called in main program loop */
void schedule(void);

/* user-defined array of persistent tasks; the array must end with an
 * entry which has a NULL task function pointer.
 */
extern TaskPersistent_e g_tasks_persist[];

/* user-defined array of soft-scheduled tasks; the array must end with an
 * entry which has a NULL task function pointer.
 */
extern TaskSoftSched_e  g_tasks_soft[];


#endif /* COOPERATIVE_SCHED_H */
