/**
 * File : memutil.h
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Declares the Cache management and Address Conversion API.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CACHE_H
#define CACHE_H

#include <stdint.h>

/* (void *) value signifying an invalid memory address */
#define CFG_MEM_INVALID_ADDR    (0xFFFFFFFF)

typedef struct
{
    /**
     * @function Evict
     * @detail   Evicts the cache line containing the given address.
     *
     * @param    address [IN] virtual cached address to evict.
     * @return   0 on success, else -1.
     */
    int    (*Evict)             (void const * const address);

    /**
     * @function EvictRange
     * @detail   Evicts the cache lines containing the
     *           <b>bytes</b> starting at the given <b>address</b>.
     *
     * @param    address [IN] virtual cached address to evict.
     * @param    bytes [IN] is the minimum number of bytes to be evicted.
     * @return   0 on success, else -1.
     */
    int    (*EvictRange)        (void const * const address, uint32_t const bytes);

    /**
     * @function Flush
     * @detail   Writes back and evicts the cache line containing the given address.
     *
     * @param    address [IN] virtual cached address to flush.
     * @return   0 on success, else -1.
     */
    int    (*Flush)             (void const * const address);

    /**
     * @function FlushRange
     * @detail   Writes back and evicts the cache lines containing the
     *           <b>bytes</b> starting at the given <b>address</b>.
     *
     * @param    address [IN] virtual cached address to flush.
     * @param    bytes [IN] is the minimum number of bytes to be flushed.
     * @return   0 on success, else -1.
     */
    int   (*FlushRange)         (void const * const address, uint32_t const bytes);

    /**
     * @function GetCachedAddress
     * @detail   Calculates a cached address given a Virtual or Physical address.
     *
     * @param    address [IN] virtual or physical address to translate.
     * @return   Cached address, or 0xFFFFFFFF on failure.
     */
    void *(*GetCachedAddress)   (void const * const address);

    /**
     * @function GetNonCachedAddress
     * @detail   Calculates a non-cached address given a Virtual or Physical address.
     *
     * @param    address [IN] virtual or physical address to translate.
     * @return   Non-cached address, or 0xFFFFFFFF on failure.
     */
    void *(*GetNonCachedAddress)(void const * const address);

    /**
     * @function GetPhysicalAddress
     * @detail   Calculates a physical address given a virtual or physical address.
     *
     * @param    address [IN] virtual or physical address to translate.
     * @return   Physical address, or 0xFFFFFFFF on failure.
     */
    void *(*GetPhysicalAddress) (void const * const address);
} const Memory_t;

extern Memory_t const Mem;

#endif /* CACHE_H */
