/**
 * File : sched.c
 * Copyright (C) 2023 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Cooperative scheduler implementation.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */


#include <limits.h>
#include <stdio.h>
#include "sched_simple.h"
#include "sysutil.h"


/*******************************************************************************
 *              PRIVATE VARIABLES
 ******************************************************************************/
static SYSTICK m_schedTime;


/*******************************************************************************
 *              PUBLIC FUNCTIONS
 ******************************************************************************/

/**
 * @function schedule_init
 * @detail   Initializes the performance data of scheduler tasks
 *           and sets the internal schedule timer.
 */
void schedule_init(void)
{
    int idx = 0;
    m_schedTime = Sys.MillisecTime();

    while (0 != g_tasks_persist[idx].task)
    {
        g_tasks_persist[idx].max_execution_cycles = 0;
        g_tasks_persist[idx].min_execution_cycles = UINT_MAX;
        ++idx;
    }

    idx = 0;
    while (0 != g_tasks_soft[idx].task)
    {
        g_tasks_soft[idx].max_execution_cycles = 0;
        g_tasks_soft[idx].min_execution_cycles = UINT_MAX;
        g_tasks_soft[idx].max_latency_ms = 0;
        ++idx;
    }

    return;
} /* schedule_init */

/**
 * @function schedule_print_summary
 * @detail   Print summary of task execution times and latency data
 */
void schedule_print_summary(void)
{
    int idx = 0;
    printf("***** SCHEDULER SUMMARY *****\n");

    while (0 != g_tasks_persist[idx].task)
    {
        if (UINT_MAX > g_tasks_persist[idx].min_execution_cycles)
        {
            printf("* task '%s':\n", g_tasks_persist[idx].name);
            printf("  min. cycles: %u\n", g_tasks_persist[idx].min_execution_cycles);
            printf("  max. cycles: %u\n", g_tasks_persist[idx].max_execution_cycles);
        }
        else
        {
            /* task never executed */
            printf("* task not run: '%s'\n", g_tasks_persist[idx].name);
        }

        ++idx;
    }

    idx = 0;
    while (0 != g_tasks_soft[idx].task)
    {
        if (UINT_MAX > g_tasks_soft[idx].min_execution_cycles)
        {
            printf("* task '%s'\n", g_tasks_soft[idx].name);
            printf("  min. cycles : %u\n", g_tasks_soft[idx].min_execution_cycles);
            printf("  max. cycles : %u\n", g_tasks_soft[idx].max_execution_cycles);
            printf("  max. latency: %u (ms)\n", g_tasks_soft[idx].max_latency_ms);
        }
        else
        {
            /* task never executed */
            printf("* task not run: '%s'\n", g_tasks_soft[idx].name);
        }

        ++idx;
        g_tasks_soft[idx].max_execution_cycles = 0;
        g_tasks_soft[idx].min_execution_cycles = UINT_MAX;
        g_tasks_soft[idx].max_latency_ms = 0;
        ++idx;
    }

    return;
} /* schedule_print_summary */

/**
 * @function schedule
 * @detail   Cooperative scheduler. This function executes all persistent
 *           tasks followed by any 
 */
void schedule(void)
{
    int      idx = 0;
    SYSTICK  dtime = Sys.MillisecTime() - m_schedTime;
    SYSCYCLE taskcycles;

    while (0 != g_tasks_persist[idx].task)
    {
        taskcycles = Sys.CycleCounter();
        g_tasks_persist[idx].task();
        taskcycles = Sys.CycleCounter() - taskcycles;

        if (taskcycles > g_tasks_persist[idx].max_execution_cycles)
        {
            g_tasks_persist[idx].max_execution_cycles = taskcycles;
        }

        if (taskcycles < g_tasks_persist[idx].min_execution_cycles)
        {
            g_tasks_persist[idx].min_execution_cycles = taskcycles;
        }

        ++idx;
    }

    /* execute soft tasks */
    if (0 < dtime)
    {
        idx = 0;

        while (0 != g_tasks_soft[idx].task)
        {
            if (dtime < g_tasks_soft[idx].interval_counter)
            {
                g_tasks_soft[idx].interval_counter -= dtime;
            }
            else
            {
                SYSTICK latency =
                        (Sys.MillisecTime() - m_schedTime) - g_tasks_soft[idx].interval_counter;
                taskcycles = Sys.CycleCounter();
                g_tasks_soft[idx].interval_counter = g_tasks_soft[idx].task();
                taskcycles = Sys.CycleCounter() - taskcycles;

                /* if interval counter is zero then use the default interval */
                if (!g_tasks_soft[idx].interval_counter)
                {
                    g_tasks_soft[idx].interval_counter = g_tasks_soft[idx].interval_default;
                }

                /* update performance data */
                if (taskcycles > g_tasks_soft[idx].max_execution_cycles)
                {
                    g_tasks_soft[idx].max_execution_cycles = taskcycles;
                }

                if (taskcycles < g_tasks_soft[idx].min_execution_cycles)
                {
                    g_tasks_soft[idx].min_execution_cycles = taskcycles;
                }

                if (latency > g_tasks_soft[idx].max_latency_ms)
                {
                    g_tasks_soft[idx].max_latency_ms = latency;
                }
            }

            ++idx;
        }
    }

    return;
} /* schedule */
