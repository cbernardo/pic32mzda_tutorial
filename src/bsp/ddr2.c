/**
 * File : ddr2.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : DDR2 controller initialization.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <xc.h>
#include "ddr2.h"
#include "errors.h"
#include "sysutil.h"


/*******************************************************************************
 *              PRIVATE DEFINITONS
 ******************************************************************************/

/* 200 MHz timing; ref 60001321C */
#define TIMING_RFC     (0x0F906000UL)
#define TIMING_RP      (0x0222E000UL)
#define TIMING_MRD     (0x01388000UL)
#define TIMING_CK      (0x55730000UL)

/* Controller commands */
#define CMD_IDLE       (0x00FFFFFFUL)
#define CMD_PRECH_ALL  (0x00FFF401UL)
#define CMD_REFRESH    (0x00FFF801UL)
#define CMD_LOAD_MODE  (0x00FFF001UL)
#define CMD_CKE_LOW    (0x00FFEFFEUL);


/*******************************************************************************
 *              PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/

static int32_t  ddr2_init(void);
static int32_t  ddr2_uninit(void);
static bool     ddr2_ready(void);
static void     ddr2_config_pads(void);
static void     ddr2_config_cal(void);
static void     ddr2_config_addressing(void);
static void     ddr2_config_controller(void);
static void     ddr2_config_targets(void);


/*******************************************************************************
 *              PUBLIC API
 ******************************************************************************/

DrvDDR2_t ddr =
{
    .Init = ddr2_init,
    .UnInit = ddr2_uninit,
    .Ready = ddr2_ready,
};


/*******************************************************************************
 *              PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/

static int32_t  ddr2_init(void)
{
    int32_t result = 0;

    if (0 != (PMD7 & (1 << 28)))
    {
        /* module is not powered */
        result = ENODEV;
    }

    if (0 == result)
    {
        CFGMPLLbits.INTVREFCON = 0;         /* external DDRVRef */
        for (int i = 0; i < 100; ++i);      /* delay to allow config to take effect */
        CFGMPLLbits.MPLLVREGDIS = 0;        /* enable DDR2 regulator */

        while (0 == CFGMPLLbits.MPLLVREGRDY)
        {
            ;   /* wait for regulator to stabilize */
        }

        /* set PLL output to 200MHz */
        CFGMPLLbits.MPLLIDIV = 1;
        CFGMPLLbits.MPLLMULT = 50;
        CFGMPLLbits.MPLLODIV1 = 6;
        CFGMPLLbits.MPLLODIV2 = 1;
        CFGMPLLbits.MPLLDIS = 0;            /* enable PLL */

        while (0 == CFGMPLLbits.MPLLRDY)
        {
            ;   /* wait for PLL to stabilize */
        }

        /* give controller time to initialize */
        Sys.WaitMicrosec(700);
        ddr2_config_pads();
        ddr2_config_cal();
        ddr2_config_addressing();
        ddr2_config_controller();
        ddr2_config_targets();

        /* ddr2 initialization sequence */
        DDRCMD10 = CMD_IDLE;
        DDRCMD20 = 400000UL << 11;
        DDRCMD11 = CMD_PRECH_ALL;
        DDRCMD21 = TIMING_RP | 0x04UL;
        DDRCMD12 = CMD_LOAD_MODE;
        DDRCMD22 = TIMING_MRD | 0x200UL;
        DDRCMD13 = CMD_LOAD_MODE;
        DDRCMD23 = TIMING_MRD | 0x300UL;
        DDRCMD14 = CMD_LOAD_MODE | (0x40UL << 24);
        DDRCMD24 = TIMING_MRD | 0x100UL;
        DDRCMD15 = CMD_LOAD_MODE | (5UL << 28) | (2UL << 24);
        DDRCMD25 = TIMING_MRD | 0x03UL;
        DDRCMD16 = CMD_PRECH_ALL;
        DDRCMD26 = TIMING_RP | 0x04UL;
        DDRCMD17 = CMD_REFRESH;
        DDRCMD27 = TIMING_RFC;
        DDRCMD18 = CMD_REFRESH;
        DDRCMD28 = TIMING_RFC;
        DDRCMD19 = CMD_LOAD_MODE | (5UL << 28) | (3UL << 24);
        DDRCMD29 = TIMING_MRD | 0x03UL;
        DDRCMD110 = CMD_LOAD_MODE | (0x0CUL << 24);
        DDRCMD210 = TIMING_MRD | 0x103UL;
        DDRCMD111 = CMD_LOAD_MODE | (0x04UL << 28);
        DDRCMD211 = TIMING_CK | 0x100UL;
        DDRCMDISSUEbits.NUMHOSTCMDS = 12;
        DDRCMDISSUEbits.VALID = 1;
        DDRMEMCON = 1UL;

        while (DDRCMDISSUEbits.VALID)
        {
            ;   /* wait for initialization to complete */
        }

        /* initiate self-calibration */
        DDRMEMCON = 3UL;
        DDRSCLSTARTbits.SCLEN = 1;
        DDRSCLSTARTbits.SCLSTART = 1;

        while (0x03UL != (DDRSCLSTART & 0x03UL))
        {
            ;   /* wait for SCLUBPASS + SCLLBPASS */
        }
    }

    return result;    
}

static int32_t  ddr2_uninit(void)
{
    int32_t result = 0;
    /* Note: DDR2 cannot be shut down since the controller requires a system reset
     * in order to reinitialize.
     */
    return result;    
} /* ddr2_init */


static bool  ddr2_ready(void)
{
    bool result = false;

    if ((0 == (PMD7 & (1 << 28))) && (3UL == (DDRMEMCON & 3UL)))
    {
        result = true;
    }

    return result;    
} /* ddr2_ready */


static void  ddr2_config_pads(void)
{
    DDRPHYPADCONbits.PREAMBDLY = 2;     /* 1 cycle preamble */
    DDRPHYPADCONbits.RCVREN = 1;        /* enable bidirectional I/O pad receivers */
    DDRPHYPADCONbits.DRVSTRPFET = 14;   /* near-max PFET drive strength */
    DDRPHYPADCONbits.DRVSTRNFET = 14;   /* near-max NFET drive strength */
    DDRPHYPADCONbits.HALFRATE = 1;      /* use half-rate transfers; per data sheet must be 1 */
    DDRPHYPADCONbits.WRCMDDLY = 1;      /* must be set to 1 if Write Latency (WL) is even */
    DDRPHYPADCONbits.NOEXTDLL = 1;      /* enable internal digital delay-lock-loop */
    DDRPHYPADCONbits.EOENCLKCYC = 0;    /* if 1 then enabled drive pad output for an additional cycle after a write */
    DDRPHYPADCONbits.ODTPUCAL = 3;      /* 0 = MIN, 3= MAX on-die-termination impedance during calibration */
    DDRPHYPADCONbits.ODTPDCAL = 2;      /* 0 = MIN, 3= MAX on-die-termination impedance during calibration */
    DDRPHYPADCONbits.ADDCDRVSEL = 0;    /* address and control pad drive strength (0=60%, 1=100%) */
    DDRPHYPADCONbits.ODTEN = 1;         /* enable on-die termination */
    DDRPHYPADCONbits.ODTSEL = 1;        /* 0=75 ohm, 1=150 ohm on-die termination */
    DDRPHYPADCONbits.DATDRVSEL = 0;

    /* enable Controller On-Die-Termination:
     * a. ODTCFG.ODTCSEN must be programmed with Chip Select# * Total Chip Selects
     * b. set ODTENCFG.ODTREN /  ODTENCFG.ODTWEN
     */
    DDRODTCFGbits.ODTCSEN = 0;
    DDRODTENCFGbits.ODTREN = 0;
    DDRODTCFGbits.ODTCSEN = 0;
    DDRODTENCFGbits.ODTWEN = 1;
    DDRODTCFGbits.ODTWLEN = 3;
    DDRODTCFGbits.ODTWDLY = 1;
    return;
} /* ddr2_config_pads */


static void  ddr2_config_cal(void)
{
    DDRPHYDLLRbits.DLYSTVAL = 3;    /* recommended value for digital DLL master delay */
    DDRPHYDLLRbits.DISRECALIB = 0;  /* 0: recalibrate DLL according to <RECALIBCNT>  */
    DDRPHYDLLRbits.RECALIBCNT = 16; /* recalibrate DLL interval every 256 * PHY Clock Cycles */

    DDRSCLCFG1bits.SCLCSEN = 1;
    DDRSCLCFG0bits.BURST8 = 1;
    DDRSCLCFG0bits.DDR2 = 1;
    DDRSCLCFG0bits.ODTCSW = 1;
    DDRSCLCFG0bits.RCASLAT = 5;
    DDRSCLCFG1bits.WCASLAT = 4;
    DDRSCLCFG1bits.DBLREFDLY = 0;
    DDRSCLLATbits.DDRCLKDLY = 4;
    DDRSCLLATbits.CAPCLKDLY = 3;
    return;
} /* ddr2_config_cal */


static void  ddr2_config_addressing(void)
{
    DDRMEMCFG0bits.RWADDR = 11;
    DDRMEMCFG1bits.RWADDRMSK = (1 << 13) - 1;
    DDRMEMCFG0bits.CLHADDR = 0;
    DDRMEMCFG3bits.CLADDRLMSK = (1 << 9) - 1;
    DDRMEMCFG2bits.CLADDRHMSK = 0;
    DDRMEMCFG0bits.BNKADDR = 9;
    DDRMEMCFG4bits.BNKADDRMSK = 0x03;
    DDRMEMCFG0bits.CSADDR = 0;
    DDRMEMCFG4bits.CSADDRMSK = 0;
    DDRMEMCFG0bits.APCHRGEN = 0;
    return;
} /* ddr2_config_addressing */


static void  ddr2_config_controller(void)
{
    /* Refresh Control Register */
    DDRREFCFGbits.REFCNT = 778;
    DDRREFCFGbits.REFDLY = 11;
    DDRREFCFGbits.MAXREFS = 7;

    /* Power Control Register */
    DDRPWRCFGbits.ASLFREFEN = 0;
    DDRPWRCFGbits.APWRDNEN = 0;
    DDRPWRCFGbits.PCHRGPWRDN = 0;
    DDRPWRCFGbits.SLFREFDLY = 17;
    DDRPWRCFGbits.PWRDNDLY = 8;

    /* Delay Control Register 0 */
    DDRDLYCFG0bits.R2WDLY = 4;
    DDRDLYCFG0bits.RMWDLY = 4;
    DDRDLYCFG0bits.W2RDLY = 8;
    DDRDLYCFG0bits.W2RCSDLY = 1;
    DDRDLYCFG0bits.R2RDLY = 1;
    DDRDLYCFG0bits.R2RCSDLY = 1;
    DDRDLYCFG0bits.W2WDLY = 1;
    DDRDLYCFG0bits.W2WCSDLY = 1;

    /* Delay Control Register 1 */
    DDRDLYCFG1bits.W2RDLY4 = 0;
    DDRDLYCFG1bits.W2RCSDLY4 = 0;
    DDRDLYCFG1bits.SLFREFMINDLY = 2;
    DDRDLYCFG1bits.SLFREFEXDLY = 98;
    DDRDLYCFG1bits.SLFREFEXDLY8 = 0;
    DDRDLYCFG1bits.PWRDNMINDLY = 2;
    DDRDLYCFG1bits.PWRDNEXDLY = 2;
    DDRDLYCFG1bits.W2PCHRGDLY4 = 0;
    DDRDLYCFG1bits.NXTDATAVDLY4 = 0;

    /* Delay Control Register 2 */
    DDRDLYCFG2bits.PCHRGALLDLY = 3;
    DDRDLYCFG2bits.R2PCHRGDLY = 2;;
    DDRDLYCFG2bits.W2PCHRGDLY = 9;
    DDRDLYCFG2bits.PCHRG2RASDLY = 2;
    DDRDLYCFG2bits.RAS2RASDLY = 1;
    DDRDLYCFG2bits.RAS2CASDLY = 2;
    DDRDLYCFG2bits.RBENDDLY = 8;

    /* Delay Control Register 3 */
    DDRDLYCFG3bits.RAS2PCHRGDLY = 8;
    DDRDLYCFG3bits.RAS2RASSBNKDLY = 11;
    DDRDLYCFG3bits.FAWTDLY = 6;

    /* Transfer Control Register 3 */
    DDRXFERCFGbits.NXTDATRQDLY = 2;
    DDRXFERCFGbits.NXTDATAVDLY = 4;
    DDRXFERCFGbits.RDATENDLY = 2;

    /* Analog Delay Loop Bypass */
    DDRADLLBYPbits.ANLDLLBYP = 1;

    return;
} /* ddr2_config_controller */


static void  ddr2_config_targets(void)
{
    /* Target 0 : MCU access */
    DDRTSELbits.TSEL = 0;
    DDRMINLIMbits.MINLIMIT = 0x1F;
    DDRTSELbits.TSEL = 0;
    DDRRQPERbits.RQPER = 0xFF;
    DDRTSELbits.TSEL = 0;
    DDRMINCMDbits.MINCMD = 4;

    /* Target 1 : DMA and Peripheral access */
    DDRTSELbits.TSEL = 5;
    DDRMINLIMbits.MINLIMIT = 0x1F;
    DDRTSELbits.TSEL = 8;
    DDRRQPERbits.RQPER = 0xFF;
    DDRTSELbits.TSEL = 8;
    DDRMINCMDbits.MINCMD = 0x10;

    /* Target 2 : DMA and Peripheral access */
    DDRTSELbits.TSEL = 10;
    DDRMINLIMbits.MINLIMIT = 0x1F;
    DDRTSELbits.TSEL = 16;
    DDRRQPERbits.RQPER = 0xFF;
    DDRTSELbits.TSEL = 16;
    DDRMINCMDbits.MINCMD = 0x10;

    /* Target 3 : LCD controller and GPU access */
    DDRTSELbits.TSEL = 15;
    DDRMINLIMbits.MINLIMIT = 4;
    DDRTSELbits.TSEL = 24;
    DDRRQPERbits.RQPER = 0xFF;
    DDRTSELbits.TSEL = 24;
    DDRMINCMDbits.MINCMD = 4;

    /* Target 4 : LCD controller and GPU access */
    DDRTSELbits.TSEL = 20;
    DDRMINLIMbits.MINLIMIT = 4;
    DDRTSELbits.TSEL = 32;
    DDRRQPERbits.RQPER = 0xFF;
    DDRTSELbits.TSEL = 32;
    DDRMINCMDbits.MINCMD = 4;

    return;
} /* ddr2_config_targets */
