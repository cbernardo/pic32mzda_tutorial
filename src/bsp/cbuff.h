/**
 * File : cbuff.h
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : circular character buffer.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <stdint.h>

typedef struct
{
    int      size;
    int      mask;
    int      head;
    int      tail;
    uint8_t  data[1024];
} CBuff_t;

int      cbuf_Init(CBuff_t *buffer);
int      cbuf_PushByte(CBuff_t *buffer, int     const token);
int      cbuf_PushString(CBuff_t *buffer, void const *source, int const sourceLength);
int      cbuf_PeekByte(CBuff_t const *buffer, void *token);
int      cbuf_PopByte(CBuff_t *buffer, void *token);
int      cbuf_PopString(CBuff_t *buffer, void *dest, int const destLength,
                        int *stringLength);
int      cbuf_GetUnusedBytes(CBuff_t const *buffer);
int      cbuf_GetUsedBytes(CBuff_t const *buffer);

#endif /* CIRCULAR_BUFFER_H */
