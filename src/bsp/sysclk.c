/**
 * File : sysclk.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides control of the System Clock.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>

#include "cfg_clocks.h"
#include "sysclk.h"


/*******************************************************************************
 *              PRIVATE MACRO DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *              PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/
static uint32_t  sysclk_getfrequency(void);
static uint32_t  sysclk_getpllfrequency(void);


/*******************************************************************************
 *              PUBLIC API
 ******************************************************************************/
DrvSysClk_t SysClk =
{
    .GetFrequency = sysclk_getfrequency,
    .GetSPLLFrequency = sysclk_getpllfrequency,
};


/*******************************************************************************
 *              PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/
static uint32_t  sysclk_getfrequency(void)
{
    uint32_t result = 0;

    switch (OSCCONbits.COSC)
    {
    case 0: /* Internal Fast RC divided by FRCDIV, 5 to 8% */
    {
        uint32_t divisor = (1UL << OSCCONbits.FRCDIV);

        if (128 == divisor)
        {
            divisor <<= 1UL;
        }

        result = (CFG_FRC_FREQ) / divisor;
        break;
    }

    case 1: /* System Clock PLL (SPLL) */
    case 7:
    {
        result = sysclk_getpllfrequency();
        break;
    }

    case 2: /* Primary Oscillator (High Speed XTAL or External Clock) */
    {
        result = (CFG_POSC_FREQ);
        break;
    }

    case 4: /* Secondary Oscillator */
    {
        result = (CFG_SOSC_FREQ);
        break;
    }

    case 5: /* Internal Low-Power RC (LPRC) 31.25KHz, 8 to 25% */
    {
        result = (CFG_LPRC_FREQ);
        break;
    }

    case 6: /* Backup Fast RC (BFRC) 8MHZ, 30% */
    {
        result = (CFG_BFRC_FREQ);
        break;
    }

    default:
    {
        /* invalid/unhandled setting */
        break;
    }
    }

    return result;
} /* sysclk_getfrequency */


static uint32_t  sysclk_getpllfrequency(void)
{
    uint32_t result;
    uint32_t srcClock = (CFG_FRC_FREQ);

    if (0UL == SPLLCONbits.PLLICLK)
    {
        srcClock = (CFG_POSC_FREQ);
    }

    /* Note: MAX 64MHz input * MAX 128 multiplier does not overflow uint32_t */
    srcClock *= (SPLLCONbits.PLLMULT + 1UL);

    uint32_t divisor = (SPLLCONbits.PLLIDIV + 1UL);
    uint32_t odiv = SPLLCONbits.PLLODIV;

    if ((1UL <= odiv) && (5UL >= odiv))
    {
        divisor <<= odiv;
    }
    else
    {
        /* unhandled configuration */
        srcClock = 0UL;
        divisor = 1UL;
    }

    result = srcClock / divisor;
    return result;
} /* sysclk_getpllfrequency */
