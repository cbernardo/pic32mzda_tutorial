/**
 * File : pbclk.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides control of the Peripheral Bus Clocks.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DRV_PBCLK_H
#define DRV_PBCLK_H

#include <stdbool.h>
#include <stdint.h>

#include "cfg_clocks.h"

typedef struct
{
    /**
     * @function Enable
     * @detail   enables or disables the clock; not applicable to PbClk1.
     * @param    enable [IN] true to enable the clock.
     * @return   0 on success, else -1.
     */
    int       (*Enable)(bool const enable);

    /**
     * @function Enabled
     * @return   true if the clock is enabled.
     */
    bool      (*Enabled)(void);

    /**
     * @function GetFrequency
     * @detail   Returns the clock frequency in Hertz. In the event of an unhandled
     *           configuration setting the return value will be 0.
     */
    uint32_t  (*GetFrequency)(void);

    /**
     * @function SetDivisor
     * @param    div [IN] is the divisor; range is 1..128
     * @return   0 on success; -1 
     */
    int       (*SetDivisor)(uint32_t const div);
} const DrvPbClk_t;


#if defined(CFG_USE_PBCLK_1) && (0 != CFG_USE_PBCLK_1)
extern DrvPbClk_t PbClk1;
#endif

#if defined(CFG_USE_PBCLK_2) && (0 != CFG_USE_PBCLK_2)
extern DrvPbClk_t PbClk2;
#endif

#if defined(CFG_USE_PBCLK_3) && (0 != CFG_USE_PBCLK_3)
extern DrvPbClk_t PbClk3;
#endif

#if defined(CFG_USE_PBCLK_4) && (0 != CFG_USE_PBCLK_4)
extern DrvPbClk_t PbClk4;
#endif

#if defined(CFG_USE_PBCLK_5) && (0 != CFG_USE_PBCLK_5)
extern DrvPbClk_t PbClk5;
#endif

#if defined(CFG_USE_PBCLK_6) && (0 != CFG_USE_PBCLK_6)
extern DrvPbClk_t PbClk6;
#endif

#if defined(CFG_USE_PBCLK_7) && (0 != CFG_USE_PBCLK_7)
extern DrvPbClk_t PbClk7;
#endif

#endif /* DRV_PBCLK_H */
