/**
 * File : memutil.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Implements the Cache management and Address Conversion API.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdint.h>
#include "memutil.h"

/*
 * Memory Ranges:
 * 00000000 - 3FFFFFFF (  1GiB) : Physical Memory
 * 80000000 - 9FFFFFFF (512MiB) : KSEG 0 (non-cached)
 * A0000000 - BFFFFFFF (512MiB) : KSEG 1 (cache-able)
 * C0000000 - D3FFFFFF (512MiB) : KSEG 2 (non-cached, EBI/SQI)
 * E0000000 - FFFFFFFF (512MiB) : KSEG 3 (non-cached, EBI/SQI)
 *
 * Physical Memory:
 * 00000000 - 0009FFFF          : SRAM, 640KiB
 * 000A0000 - 07FFFFFF          : [RESERVED] end of 640KiB SRAM to start of 32MiB DDR2
 * 08000000 - 09FFFFFF          : DDR2, 32MiB
 * 0A000000 - 1CFFFFFF          : [RESERVED] end of 32MiB DDR2 to start of Program Flash
 * 1D000000 - 1D1FFFFF          : Program Flash, 2MiB
 * 1D200000 - 1F7FFFFF          : [RESERVED] end of 2MiB Program Flash to start of SFRs
 * 1F800000 - 1F8FFFFF          : Special Function Registers (SFRs)
 * 1F900000 - 1FBFFFFF          : [RESERVED] end of SFRs to start of Boot Flash Block
 * 1FC00000 - 1FC13FFF          : Lower Boot Alias
 * 1FC14000 - 1FC1FFFF          : [RESERVED] end of Lower Boot Alias to start of Upper Boot Alias
 * 1FC20000 - 1FC33FFF          : Upper Boot Alias
 * 1FC34000 - 1FC3FFFF          : [RESERVED] end of Upper Boot Alias to start of Boot Flash 1
 * 1FC40000 - 1FC5402B          : Boot Flash 1 and Serial Number
 * 1FC5402C - 1FC5FFFF          : [RESERVED] end of 128-bit Serial Number to start of Boot Flash 2
 * 1FC60000 - 1FC73FFF          : Boot Flash 2
 * 1FC74000 - 1FFFFFFF          : [RESERVED] end of Boot Flash Block to start of EBI External Mem
 * 20000000 - 23FFFFFF          : EBI External Memory, 64 MiB
 * 24000000 - 2FFFFFFF          : [RESERVED] end of EBI External Mem to start of SQI External Mem
 * 30000000 - 33FFFFFF          : SQI External Memory, 64 MiB
 * 34000000 - 7FFFFFFF          : [RESERVED] end of SQI External Mem to start of Virtual Mem
 *
 * Virtual Memory, KSEG0
 * Similar to Physical Memory with an offset of 80000000 but with a RESERVED range
 * corresponding to the Special Function Registers.
 *
 * Virtual Memory, KSEG1
 * Similar to Physical Memory with an offset of A0000000 but with a RESERVED range
 * corresponding to the Special Function Registers.
 */

/*******************************************************************************
 *              Private Macros
 ******************************************************************************/
#define CFG_MEM_PHYS_START      (0x00000000UL)
#define CFG_MEM_PHYS_END        (0x7FFFFFFFUL)
#define CFG_MEM_SEG0_START      (0x80000000UL)
#define CFG_MEM_SEG0_END        (0x9FFFFFFFUL)
#define CFG_MEM_SEG1_START      (0xA0000000UL)
#define CFG_MEM_SEG1_END        (0xBFFFFFFFUL)
#define CFG_MEM_SEG2_START      (0xC0000000UL)
#define CFG_MEM_SEG2_END        (0xDFFFFFFFUL)
#define CFG_MEM_SEG3_START      (0xE0000000UL)
#define CFG_MEM_SEG3_END        (0xFFFFFFFFUL)
/* lower address bytes defining built-in regions (not EBI nor SQI) */
#define CFG_MEM_RAM_START       (0x00000000UL)
#define CFG_MEM_RAM_END         (0x009FFFFFUL)  /* 640KiB memory */
#define CFG_MEM_DDR_START       (0x08000000UL)
#define CFG_MEM_DDR_END         (0x09FFFFFFUL)
#define CFG_MEM_PROG_START      (0x1D000000UL)
#define CFG_MEM_PROG_END        (0x1D1FFFFFUL)  /* 2MiB Program Flash */
/* note: since it is unlikely that operations will be performed within
 * the boot flash regions, the entire boot flash memory region is
 * treated as a valid address range for cache operations
 */
#define CFG_MEM_BOOT_START      (0x1FC00000UL)
#define CFG_MEM_BOOT_END        (0x1FC73FFFUL)



/*******************************************************************************
 *              Private Function Declarations
 ******************************************************************************/
int   mem_Evict(void const * const address);
int   mem_EvictRange(void const * const address, uint32_t const bytes);
int   mem_Flush(void const * const address);
int   mem_FlushRange(void const * const address, uint32_t const bytes);
void *mem_GetCachedAddress(void const * const address);
void *mem_GetNonCachedAddress(void const * const address);
void *mem_GetPhysicalAddress(void const * const address);
int   mem_GetCachedRange(void const *address, uint32_t const bytes,
                         uintptr_t *rangeStart, uintptr_t *rangeEnd);


/*******************************************************************************
 *              Memory Utilities API
 ******************************************************************************/
Memory_t const Mem =
{
    .Evict = mem_Evict,
    .EvictRange = mem_EvictRange,
    .Flush = mem_Flush,
    .FlushRange = mem_FlushRange,
    .GetCachedAddress = mem_GetCachedAddress,
    .GetNonCachedAddress = mem_GetNonCachedAddress,
    .GetPhysicalAddress = mem_GetPhysicalAddress,
};


/*******************************************************************************
 *              Private Function Definitions
 ******************************************************************************/

/**
 * @function mem_Evict
 * @detail   Evicts the Data Cache line containing the given address; the address
 *           will be converted into a virtual cacheable address if it is a
 *           physical or a non-cacheable address. Reserved address ranges and
 *           Flash memory ranges are deliberately excluded from D-Cache evictions.
 *
 * @param    address [IN] virtual cached address to evict.
 * @return   0 on success, else -1.
 */
int  mem_Evict(void const * const address)
{
    int   result = 0;
    char *vaddr = (char *) mem_GetCachedAddress(address);

    if ((CFG_MEM_INVALID_ADDR) != (uintptr_t) vaddr)
    {
        // evict the address and sync before proceeding
        _cache(0x11, vaddr);
        _sync();
    }
    else
    {
        result = -1;
    }

    return result;
} /* mem_Evict */

/**
 * @function mem_EvictRange
 * @detail   Evicts the cache lines containing the
 *           <b>bytes</b> starting at the given <b>address</b>.
 *           The address may be a Physical, Cacheable, or Non-Cacheable
 *           address within the Static RAM, DDR2, EBI, or SQI regions.
 *
 * @param    address [IN] virtual cached address to evict.
 * @param    bytes [IN] is the minimum number of bytes to be evicted.
 * @return   0 on success, else -1.
 */
int  mem_EvictRange(void const * const address, uint32_t const bytes)
{
    uintptr_t addrStart = 0;
    uintptr_t addrEnd = 0;
    int       result = mem_GetCachedRange(address, bytes, &addrStart, &addrEnd);

    if (0 == result)
    {
        while (addrStart <= addrEnd)
        {
            _cache(0x11, (void *) addrStart);
            addrStart += 0x10UL;
        }

        _sync();
    }

    return result;
}

/**
 * @function mem_Flush
 * @detail   Writes back and evicts the cache line containing the given address.
 *
 * @param    address [IN] virtual cached address to flush.
 * @return   0 on success, else -1.
 */
int  mem_Flush(void const * const address)
{
    int   result = 0;
    char *vaddr = (char *) mem_GetCachedAddress(address);

    if ((CFG_MEM_INVALID_ADDR) != (uintptr_t) vaddr)
    {
        // flush the address and sync before proceeding
        _cache(0x15, vaddr);
        _sync();
    }
    else
    {
        result = -1;
    }

    return result;
} /* mem_Flush */

/**
 * @function mem_FlushRange
 * @detail   Writes back and evicts the cache lines containing the
 *           <b>bytes</b> starting at the given <b>address</b>.
 *           The address may be a Physical, Cacheable, or Non-Cacheable
 *           address within the Static RAM, DDR2, EBI, or SQI regions.
 *
 * @param    address [IN] virtual cached address to flush.
 * @param    bytes [IN] is the minimum number of bytes to be flushed.
 * @return   0 on success, else -1.
 */
int  mem_FlushRange(void const * const address, uint32_t const bytes)
{
    uintptr_t addrStart = 0;
    uintptr_t addrEnd = 0;
    int       result = mem_GetCachedRange(address, bytes, &addrStart, &addrEnd);

    if (0 == result)
    {
        while (addrStart <= addrEnd)
        {
            _cache(0x15, (void *) addrStart);
            addrStart += 0x10UL;
        }

        _sync();
    }

    return result;
}

/**
 * @function mem_GetCachedAddress
 * @detail   Calculates a cached address given a Virtual or Physical address.
 *           The address must be within one of the following mapped regions:
 *           - Static RAM
 *           - DDR memory
 *           - EBI memory
 *           - SQI memory
 *
 * @param    address [IN] virtual or physical address to translate.
 * @return   Cached address, or 0xFFFFFFFF on failure.
 */
void *mem_GetCachedAddress(void const * const address)
{
    uintptr_t vaddr = (uintptr_t) address;

    if ((CFG_MEM_PHYS_END) >= vaddr)
    {
        /* convert physical addresses to a virtual address;
         * set the upper byte according to bits 29:28
         */
        uintptr_t region = (vaddr & 0x30000000UL) >> 28;

        if (2 == region)
        {
            /* EBI memory; cacheable address has high byte 0xCn */
            vaddr = (vaddr & 0x0FFFFFFFUL) | 0xC0000000UL; 
        }
        else if (3 == region)
        {
            /* SQI memory; cacheable address has high byte 0xDn */
            vaddr = (vaddr & 0x0FFFFFFFUL) | 0xD0000000UL; 
        }
        else
        {
            /* built-in memory or DDR; cacheable address has high byte 0x8n */
            vaddr = (vaddr & 0x0FFFFFFFUL) | 0x80000000UL; 
        }
    }
    else
    {
        /* virtual address; clear bit 29 to obtain the cacheable region */
        vaddr = (vaddr & 0xDFFFFFFFUL) | 0x80000000UL; 
    }

    /* set error if virtual address is within a special range
     */
    if (0 != (vaddr & 0x40000000UL))
    {
        /* EBI or SQI; valid addresses are to to 0x03FFFFFF
         * Note: address translation does not take into account whether EBI or SQI
         * are used or correctly configured.
         */
        if ((vaddr & 0x0FFFFFFFUL) > 0x03FFFFFFUL)
        {
            vaddr = (CFG_MEM_INVALID_ADDR);
        }
    }
    else
    {
        /* built-in or DDR address
         * Note: DDR address access is mapped here even if DDR is not active and initialized
         */
        uintptr_t lowerAddr = vaddr & 0x1FFFFFFFUL;

        if (((lowerAddr > (CFG_MEM_RAM_END)) && (lowerAddr < (CFG_MEM_DDR_START)))
        || (lowerAddr > (CFG_MEM_DDR_END)))
        {
            vaddr = (CFG_MEM_INVALID_ADDR);
        }
    }

    return (void *) vaddr;
} /* mem_GetCachedAddress */

/**
 * @function mem_GetNonCachedAddress
 * @detail   Calculates a non-cached address given a Virtual or Physical address.
 *
 * @param    address [IN] virtual or physical address to translate.
 * @return   Non-cached address, or 0xFFFFFFFF on failure.
 */
void *mem_GetNonCachedAddress(void const * const address)
{
    uintptr_t vaddr = (uintptr_t) address;

    if ((CFG_MEM_PHYS_END) >= vaddr)
    {
        /* convert physical addresses to a virtual address;
         * set the upper byte according to bits 29:28
         */
        uintptr_t region = (vaddr & 0x30000000UL) >> 28;

        if (2 == region)
        {
            /* EBI memory; non-cacheable address has high byte 0xEn */
            vaddr = (vaddr & 0x0FFFFFFFUL) | 0xE0000000UL;
        }
        else if (3 == region)
        {
            /* SQI memory; cacheable address has high byte 0xFn */
            vaddr = (vaddr & 0x0FFFFFFFUL) | 0xF0000000UL;
        }
        else
        {
            /* built-in memory or DDR; non-cacheable address has high byte 0xAn */
            vaddr = (vaddr & 0x0FFFFFFFUL) | 0xA0000000UL;
        }
    }
    else
    {
        /* virtual address; set bits 31 and 29 to obtain the non-cacheable region */
        vaddr = (vaddr | 0xA0000000UL);
    }

    /* set error if virtual address is within a special range, Boot, or Program flash
     */
    if (0 != (vaddr & 0x40000000UL))
    {
        /* EBI or SQI; valid addresses are to to 0x03FFFFFF
         * Note: address translation does not take into account whether EBI or SQI
         * are used or correctly configured.
         */
        if ((vaddr & 0x0FFFFFFFUL) > 0x03FFFFFFUL)
        {
            vaddr = (CFG_MEM_INVALID_ADDR);
        }
    }
    else
    {
        /* built-in or DDR address
         * Note: DDR address access is mapped here even if DDR is not active and initialized
         */
        uintptr_t lowerAddr = vaddr & 0x1FFFFFFFUL;

        if (((lowerAddr > (CFG_MEM_RAM_END)) && (lowerAddr < (CFG_MEM_DDR_START)))
        || (lowerAddr > (CFG_MEM_DDR_END)))
        {
            vaddr = (CFG_MEM_INVALID_ADDR);
        }
    }

    return (void *) vaddr;
} /* mem_GetNonCachedAddress */

/**
 * @function mem_GetPhysicalAddress
 * @detail   Calculates a physical address given a Virtual or Physical address.
 *           No checks are performed to ensure that the address is not a reserved address.
 *
 * @param    address [IN] virtual or physical address to translate.
 * @return   Physical address, or 0xFFFFFFFF on failure.
 */
void *mem_GetPhysicalAddress(void const * const address)
{
    uintptr_t vaddr = (uintptr_t) address;

    if ((CFG_MEM_PHYS_END) < vaddr)
    {
        if (0xE0000000UL <= vaddr)
        {
            /* KSEG3, SQI or EBI */
            vaddr &= 0x3FFFFFFFUL;
        }
        else if (0xC0000000UL <= vaddr)
        {
            /* KSEG2, SQI or EBI */
            vaddr = (vaddr & 0x1FFFFFFFUL) | 0x02000000UL;
        }
        else
        {
            /* KSEG0 or KSEG1 */
            vaddr &= 0x1FFFFFFFUL;
        }
    }

    return (void *) vaddr;
}

/**
 * @function mem_GetCachedRange
 * @detail   Calculates a Start and End value to a cacheable address range.
 *           The address must be a valid Static RAM, DDR2, EBI, or SQI address and
 *           for convenience may be specified as PHYS, cacheable, or non-cacheable.
 *
 * @param    address    [IN] first address in the range
 * @param    bytes      [IN] number of bytes (zero to 128MiB) 
 * @param    rangeStart [IN, OUT] pointer to receive starting cache address value
 * @param    rangeEnd   [IN, OUT] pointer to receive final cache address value
 * @return   0 on success, else -1.
 */
int   mem_GetCachedRange(void const *address, uint32_t const bytes,
                            uintptr_t *rangeStart, uintptr_t *rangeEnd)
{
    int      result = 0;
    /* note: cast to (char *) followed by cast to (uintptr_t) suppresses
     * erroneous warning of invalid cast of function return.
     */
    uintptr_t vaddr1 = (uintptr_t) ((char *) mem_GetCachedAddress(address));
    uintptr_t vaddr2 = (vaddr1 + bytes) - 1UL;

    /* note: 128MiB is taken as an upper limit to the number of bytes in the general case;
     * EBI and SQI addressing are limited by hardware to 64MiB blocks */
    if (((CFG_MEM_INVALID_ADDR) == vaddr1) || (vaddr2 < vaddr1)
    || (0 == bytes) || ((128UL << 20) < bytes)
    || (0 == rangeStart) || (0 == rangeEnd))
    {
        /* Error, one of:
         * - null pointer
         * - invalid address
         * - address overflow
         * - invalid number of bytes
         */
        result = -1;
    }
    else
    {
        /* align addresses to cache line */
        vaddr1 = vaddr1 & 0xFFFFFFF0UL;
        vaddr2 = vaddr2 & 0xFFFFFFF0UL;

        /* check that both addresses are within a contiguous block */
        uintptr_t tag1 = ((vaddr1 & 0xF0000000) >> 28);
        uintptr_t tag2 = ((vaddr2 & 0xF0000000) >> 28);

        if (tag1 != tag2)
        {
            /* addresses are in different regions */
            result = -1;
        }
        else if (0x08 == tag1)
        {
            /* RAM or DDR : note : RAM starts at PHY address 0x00000000 */
            if ((vaddr1 & 0x0FFFFFFFUL) <= (CFG_MEM_RAM_END))
            {
                /* vaddr1 is in RAM */
                if ((vaddr2 & 0x0FFFFFFFUL) > (CFG_MEM_RAM_END))
                {
                    /* vaddr2 is not in RAM */
                    result = -1;
                }
            }
            else if (((vaddr1 & 0x0FFFFFFFUL) >= (CFG_MEM_DDR_START))
            && ((vaddr1 & 0x0FFFFFFFUL) <= (CFG_MEM_DDR_END)))
            {
                /* vaddr1 is in DDR */
                if (((vaddr2 & 0x0FFFFFFFUL) < (CFG_MEM_DDR_START))
                || ((vaddr2 & 0x0FFFFFFFUL) > (CFG_MEM_DDR_END)))
                {
                    /* vaddr2 is not in DDR */
                    result = -1;
                }
            }
        }
        else if ((0x0C == tag1) || (0x0D == tag1))
        {
            /* EBI or SQI */
            if ((0x03FFFFFFUL < (vaddr1 & 0x0FFFFFFFUL))
            || (0x03FFFFFFUL < (vaddr2 & 0x0FFFFFFFUL)))
            {
                /* out of valid range */
                result = -1;
            }
        }
    }

    if (0 == result)
    {
        *rangeStart = vaddr1;
        *rangeEnd = vaddr2;
    }

    return result;
}
