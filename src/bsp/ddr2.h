/**
 * File : ddr2.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : DDR2 controller initialization.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */


#ifndef DDR2_H
#define DDR2_H

#include <stdbool.h>
#include <stdint.h>

/* *INDENT-OFF* */
typedef struct
{
    /* @function Init
     * @detail   Initializes the DDR2 controller and RAM
     * @return   0 on success, else error code.
     */
    int32_t (*Init)(void);

    /* @function UnInit
     * @detail   Currently has no effect; the DDR RAM or its controller cannot be
     *           shut down and reinitialized without a system reset.
     * @return   0 (always succeeds).
     */
    int32_t (*UnInit)(void);

    /* @function Ready
     * @return   true if the DDR RAM is ready for operation.
     */
    bool    (*Ready)(void);
} const DrvDDR2_t;
/* *INDENT-ON* */

extern DrvDDR2_t ddr;

#endif /* DDR2_H */
