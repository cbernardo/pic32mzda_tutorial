/**
 * File : refclk.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides control of the Reference Clocks.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DRV_REFCLK_H
#define DRV_REFCLK_H

#include <stdbool.h>
#include <stdint.h>


typedef enum
{
    REFCLK_SRC_SYS = 0,
    REFCLK_SRC_PB1 = 1,
    REFCLK_SRC_POSC = 2,
    REFCLK_SRC_FRC = 3,
    REFCLK_SRC_LPRC = 4,
    REFCLK_SRC_SOSC = 5,
    REFCLK_SRC_SPLL = 7,
    REFCLK_SRC_CLKIN = 8,
    REFCLK_SRC_BFRC = 9,
} RefClkSrc_e;

typedef struct
{
    /**
     * @function Enable
     * @param    enable [IN] true to enable the clock.
     * @return   0 on success, else -1.
     */
    int       (*Enable)(bool const enable);

    /**
     * @function Enabled
     * @return   true if the clock is enabled.
     */
    bool      (*Enabled)(void);

    /**
     * @function GetFrequency
     * @detail   Returns the clock frequency in Hertz. In the event of an unhandled
     *           configuration setting the return value will be 0.
     */
    uint32_t  (*GetFrequency)(void);

    /**
     * @function SetDivisor
     * @detail   Sets the divisor and trim registers; trim must be 0 for use with SQI or SDHC.
     *           The final fractional divisor is 2 * [div + (trim / 512)]. Trim has no effect
     *           if the divisor is zero, i.e. first 2 fractional divisors are 1, 1 + 1/512.
     * @param    div [IN] is the divisor divided by 2; range is 0..32767, 0 = divisor of 1.
     * @param    trim [IN] is the trim value; range is 0..511.
     * @return   0 on success; else -1
     */
    int   (*SetDivisor)(uint32_t const div, uint32_t const trim);

    /**
     * @function SetSourceClk
     * @param    sourceClk [IN] one of RefClkSrc_e
     * @return   0 on success; else -1
     */
    int   (*SetSourceClk)(uint32_t const sourceClk);

    /**
     * @function EnableClkOut
     * @param    enable [IN] true to enable the clock output to a pin; the user is
     *           responsible for ensuring that the output pin has been correctly
     *           configured and that the clock frequency is supported on the selected pin.
     * @return   0 on success; else -1
     */
    int   (*EnableClkOut)(bool const enable);
} const DrvRefClk_t;


extern DrvRefClk_t RefClk1; /* general purpose or SPI */
extern DrvRefClk_t RefClk2; /* general purpose or SQI (TRIM must be 0 for SQI) */
extern DrvRefClk_t RefClk3; /* general purpose or ADC */
extern DrvRefClk_t RefClk4; /* general purpose or SDHC */
extern DrvRefClk_t RefClk5; /* general purpose or LCD pixel clock */

#endif /* DRV_REFCLK_H */
