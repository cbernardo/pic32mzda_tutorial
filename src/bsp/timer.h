/**
 * File : timer.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides control of the Timer modules.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef TIMER_DRIVER_H
#define TIMER_DRIVER_H

#include <xc.h>
#include <sys/attribs.h>
#include <stdbool.h>
#include <stdint.h>

#include "interrupt_priorities.h"


typedef void (*TimerHandler_t)(void);

typedef struct
{
    /**
     * @function Enable
     * @param    enable [IN] true to enable the timer.
     * @return   0 = success
     *           ERR_PARAM = invalid period register (frequency not set within range)
     *           ERR_NODEV = 
     */
    int      (*Enable)(const bool enable);

    /**
     * @function Enabled
     * @return   true if timer is enabled.
     */
    bool     (*Enabled)(void);

    /**
     * @function SetISR
     * @detail   Sets the interrupt handler. The interrupt handler can be replaced while
     *           the timer is active. Use a value of 0 to remove the interrupt handler.
     * @param    isr [IN] is a pointer to the function to execute on an interrupt.
     */
    void     (*SetISR)(void (*isr)(void));

    /**
     * @function SetFrequency
     * @detail   Sets the frequency to the nearest value not in excess of the specified frequency.
     * @param    frequency [IN] is the desired timer frequency.
     * @return   0 on success, else ERR_FAIL.
     */
    int      (*SetFrequency)(uint32_t const frequency);

    /**
     * @function GetFrequency
     * @param    nominalFreq [IN] is a pointer to receive the nominal frequency setting.
     * @param    actualFreq [IN] is a pointer to receive the actual timer frequency.
     * @return   0 on success, else ERR_FAIL.
     */
    int      (*GetFrequency)(uint32_t *nominalFreq, uint32_t *actualFreq);

    /**
     * @function GetPeriod
     * @return   the period register value + 1.
     */
    uint32_t (*GetPeriod)(void);

    /**
     * @function SetOffset
     * @param    offset [IN] is the initial value of the timer register when first enabled.
     * @return   0 on success, else ERR_FAIL.
     */
    int      (*SetOffset)(uint32_t const offset);
} const DrvTimer_t;

extern DrvTimer_t Timer1;   /* system millisecond timer */
extern DrvTimer_t Timer2;   /* general purpose timer */
extern DrvTimer_t Timer3;   /* general purpose timer */
extern DrvTimer_t Timer4;   /* general purpose timer */
extern DrvTimer_t Timer5;   /* general purpose timer */
extern DrvTimer_t Timer6;   /* general purpose timer */
extern DrvTimer_t Timer7;   /* general purpose timer */
extern DrvTimer_t Timer8;   /* general purpose timer */
extern DrvTimer_t Timer9;   /* general purpose timer */

#endif /* TIMER_DRIVER_H */
