/**
 * File : sysutil.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Miscellaneous system tools.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <xc.h>
#include <cp0defs.h>
#include <stdbool.h>
#include <stdint.h>

#include "sysclk.h"
#include "sysutil.h"
#include "timer.h"


/*******************************************************************************
 *              EXTERNAL DECLARATIONS
 ******************************************************************************/
extern void _reset(void);


/*******************************************************************************
 *              PRIVATE VARIABLES
 ******************************************************************************/
static volatile SYSTICK  m_millisectime;
static bool              m_initialized;

/*******************************************************************************
 *              PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/

static void      sys_init(void);
static void      sys_uninit(void);
static SYSCYCLE  sys_cyclecounter(void);
static SYSTICK   sys_millisectime(void);
static void      sys_waitmicrosec(unsigned int delay);
static void      sys_waitmillisec(unsigned int delay);
static bool      sys_timeoutmicrosec(unsigned int const start, unsigned int const microsec);
static bool      sys_timeoutmillisec(unsigned int const start, unsigned int const millisec);
static unsigned int  sys_disableie(void);
static void      sys_enableie(unsigned int const enable);
static void      __attribute__((__noreturn__)) sys_reset(void);
static void      sys_isr_millisec(void);


/*******************************************************************************
 *              PUBLIC API
 ******************************************************************************/

DrvSys_t Sys =
{
    .Init = sys_init,
    .UnInit = sys_uninit,
    .CycleCounter = sys_cyclecounter,
    .MillisecTime = sys_millisectime,
    .WaitMicrosec = sys_waitmicrosec,
    .WaitMillisec = sys_waitmillisec,
    .TimeoutMicrosec = sys_timeoutmicrosec,
    .TimeoutMillisec = sys_timeoutmillisec,
    .DisableInterrupts = sys_disableie,
    .EnableInterrupts = sys_enableie,
    .Reset = sys_reset,
};


/*******************************************************************************
 *              PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/

/**
 * @function sys_isr_millisec
 * @detail   Increments the system millisecond timer.
 */
static void  sys_isr_millisec(void)
{
    ++m_millisectime;
    return;
}

/**
 * @function sys_init
 * @detail   Initializes the resources used by the system utilities.
 */
static void  sys_init(void)
{
    if (!m_initialized)
    {
        /* Enable CP0 Cycle Counter (clear bit 27 of CAUSE) */
        unsigned int cause = _CP0_GET_CAUSE();
        cause &= ~(1UL << 27);
        _CP0_SET_CAUSE(cause);
        _ehb();

        /* Initialize the system millisecond timer */
        m_millisectime = 0;
        Timer1.SetFrequency(1000UL);
        Timer1.SetISR(sys_isr_millisec);
        Timer1.Enable(true);

        m_initialized = true;
    }

    return;
} /* sys_init */


static void  sys_uninit(void)
{
    if (m_initialized)
    {
        /* Disable interrupts and retrieve current global IE flag */
        unsigned int intActive = __builtin_disable_interrupts();

        /* Disable CP0 Cycle Counter (set bit 27 of CAUSE) */
        unsigned int cause = _CP0_GET_CAUSE();
        cause |= (1UL << 27);
        _CP0_SET_CAUSE(cause);
        _ehb();

        /* Disable system millisecond timer */
        Timer1.SetISR(0);
        Timer1.Enable(false);
        m_millisectime = 0;
        m_initialized = false;

        /* Enable interrupts if required */
        if (0 != intActive)
        {
            __builtin_enable_interrupts();
        }
    }

    return;
} /* sys_uninit */


static SYSCYCLE sys_cyclecounter(void)
{
    return _CP0_GET_COUNT();
} /* sys_cyclecounter */


static SYSTICK sys_millisectime(void)
{
    return m_millisectime;
} /* sys_millisectime */


static void  sys_waitmicrosec(unsigned int delay)
{
    if (m_initialized)
    {
        /* note: Cycle Counter runs at SysClk / 2 */
        unsigned int start = _CP0_GET_COUNT();
        unsigned int cyclesToWait = (SysClk.GetFrequency() / 2000000UL) * delay;

        while (cyclesToWait > (_CP0_GET_COUNT() - start))
        {
            ; /* do nothing */
        }
    }

    return;
} /* sys_waitmicrosec */


static void  sys_waitmillisec(unsigned int delay)
{
    if (m_initialized)
    {
        unsigned int start = m_millisectime;

        while (delay < (m_millisectime - start))
        {
            ; /* do nothing */
        }
    }

    return;
} /* sys_waitmillisec */


static bool  sys_timeoutmicrosec(unsigned int const start, unsigned int const microsec)
{
    bool result = false;

    if (m_initialized)
    {
        /* note: Cycle Counter runs at SysClk / 2 */
        unsigned int cyclesToWait = (SysClk.GetFrequency() / 2000000UL) * microsec;

        if (cyclesToWait <= (_CP0_GET_COUNT() - start))
        {
            result = true;
        }
    }

    return result;
} /* sys_timeoutmicrosec */


static bool  sys_timeoutmillisec(unsigned int const start, unsigned int const millisec)
{
    bool result = false;

    if (millisec <= (m_millisectime - start))
    {
        result = true;
    }

    return result;
} /* sys_timeoutmillisec */


static unsigned int  sys_disableie(void)
{
    return __builtin_disable_interrupts();
} /* sys_disableie */


static void  sys_enableie(unsigned int const enable)
{
    if (0 != enable)
    {
        __builtin_enable_interrupts();
    }

    return;
} /* sys_enableie */


static void  __attribute__((__noreturn__)) sys_reset(void)
{
    _reset();
    __builtin_unreachable ();
} /* sys_reset */
