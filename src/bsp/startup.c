/**
 * File : startup.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides SFR configuration for startup sequence.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <cp0defs.h>

#include "startup.h"

extern unsigned int __builtin_disable_interrupts(void);
extern unsigned int __builtin_enable_interrupts(void);
extern void         _on_reset(void);
extern void         _on_bootstrap(void);

/**
 * @function _on_reset
 * @detail   Invoked by the startup code after the Global Pointer initialization
 *           and prior to all other microprocessor and coprocessor initialization.
 *           This function configures the microprocessor pins and modules for low
 *           power operation.
 */
void _on_reset(void)
{
    /* unlock device configuration registers */
    SYSKEY = 0xAA996655;
    SYSKEY = 0x556699AA;
    CFGCONbits.IOLOCK = 0;

    /* map peripheral module pins */

    /* UART2: RPG9=U2TX, RPB0=U2RX (Debug Console) */
    RPG9Rbits.RPG9R = 0x02;
    U2RXRbits.U2RXR = 0x05;

    /* OC8: (LCD backlight PWM) */
    RPE3Rbits.RPE3R = 0x0C;

    /* SPI2 : (MEB2 SD Card) */
    /* RPG8Rbits.RPG8R = 6;  */ /* leave SDO (RG8) unused */
    /* SDI2Rbits.SDI2R = 14; */ /* SDI2 (RD7) currently connected to FLIR Lepton */

    CFGCONbits.IOLOCK = 1;  /* prevent subsequent I/O remapping */

    /* PERIPHERAL MODULE DISABLE */
    /* PMD1: HLVDMD, CVRMD, CTMUMD, ADCMD */
    PMD1bits.LVDMD = 1;     /* non-functional (Erratum 19, Doc. 80000736B) */
    PMD1bits.CVRMD = 1;     /* voltage comparator not used */
    PMD1bits.CTMUMD = 1;    /* CTMU not used */

    /* PMD2: CMP2MD, CMP2MD : comparators not used */
    PMD2bits.CMP1MD = 1;
    PMD2bits.CMP2MD = 1;

    /* PMD3: OC1:9<24:16>, IC1:9 <8:0> */
    /* input capture not used */
    PMD3 = 0x1F;

    /* PMD4: Timers 1:9 */
    /* all timers are used */

    /* PMD5: CAN2MD<29>, CAN1MD<28>, USBMD<24>, I2C1:5<16:20>,
             SPI1:6<8:13>, U1:6<0:5>
     * CAN not used
     * USB not currently used
     */
    PMD5 = 0x31000000;

    /* PMD6: ETH<28>, SQI<23>, SDHC<21>, GLCD<20>, GPU<18>, EBI<17>, PMP<16>
             REFO1:5<8:12>
     * ETH not currently used
     * SQI not used (SST26VF032B will not be used)
     * SDHC not used (SQI uses same pins; do not enable)
     * GPU not used (documentation not available)
     * EBI not used
     * PMP not used
     * REFOx bits must not be set (Erratum 5, Doc. 80000736B)
     */
    PMD6 = 0x10270000;  /* 0001 0000 0010 0111 0000 0000 0000 0000 */

    /* PMD7: DDR2<28>, CRYPT<22>, RNG<20>, DMA<4>
     * CRYPT not currently used
     * DMA must not be disabled via DMAMD (Erratum 6, Doc. 80000736B)
     */
    PMD7 = 0x00500000;

    CFGCONbits.PMDLOCK = 1; /* prevent subsequent module enable/disable */

    /* Permission Group Lock
     * All firmware runs with full privileges.
     */
    CFGCONbits.PGLOCK = 1;

    /* PERIPHERAL BUS CLOCKS
     * On Reset the clocks are on and the source is always SYSCLK;
     * default clock rates are:
     * PBCLK1:6 = 100MHz
     * PBCLK7   = 200MHz
     */

    /* REFERENCE CLOCKS
     * Defaults are OFF, Source=SYSCLK, Divisor=1 (200MHz)
     * REFCLK1 : Alternative SPI clock
     * REFCLK2 : Alternative SQI clock
     * REFCLK3 : Alternative ADC clock
     * REFCLK4 : SDHC clock
     * REFCLK5 : GLCD clock; must use SPLL; MAX Pixel Clock is 50MHz.
     */

    /* lock device configuration registers */
    SYSKEY = 0x33333333;

    /* OC modules use Alternate Clock sets for greatest flexibility */
    CFGCONbits.OCACLK = 1;

    /* Disable ADC */
    ANSELA = ANSELB = ANSELC = ANSELD = ANSELE = ANSELF = ANSELG = ANSELH = ANSELJ = 0;

    /* Configure MCU pins */
    ODCA  = 0xC6AF;		/* 1100 0110 1010 1111 */
    LATA  = 0xC6AF;		/* 1100 0110 1010 1111 */
    CNPUA = 0x0000;
    CNPDA = 0x0000;
    TRISA = 0x3900;		/* 0011 1001 0000 0000 */

	ODCB  = 0x332B;		/* 0011 0011 0010 1011 */
	LATB  = 0x732B;		/* 0111 0011 0010 1011 */
	CNPDB = 0x0000;
	CNPUB = 0x0800;		/* 0000 1000 0000 0000 */
	TRISB = 0x08D0;		/* 0000 1000 1101 0000 */

    ODCC  = 0xA00A;		/* 1010 0000 0000 1010 */
    LATC  = 0xA00A;		/* 1010 0000 0000 1010 */
    CNPDC = 0x0000;
    CNPUC = 0x4000;		/* 0100 0000 0000 0000 */
    TRISC = 0x5FF0;		/* 0101 1111 1111 0000 */

	ODCD  = 0x4430;		/* 0100 0100 0011 0000 */
	LATD  = 0x4430;		/* 0100 0100 0011 0000 */
	CNPUD = 0x0000;
	CNPDD = 0x0000;
	TRISD = 0x0180;		/* 0000 0001 1000 0000 */


    ODCE  = 0x0140;		/* 0000 0001 0100 0000 */
    LATE  = 0x01C0;		/* 0000 0001 1100 0000 */
    CNPUE = 0x0000;
    CNPDE = 0x0000;
    TRISE = 0xFC00;		/* 1111 1100 0000 0000 */

    ODCF  = 0x010C;		/* 0000 0001 0000 1100 */
    LATF  = 0x010C;		/* 0000 0001 0000 1100 */
    CNPUF = 0x0000;
    CNPDF = 0x0000;
    TRISF = 0xCEC0;		/* 1100 1110 1100 0000 */

	ODCG  = 0x7080;		/* 0111 0000 1000 0000 */
	LATG  = 0x71C0;		/* 0111 0001 1100 0000 */
    CNPUG = 0x0000;
    CNPDG = 0x0000;
    TRISG = 0x0C3C;		/* 0000 1100 0011 1100 */

    ODCH  = 0x65E7;		/* 0110 1101 1110 0111 */
    LATH  = 0x65E7;		/* 0110 1101 1110 0111 */
    CNPUH = 0x0000;
    CNPDH = 0x0000;
    TRISH = 0x0000;		/* 0000 0000 0000 0000 */

    ODCJ  = 0x0806;		/* 0000 1000 0000 0110 */
    LATJ  = 0x0806;		/* 0000 1000 0000 0110 */
    CNPUJ = 0x0000;
    CNPDJ = 0x0000;
    TRISJ = 0x0000;

    ODCK  = 0x0006;		/* 0000 0000 0000 0110 */
    LATK  = 0x0006;		/* 0000 0000 0000 0110 */
    CNPUK = 0x0000;
    CNPDK = 0x0000;
    TRISK = 0xFF00;		/* 1111 1111 0000 0000 */

    return;
}   /* _on_reset */


/**
 * @function _on_bootstrap
 * @detail   Invoked after CP0 configuration and prior to switching exception vectors.
 */
void  _on_bootstrap(void)
{
    /* note : no special setup required at this point */
    return;
} /* _on_bootstrap */


/**
 * @function _on_start
 * @detail   Complete processor setup by selecting multi-vectored interrupts and
 *           enabling the global interrupt flag. This must be the first function
 *           invoked by main().
 */
void  _on_start(void)
{
    __builtin_disable_interrupts();

    /* Set up Interrupt Shadow registers */
    PRISSbits.PRI7SS = 7;
    PRISSbits.PRI6SS = 6;
    PRISSbits.PRI5SS = 5;
    PRISSbits.PRI4SS = 4;
    PRISSbits.PRI3SS = 3;
    PRISSbits.PRI2SS = 2;
    PRISSbits.PRI1SS = 1;

    /* Set up multi-vectored interrupt mode and enable interrupts */

    /* Set CP0.CAUSE.IV = 1 */
    unsigned int cause = _CP0_GET_CAUSE();
    cause |= 0x00800000;
    _CP0_SET_CAUSE(cause);
    _ehb();

    /* Select multi-vectored interrupts */
    INTCONbits.MVEC = 1;

    __builtin_enable_interrupts();

    return;
}   /* sys_init */
