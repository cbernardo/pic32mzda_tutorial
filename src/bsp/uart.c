/**
 * File : uart.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Implements the generic PIC32M UART driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <sys/attribs.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "cbuff.h"
#include "cfg_clocks.h"
#include "interrupt_priorities.h"
#include "pbclk.h"
#include "sysclk.h"
#include "syserr.h"
#include "uart.h"


/*******************************************************************************
 *              Private UART Definitions
 ******************************************************************************/
#define CFG_MAX_UARTS   (6)         /* number of UARTS in the device */
#define CFG_BITRATE_MIN (50U)       /* arbitary minimum bitrate; 50bps was in
                                       common use in very early modems */
#define CFG_BRG_MAX     (65535UL)   /* BRG register is 16 bits wide */


/*******************************************************************************
 *              Private UART Types
 ******************************************************************************/

typedef struct
{
    uint32_t volatile *ifs;
    uint32_t volatile *iec;
    uint32_t volatile *rx_ipc;
    uint32_t volatile *tx_ipc;
    uint32_t const     rx_flag;
    uint32_t const     rx_pri;
    uint32_t const     rx_pmask;
    uint32_t const     tx_flag;
    uint32_t const     tx_pri;
    uint32_t const     tx_pmask;
} UartIntbits_t;

typedef struct
{
    uint32_t stop_select        :1;     /* 0: 1 stop bit, 2: 2 stop bits */
    uint32_t parity_select      :2;     /* parity and data select:
                                         * 0 : 8N
                                         * 1 : 8E
                                         * 2 : 8O
                                         * 3 : 9N (not supported by this driver)
                                         */
    uint32_t fast_brg_enable    :1;     /* 1: fast brg (1/4 CLK as opposed to nominal 1/16 CLK) */
    uint32_t invert_rx          :1;     /* invert receiver polarity */
    uint32_t auto_bitrate       :1;     /* auto bitrate detect; do not use, errata apply */
    uint32_t loopback_enable    :1;     /* hardware loopback feature */
    uint32_t wake_from_sleep    :1;     /* wake from sleep on Start Bit Detect */
    uint32_t pin_enable         :2;     /* module pin enable:
                                         * 0 : Tx and Rx pins may be used by the module
                                         * 1 : Tx, Rx, and RTS pins may be used by the module
                                         * 2 : Tx, Rx, RTS, and CTS pins may be used by the module
                                         * 3 : Tx, Rx, BCLK (RTS) pins may be used by the module
                                         */
    uint32_t reserved1          :1;     /* reserved for future use */
    uint32_t rts_mode           :1;     /* 0: hardware flow, 1: simplex mode */
    uint32_t irda_enable        :1;     /* 1: enable IRDA mode */
    uint32_t stop_in_idle       :1;     /* 1: halt UART in IDLE Power Mode */
    uint32_t reserved2          :1;     /* reserved for future use */
    uint32_t module_on          :1;     /* 1: enable UART module */
    uint32_t run_during_ovf     :1;     /* 1: continue to operate received during overflow */
    uint32_t clock_select       :2;     /* Select UART clock source:
                                         * 0 : PBCLK2, OFF in Sleep Mode
                                         * 1 : SYSCLK, OFF in Sleep Mode
                                         * 2 : FRC, can receive in Sleep Mode
                                         * 3 : PBCLK2, can receive in Idle Mode
                                         */
    uint32_t reserved3          :3;     /* reserved for future use */
    uint32_t active             :1;     /* when 1, UxMODE register must not be updated */
    uint32_t run_in_sleep       :1;     /* allow wakeup in sleep (must use FRC clock) */
    uint32_t reserved4          :8;     /* reserved for future use */
} UartRegMode_t;

typedef struct
{
    uint32_t rx_data_avail      :1;     /* data is available in receiver reg */
    uint32_t rx_err_overflow    :1;     /* receiver overflow error; must be cleared in sw if set */
    uint32_t rx_err_frame       :1;     /* framing error on current character */
    uint32_t rx_err_parity      :1;     /* parity error on current character */
    uint32_t rx_idle            :1;     /* if 0 then the receiver is busy shifting data in */
    uint32_t addr_detect_enable :1;     /* (9-bit data mode only): 1 = enable Address Detect */
    uint32_t rx_interrupt_mode  :2;     /* Note: MCU has 8-bit FIFO
                                         * 0 : interrupt when Rx buffer not empty
                                         * 1 : interrupt if Rx buffer has at least 4 characters
                                         * 2 : interrupt if Rx buffer has at least 6 characters
                                         * 3 : RESERVED, DO NOT USE
                                         */
    uint32_t tx_sr_empty        :1;     /* 1 when Tx shift register empty; check errata in case
                                         * of issues such as the flag being set before completion
                                         * of the transmission (stop bit completed).
                                         */
    uint32_t tx_full            :1;     /* 1 when transmitter buffer is full */
    uint32_t tx_enable          :1;     /* 1 to enable transmitter */
    uint32_t tx_break           :1;     /* set to 1 to transmit a BREAK signal */
    uint32_t rx_enable          :1;     /* 1 to enable receiver */
    uint32_t invert_tx          :1;     /* invert transmitter polarity */
    uint32_t tx_interrupt_mode  :2;     /* Note: MCU has 8-bit FIFO
                                         * 0 : interrupt when Tx buffer not full
                                         * 1 : interrupt if all characters transmitted (shift
                                         *     register empty and STOP bits transmitted)
                                         * 2 : interrupt if Tx buffer empty
                                         * 3 : RESERVED, DO NOT USE
                                         */
    uint32_t addr_module        :8;     /* module's address for 9-bit mode */
    uint32_t addr_mask          :8;     /* address detect mask */
} UartSCR_t;

/* Structure representing a single UART register set.
 * Note that the microcontroller has a number of special
 * registers to set, clear, and toggle bits.
 */
typedef struct
{
    union
    {
        volatile uint32_t      mode;
        volatile UartRegMode_t modebits;
    };

    volatile uint32_t          mode_clr;
    volatile uint32_t          mode_set;
    volatile uint32_t          mode_inv;

    union
    {
        volatile uint32_t      status;
        volatile UartSCR_t     statusbits;
    };

    volatile uint32_t          status_clr;
    volatile uint32_t          status_set;
    volatile uint32_t          status_inv;

    volatile uint32_t          txreg;   /* 9-bit Tx Register (this driver supports only 8 bits) */
    uint32_t                   txdummy1;
    uint32_t                   txdummy2;
    uint32_t                   txdummy3;

    volatile uint32_t          rxreg;   /* 9-bit Rx Register (this driver supports only 8 bits) */
    uint32_t                   rxdummy1;
    uint32_t                   rxdummy2;
    uint32_t                   rxdummy3;

    volatile uint32_t          brg;     /* 16-bit bitrate divisor register */
} UartCtl_t;


/*******************************************************************************
 *              Private UART Constants and Variables
 ******************************************************************************/

static UartIntbits_t const m_intparams[(CFG_MAX_UARTS)] =
{
    /* Interrupt    IRQ     Flag        Enable      IPL             ISP
     * U1 RX        113     IFS3<17>    IEC3<17>    IPC28<12:10>    IPC28<9:8>
     * U1 TX        114     IFS3<18>    IEC3<18>    IPC28<20:18>    IPC28<17:16>
     */
    {
        .ifs = &IFS3,
        .iec = &IEC3,
        .rx_ipc = &IPC28,
        .rx_flag = (1UL << 17),
        .rx_pri = (((CFG_U1RX_IPL) << 2) | (CFG_U1RX_ISL)) << 8,
        .rx_pmask = (0x1F) << 8,
        .tx_ipc = &IPC28,
        .tx_flag = (1UL << 18),
        .tx_pri = (((CFG_U1TX_IPL) << 2) | (CFG_U1TX_ISL)) << 16,
        .tx_pmask = (0x1F) << 16,
    },
    /* Interrupt    IRQ     Flag        Enable      IPL             ISP
     * U2 RX        146     IFS4<18>    IEC4<18>    IPC36<20:18>    IPC36<17:16>
     * U2 TX        147     IFS4<19>    IEC4<19>    IPC36<28:26>    IPC36<25:24>
     */
    {
        .ifs = &IFS4,
        .iec = &IEC4,
        .rx_ipc = &IPC36,
        .rx_flag = (1UL << 18),
        .rx_pri = (((CFG_U2RX_IPL) << 2) | (CFG_U2RX_ISL)) << 16,
        .rx_pmask = (0x1F) << 16,
        .tx_ipc = &IPC36,
        .tx_flag = (1UL << 19),
        .tx_pri = (((CFG_U2TX_IPL) << 2) | (CFG_U2TX_ISL)) << 24,
        .tx_pmask = (0x1F) << 24,
    },
    /* Interrupt    IRQ     Flag        Enable      IPL             ISP
     * U3 RX        158     IFS4<30>    IEC4<30>    IPC39<20:18>    IPC39<17:16>
     * U3 TX        159     IFS4<31>    IEC4<31>    IPC39<28:26>    IPC39<25:24>
     */
    {
        .ifs = &IFS4,
        .iec = &IEC4,
        .rx_ipc = &IPC39,
        .rx_flag = (1UL << 30),
        .rx_pri = (((CFG_U3RX_IPL) << 2) | (CFG_U3RX_ISL)) << 16,
        .rx_pmask = (0x1F) << 16,
        .tx_ipc = &IPC39,
        .tx_flag = (1UL << 31),
        .tx_pri = (((CFG_U3TX_IPL) << 2) | (CFG_U3TX_ISL)) << 24,
        .tx_pmask = (0x1F) << 24,
    },
    /* Interrupt    IRQ     Flag        Enable      IPL             ISP
     * U4 RX        171     IFS5<11>    IEC5<11>    IPC42<28:26>    IPC42<25:24>
     * U4 TX        172     IFS5<12>    IEC5<12>    IPC43<4:2>      IPC43<1:0>
     */
    {
        .ifs = &IFS5,
        .iec = &IEC5,
        .rx_ipc = &IPC42,
        .rx_flag = (1UL << 11),
        .rx_pri = (((CFG_U4RX_IPL) << 2) | (CFG_U4RX_ISL)) << 24,
        .rx_pmask = (0x1F) << 24,
        .tx_ipc = &IPC43,
        .tx_flag = (1UL << 12),
        .tx_pri = (((CFG_U4TX_IPL) << 2) | (CFG_U4TX_ISL)) << 0,
        .tx_pmask = (0x1F) << 0,
    },
    /* Interrupt    IRQ     Flag        Enable      IPL             ISP
     * U5 RX        180     IFS5<20>    IEC5<20>    IPC45<4:2>      IPC45<1:0>
     * U5 TX        181     IFS5<21>    IEC5<21>    IPC45<12:10>    IPC45<9:8>
     */
    {
        .ifs = &IFS5,
        .iec = &IEC5,
        .rx_ipc = &IPC45,
        .rx_flag = (1UL << 20),
        .rx_pri = (((CFG_U5RX_IPL) << 2) | (CFG_U5RX_ISL)) << 0,
        .rx_pmask = (0x1F) << 0,
        .tx_ipc = &IPC45,
        .tx_flag = (1UL << 21),
        .tx_pri = (((CFG_U5TX_IPL) << 2) | (CFG_U5TX_ISL)) << 8,
        .tx_pmask = (0x1F) << 8,
    },
    /* Interrupt    IRQ     Flag        Enable      IPL             ISP
     * U6 RX        189     IFS5<29>    IEC5<29>    IPC47<12:10>    IPC47<9:8>
     * U6 TX        190     IFS5<30>    IEC5<30>    IPC47<20:18>    IPC47<17:16>
     */
    {
        .ifs = &IFS5,
        .iec = &IEC5,
        .rx_ipc = &IPC47,
        .rx_flag = (1UL << 29),
        .rx_pri = (((CFG_U5RX_IPL) << 2) | (CFG_U5RX_ISL)) << 8,
        .rx_pmask = (0x1F) << 8,
        .tx_ipc = &IPC47,
        .tx_flag = (1UL << 30),
        .tx_pri = (((CFG_U5TX_IPL) << 2) | (CFG_U5TX_ISL)) << 16,
        .tx_pmask = (0x1F) << 16,
    },
};


/* UART Special Function Registers */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
static UartCtl_t * const m_sfr[(CFG_MAX_UARTS)] =
{
    (UartCtl_t *) &U1MODE,
    (UartCtl_t *) &U2MODE,
    (UartCtl_t *) &U3MODE,
    (UartCtl_t *) &U4MODE,
    (UartCtl_t *) &U5MODE,
    (UartCtl_t *) &U6MODE,
};
#pragma GCC diagnostic pop

/* UART Status (required to support persistent flags) */
static UartStatus_t     m_status[(CFG_MAX_UARTS)];

/* Transmitter and Receiver Buffers */
#define DEF_UART_TXBUF(ARG) static CBuff_t m_TxBuf##ARG
#define DEF_UART_RXBUF(ARG) static CBuff_t m_RxBuf##ARG

#if (1 == CFG_UART1_ENABLE)
    #if (1 == CFG_UART1_TXBUF)
    DEF_UART_TXBUF(1);
    #endif

    #if (1 == CFG_UART1_RXBUF)
    DEF_UART_RXBUF(1);
    #endif
#endif

#if (1 == CFG_UART2_ENABLE)
    #if (1 == CFG_UART2_TXBUF)
    DEF_UART_TXBUF(2);
    #endif

    #if (1 == CFG_UART2_RXBUF)
    DEF_UART_RXBUF(2);
    #endif
#endif

#if (1 == CFG_UART3_ENABLE)
    #if (1 == CFG_UART3_TXBUF)
    DEF_UART_TXBUF(3);
    #endif

    #if (1 == CFG_UART3_RXBUF)
    DEF_UART_RXBUF(3);
    #endif
#endif

#if (1 == CFG_UART4_ENABLE)
    #if (1 == CFG_UART4_TXBUF)
    DEF_UART_TXBUF(4);
    #endif

    #if (1 == CFG_UART4_RXBUF)
    DEF_UART_RXBUF(4);
    #endif
#endif

#if (1 == CFG_UART5_ENABLE)
    #if (1 == CFG_UART5_TXBUF)
    DEF_UART_TXBUF(5);
    #endif

    #if (1 == CFG_UART5_RXBUF)
    DEF_UART_RXBUF(5);
    #endif
#endif


#if (1 == CFG_UART6_ENABLE)
    #if (1 == CFG_UART6_TXBUF)
    DEF_UART_TXBUF(6);
    #endif

    #if (1 == CFG_UART6_RXBUF)
    DEF_UART_RXBUF(6);
    #endif
#endif

static CBuff_t * const m_txbuff[(CFG_MAX_UARTS)] =
{
#if (1 == CFG_UART1_ENABLE) && (1 == CFG_UART1_TXBUF)
    (CBuff_t *) &m_TxBuf1,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART2_ENABLE) && (1 == CFG_UART2_TXBUF)
    (CBuff_t *) &m_TxBuf2,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART3_ENABLE) && (1 == CFG_UART3_TXBUF)
    (CBuff_t *) &m_TxBuf3,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART4_ENABLE) && (1 == CFG_UART4_TXBUF)
    (CBuff_t *) &m_TxBuf4,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART5_ENABLE) && (1 == CFG_UART5_TXBUF)
    (CBuff_t *) &m_TxBuf5,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART6_ENABLE) && (1 == CFG_UART6_TXBUF)
    (CBuff_t *) &m_TxBuf6,
#else
    (CBuff_t *) 0,
#endif
};

static CBuff_t * const m_rxbuff[(CFG_MAX_UARTS)] =
{
#if (1 == CFG_UART1_ENABLE) && (1 == CFG_UART1_RXBUF)
    (CBuff_t *) &m_RxBuf1,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART2_ENABLE) && (1 == CFG_UART2_RXBUF)
    (CBuff_t *) &m_RxBuf2,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART3_ENABLE) && (1 == CFG_UART3_RXBUF)
    (CBuff_t *) &m_RxBuf3,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART4_ENABLE) && (1 == CFG_UART4_RXBUF)
    (CBuff_t *) &m_RxBuf4,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART5_ENABLE) && (1 == CFG_UART5_RXBUF)
    (CBuff_t *) &m_RxBuf5,
#else
    (CBuff_t *) 0,
#endif

#if (1 == CFG_UART6_ENABLE) && (1 == CFG_UART6_RXBUF)
    (CBuff_t *) &m_RxBuf6,
#else
    (CBuff_t *) 0,
#endif
};


/*******************************************************************************
 *              Private UART Function Declarations
 ******************************************************************************/
static int          uart_Init(int const instance, UartCfg_t const *config);
static void         uart_Shutdown(int const instance);
static int          uart_SendByte(int const instance, int const byte);
static int          uart_SendString(int const instance, void const *buffer, int const length);
static int          uart_ReceiveByte(int const instance, void *byte);
static int          uart_ReceiveString(int const instance, void *buffer, int *length);
static int          uart_TxFlush(int const instance);
static int          uart_RxFlush(int const instance);
static UartStatus_t uart_GetStatus(int const instance);
static void         uart_ClearError(int const instance);
static int          uart_Loopback(int const instance, bool const enable);
static int          uart_CalcBrg(UartCfg_t const *config, uint32_t *brg);
static void         uart_TxInterruptEnable(int const instance, bool const enable);

/* UART General Interrupt Handlers */
#if (1 == CFG_UART1_TXBUF) || (1 == CFG_UART2_TXBUF) || (1 == CFG_UART3_TXBUF) || \
    (1 == CFG_UART4_TXBUF) || (1 == CFG_UART5_TXBUF) || (1 == CFG_UART6_TXBUF)
static void         uart_HandleTx(int const instance);
#endif

#if (1 == CFG_UART1_RXBUF) || (1 == CFG_UART2_RXBUF) || (1 == CFG_UART3_RXBUF) || \
    (1 == CFG_UART4_RXBUF) || (1 == CFG_UART5_RXBUF) || (1 == CFG_UART6_RXBUF)
static void         uart_HandleRx(int const instance);
#endif

/* UART Transmit Interrupt Handlers : do not declare __ISR functions as static */
#if (1 == CFG_UART1_TXBUF)
void __ISR(_UART1_TX_VECTOR, CFG_U1TX_SRS) U1TXHandler(void);
#endif

#if (1 == CFG_UART2_TXBUF)
void __ISR(_UART2_TX_VECTOR, CFG_U2TX_SRS) U2TXHandler(void);
#endif

#if (1 == CFG_UART3_TXBUF)
void __ISR(_UART3_TX_VECTOR, CFG_U3TX_SRS) U3TXHandler(void);
#endif

#if (1 == CFG_UART4_TXBUF)
void __ISR(_UART4_TX_VECTOR, CFG_U4TX_SRS) U4TXHandler(void);
#endif

#if (1 == CFG_UART5_TXBUF)
void __ISR(_UART5_TX_VECTOR, CFG_U5TX_SRS) U5TXHandler(void);
#endif

#if (1 == CFG_UART6_TXBUF)
void __ISR(_UART6_TX_VECTOR, CFG_U6TX_SRS) U6TXHandler(void);
#endif

/* UART Receive Interrupt Handlers : do not declare __ISR functions as static */
#if (1 == CFG_UART1_RXBUF)
void __ISR(_UART1_RX_VECTOR, CFG_U1RX_SRS) U1RXHandler(void);
#endif

#if (1 == CFG_UART2_RXBUF)
void __ISR(_UART2_RX_VECTOR, CFG_U2RX_SRS) U2RXHandler(void);
#endif

#if (1 == CFG_UART3_RXBUF)
void __ISR(_UART3_RX_VECTOR, CFG_U3RX_SRS) U3RXHandler(void);
#endif

#if (1 == CFG_UART4_RXBUF)
void __ISR(_UART4_RX_VECTOR, CFG_U4RX_SRS) U4RXHandler(void);
#endif

#if (1 == CFG_UART5_RXBUF)
void __ISR(_UART5_RX_VECTOR, CFG_U5RX_SRS) U5RXHandler(void);
#endif

#if (1 == CFG_UART6_RXBUF)
void __ISR(_UART6_RX_VECTOR, CFG_U6RX_SRS) U6RXHandler(void);
#endif


/*******************************************************************************
 *              UART Private Function Declarations and API instances
 ******************************************************************************/

#define DEF_UART_INTERNAL(ARG) \
    /* Function Declarations */ \
static int          uart##ARG_Init(UartCfg_t const *config); \
static void         uart##ARG_Shutdown(void); \
static int          uart##ARG_SendByte(int const byte); \
static int          uart##ARG_SendString(void const *buffer, int const length); \
static int          uart##ARG_ReceiveByte(void *byte); \
static int          uart##ARG_ReceiveString(void *buffer, int *length); \
static int          uart##ARG_TxFlush(void); \
static int          uart##ARG_RxFlush(void); \
static UartStatus_t uart##ARG_GetStatus(void); \
static void         uart##ARG_ClearError(void); \
static int          uart##ARG_Loopback(bool const enable); \
    /* Function Definitions */ \
static int          uart##ARG_Init(UartCfg_t const *config) \
                    { return uart_Init((ARG - 1), config); } \
static void         uart##ARG_Shutdown(void) \
                    { uart_Shutdown(ARG - 1); } \
static int          uart##ARG_SendByte(int const byte) \
                    { return uart_SendByte((ARG - 1), byte); } \
static int          uart##ARG_SendString(void const *buffer, int const length) \
                    { return uart_SendString((ARG - 1), buffer, length); } \
static int          uart##ARG_ReceiveByte(void *byte) \
                    { return uart_ReceiveByte((ARG - 1), byte); } \
static int          uart##ARG_ReceiveString(void *buffer, int *length) \
                    { return uart_ReceiveString((ARG - 1), buffer, length); } \
static int          uart##ARG_TxFlush(void) \
                    { return uart_TxFlush(ARG - 1); } \
static int          uart##ARG_RxFlush(void) \
                    { return uart_RxFlush(ARG - 1); } \
static UartStatus_t uart##ARG_GetStatus(void) \
                    { return uart_GetStatus(ARG - 1); } \
static void         uart##ARG_ClearError(void) \
                    { uart_ClearError(ARG - 1); } \
static int          uart##ARG_Loopback(bool const enable) \
                    { return uart_Loopback((ARG - 1), enable); } \
    /* API Instances */ \
DrvUart_t Uart##ARG = \
{ \
    .Init = uart##ARG_Init, \
    .Shutdown = uart##ARG_Shutdown, \
    .SendByte = uart##ARG_SendByte, \
    .SendString = uart##ARG_SendString, \
    .ReceiveByte = uart##ARG_ReceiveByte, \
    .ReceiveString = uart##ARG_ReceiveString, \
    .TxFlush = uart##ARG_TxFlush, \
    .RxFlush = uart##ARG_RxFlush, \
    .GetStatus = uart##ARG_GetStatus, \
    .ClearError = uart##ARG_ClearError, \
    .Loopback = uart##ARG_Loopback, \
};

/* Instantiate all enabled driver interfaces */
#if (1 == CFG_UART1_ENABLE)
DEF_UART_INTERNAL(1)
#endif

#if (1 == CFG_UART2_ENABLE)
DEF_UART_INTERNAL(2)
#endif

#if (1 == CFG_UART3_ENABLE)
DEF_UART_INTERNAL(3)
#endif

#if (1 == CFG_UART4_ENABLE)
DEF_UART_INTERNAL(4)
#endif

#if (1 == CFG_UART5_ENABLE)
DEF_UART_INTERNAL(5)
#endif

#if (1 == CFG_UART6_ENABLE)
DEF_UART_INTERNAL(6)
#endif

/*******************************************************************************
 *              Private UART Function Definitions
 ******************************************************************************/

/**
 * @function uart_Init
 * @detail   Initializes a specified UART given a pointer to the configuration
 *           parameters.
 *
 * @param    instance [IN] is an index to the instance to configure (0..5 = UART1..UART6)
 * @param    config   [IN] is a pointer to the user specified configuration parameters.
 * @return   0 on success, else error code.
 */
static int uart_Init(int const instance, UartCfg_t const *config)
{
    int           result = 0;
    uint32_t      brg = 0;
    UartCtl_t    *ctl = m_sfr[instance];
    UartIntbits_t const *intreg = &m_intparams[instance];

    union
    {
        uint32_t       reg;
        UartRegMode_t  cfg;
    } mode;

    union
    {
        uint32_t       reg;
        UartSCR_t      scr;
    } ctrl;

    /* UART Peripheral Module Disable bits are PMD5<5:0> */
    if (0 != (PMD5 & (1UL << instance)))
    {
        /* device disabled */
        result = ERR_NODEV;
    }
    else if (0 == config)
    {
        result = ERR_PARAM;
    }
    else if (1 == ctl->modebits.module_on)
    {
        uart_Shutdown(instance);
    }

    if (0 == result)
    {
        result = uart_CalcBrg(config, &brg);
    }

    if (0 == result)
    {
        /* disable UART interrupts */
        *intreg->iec = *intreg->iec & (~(intreg->rx_flag | intreg->tx_flag));

        /* set the Rx and Tx interrupt priority levels */
        *intreg->rx_ipc = *intreg->rx_ipc & (~intreg->rx_pmask);
        *intreg->tx_ipc = *intreg->tx_ipc & (~intreg->tx_pmask);
        *intreg->rx_ipc = *intreg->rx_ipc | intreg->rx_pri;
        *intreg->tx_ipc = *intreg->tx_ipc | intreg->tx_pri;

        /* set the bit rate divisor */
        ctl->brg = brg;

        /* clear the status register and status variable */
        ctl->status = 0;
        m_status[instance] = (UartStatus_t){0};

        /* Configure Mode Register */
        mode.reg = 0;
        mode.cfg.module_on = 1;                             /* enable the module */
        mode.cfg.parity_select = config->parity_select;
        mode.cfg.invert_rx = config->invert_rx;
        mode.cfg.wake_from_sleep = config->wake_from_sleep;
        mode.cfg.run_during_ovf = 1;                        /* retain sync on Rx overflow */
        mode.cfg.clock_select = config->clock_select;

        /* select which pins may be controlled by the module */
        if (config->enable_hwflow)
        {
            /* module may use Tx, Rx, RTS, and CTS pins */
            mode.cfg.pin_enable = 2;
        }
        else
        {
            /* module may only use Tx and Rx pins */
            mode.cfg.pin_enable = 0;
        }

        if ((config->wake_from_sleep) && (UART_CK_FRC == config->clock_select))
        {
            mode.cfg.run_in_sleep = 1;
        }

        /* Configure Control and Status Register */
        ctrl.reg = 0;

        if (0 != config->enable_rx)
        {
            if (0 != m_rxbuff[instance])
            {
                /* set up for receiver interrupts */
                ctrl.scr.rx_interrupt_mode = 0;
                *intreg->ifs = *intreg->ifs & (~intreg->rx_flag);
                *intreg->iec = *intreg->iec | intreg->rx_flag;
            }

            ctrl.scr.rx_enable = 1;
        }

        if (0 != config->enable_tx)
        {
            if (0 != config->invert_tx)
            {
                ctrl.scr.invert_tx = 1;
            }

            if (0 != m_txbuff[instance])
            {
                /* set up for interrupt on transmit completion */
                ctrl.scr.tx_interrupt_mode = 1;
            }

            ctrl.scr.tx_enable = 1;
        }

        /* initialize the buffers */
        if (0 != m_rxbuff[instance])
        {
            cbuf_Init(m_rxbuff[instance]);
        }

        if (0 != m_txbuff[instance])
        {
            cbuf_Init(m_txbuff[instance]);
        }

        /* set the configuration bits and enable the module */
        ctl->status = ctrl.reg;
        _nop(); /* insert 2 * NO OP to cover case where PBCLK = SYSCLK */
        _nop();
        ctl->mode = mode.reg;
    }

    return result;
} /* uart_Init */

/**
 * @function uart_Shutdown
 * @detail   Shuts down the specified UART. The receiver is shut down immediately
 *           while the transmitter is shut down after sending all queued data.
 *
 * @param    instance [IN] is an index to the instance to shut down (0..5 = UART1..UART6)
 */
static void uart_Shutdown(int const instance)
{
    UartCtl_t *ctl = m_sfr[instance];

    if (0 != ctl->modebits.module_on)
    {
        if (1 == ctl->statusbits.rx_enable)
        {
            /* shut down the receiver */
            while (0 != ctl->modebits.active)
            {
                ;   /* wait until module active flag is cleared */
            }

            ctl->statusbits.rx_enable = 0;
        }

        /* finish transmission */
        if (1 == ctl->statusbits.tx_enable)
        {
            if (0 != m_txbuff[instance])
            {
                while (0 != cbuf_GetUsedBytes(m_txbuff[instance]))
                {
                    ;   /* wait for the transmitter buffer to be queued */
                }
            }

            /* wait for final stop bit to complete */
            while (0 == ctl->statusbits.tx_sr_empty)
            {
                ;
            }
        }

        /* switch off the module */
        ctl->mode = 0;
    }

    return;
} /* uart_Shutdown */

/**
 * @function uart_SendByte
 * @detail   Queues a single byte for transmission. This function will block if the
 *           transmitter buffer is full.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @param    byte     [IN] is the byte to be transmitted.
 * @return   zero on success, else error code.
 */
static int uart_SendByte(int const instance, int const byte)
{
    int        result = ERR_FAIL;
    UartCtl_t *ctl = m_sfr[instance];
    CBuff_t   *txbuf = m_txbuff[instance];

    /* process if module is enabled and transmitter is enabled */
    if ((0 != ctl->modebits.module_on) && (0 != ctl->statusbits.tx_enable))
    {
        result = 0;

        if (0 == txbuf)
        {
            /* unbuffered operation */
            while (ctl->statusbits.tx_full)
            {
                ;
            }

            ctl->txreg = (uint32_t) byte;
        }
        else
        {
            /* buffered operation */
            while (0 == cbuf_GetUnusedBytes(txbuf))
            {
                ;
            }

            cbuf_PushByte(txbuf, (uint8_t) byte);
            uart_TxInterruptEnable(instance, true);
        }
    }

    return result;
} /* uart_SendByte */

/**
 * @function uart_SendString
 * @detail   Queues a byte string for transmission. This function will block if the
 *           transmitter buffer is full.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @param    buffer   [IN] is a pointer to the byte string to be transmitted.
 * @param    length   [IN] is the number of bytes to be transmitted.
 * @return   zero on success, else error code.
 */
static int uart_SendString(int const instance, void const *buffer, int const length)
{
    int            result = ERR_FAIL;
    int            bytesSent = 0;
    UartCtl_t     *ctl = m_sfr[instance];
    CBuff_t       *txbuf = m_txbuff[instance];
    uint8_t const *src = (uint8_t const *) buffer;

    /* process if module is enabled and transmitter is enabled */
    if ((0 != ctl->modebits.module_on) && (0 != ctl->statusbits.tx_enable))
    {
        result = 0;

        if (0 == txbuf)
        {
            /* unbuffered operation */
            while (bytesSent < length)
            {
                while (ctl->statusbits.tx_full)
                {
                    ;
                }

                ctl->txreg = (uint32_t) src[bytesSent++];
            }
        }
        else
        {
            /* buffered operation */
            bool txIntEnable = false;

            if (0 == cbuf_GetUsedBytes(txbuf))
            {
                txIntEnable = true;
            }

            while (bytesSent < length)
            {
                int bytesFree = 0;
                int bytesRemaining = length - bytesSent;
                
                do
                {
                    bytesFree = (int)cbuf_GetUnusedBytes(txbuf);
                } while (0 == bytesFree);   /* wait for space in the buffer */

                /* queue as must of the string as possible */
                if (bytesFree > bytesRemaining)
                {
                    bytesFree = bytesRemaining;
                }

                cbuf_PushString(txbuf, &src[bytesSent], bytesFree);
                bytesSent += bytesFree;

                if (txIntEnable)
                {
                    txIntEnable = false;
                    uart_TxInterruptEnable(instance, true);
                }
            }
        }
    }

    return result;
} /* uart_SendString */

/**
 * @function uart_ReceiveByte
 * @detail   Retrieves a single byte of data if any is available.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @param    byte     [IN,OUT] is a pointer to store the received byte.
 * @return   zero on success, ERR_AGAIN if no data is available, or other error code.
 */
static int uart_ReceiveByte(int const instance, void *byte)
{
    int           result = ERR_FAIL;
    UartCtl_t    *ctl = m_sfr[instance];
    CBuff_t      *rxbuf = m_rxbuff[instance];
    UartStatus_t *stat = &m_status[instance];

    /* process if module is enabled and receiver is enabled */
    if ((0 != ctl->modebits.module_on) && (0 != ctl->statusbits.rx_enable))
    {
        result = 0;
        *((uint8_t *) byte) = 0;

        if (0 == rxbuf)
        {
            /* unbuffered operation */
            if (0 == ctl->statusbits.rx_data_avail)
            {
                /* no data available; try again later */
                result = ERR_AGAIN;
            }
            else
            {
                /* Check for parity or framing errors; since framing and parity flags are cleared
                 * after reading the receiver FIFO, the shadow status buffer shall be used to make
                 * the flag persistent. It is up to the user to clear errors before proceeding.
                 */
                UartSCR_t flags = ctl->statusbits;

                if (0 != flags.rx_err_frame)
                {
                    stat->err_frame = 1;
                }

                if (0 != flags.rx_err_parity)
                {
                    stat->err_parity = 1;
                }

                if (0 != flags.rx_err_overflow)
                {
                    stat->err_rxoverflow = 1;
                }

                if (0 != stat->err_frame)
                {
                    result = ERR_SYNC;
                }
                else if (0 != stat->err_parity)
                {
                    result = ERR_PARITY;
                }
                else if (0 != stat->err_rxoverflow)
                {
                    result = ERR_OVERFLOW;
                }

                if (0 == result)
                {
                    *((uint8_t *) byte) = (uint8_t) ctl->rxreg;
                }
                else if (ERR_OVERFLOW != result)
                {
                    /* drop the data; it is safe to use NVMKEY since the register will
                     * never contain a valid unlock code
                     */
                    NVMKEY = ctl->rxreg;
                }
            }
        }
        else
        {
            /* buffered operation */
            int bytesAvailable = (int)cbuf_GetUsedBytes(rxbuf);

            if (0 != bytesAvailable)
            {
                (void) cbuf_PopByte(rxbuf, byte);
            }
            else
            {
                /* the user has all known good data; check the error flags */
                if (0 != stat->err_frame)
                {
                    result = ERR_SYNC;
                }
                else if (0 != stat->err_parity)
                {
                    result = ERR_PARITY;
                }
                else if (0 != stat->err_rxoverflow)
                {
                    result = ERR_OVERFLOW;
                }
                else
                {
                    /* no error condition and no data */
                    result = ERR_AGAIN;
                }
            }
        }
    }

    return result;
} /* uart_ReceiveByte */

/**
 * @function uart_ReceiveString
 * @detail   Retrieves multiple bytes of data if any is available. This function receives raw
 *           data and does not provide a zero terminator.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @param    buffer   [IN,OUT] is a pointer to a buffer for the received data.
 * @param    length   [IN,OUT] on entry this must point to the maximum number of bytes to
 *                    receive; on exit it will point to the number of bytes stored in the buffer.
 * @return   zero on success, ERR_AGAIN if no data is available, or other error code.
 */
static int uart_ReceiveString(int const instance, void *buffer, int *length)
{
    int           result = ERR_AGAIN;
    UartCtl_t    *ctl = m_sfr[instance];
    CBuff_t      *rxbuf = m_rxbuff[instance];
    UartStatus_t *stat = &m_status[instance];
    uint8_t      *inbuf = (uint8_t *)buffer;
    int           bytesRead = 0;

    /* process if module is enabled and receiver is enabled */
    if ((0 != ctl->modebits.module_on) && (0 != ctl->statusbits.rx_enable))
    {
        result = 0;

        if (0 == rxbuf)
        {
            /* Unbuffered operation; invoke uart_ReceiveByte repeatedly. Although this is
             * very inefficient, it makes the code neater.
             */
            while ((bytesRead < (*length)) && (0 == uart_ReceiveByte(instance, inbuf)))
            {
                ++bytesRead;
                ++inbuf;
            }
        }
        else
        {
            /* buffered operation */
            int stringLength = 0;
            bytesRead = (int)cbuf_GetUsedBytes(rxbuf);

            if (0 != bytesRead)
            {
                if (bytesRead > (*length))
                {
                    bytesRead = *length;
                }

                (void) cbuf_PopString(rxbuf, buffer, bytesRead, &stringLength);
            }
            else
            {
                /* the user has all known good data; check the error flags */
                if (0 != stat->err_frame)
                {
                    result = ERR_SYNC;
                }
                else if (0 != stat->err_parity)
                {
                    result = ERR_PARITY;
                }
                else if (0 != stat->err_rxoverflow)
                {
                    result = ERR_OVERFLOW;
                }
                else
                {
                    /* no error condition and no data */
                    result = ERR_AGAIN;
                }
            }
        }

        /* update the length to indicate the number of bytes read */
        *length = bytesRead;
    }

    if ((0 != result) && (0 != length))
    {
        *length = 0;
    }

    return result;
} /* uart_ReceiveString */

/**
 * @function uart_TxFlush
 * @detail   This function blocks until all data queued for transmission has been sent.
 *           data and does not provide a zero terminator.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @return   zero on success, else error code.
 */
static int uart_TxFlush(int const instance)
{
    int           result = ERR_FAIL;
    UartCtl_t    *ctl = m_sfr[instance];
    CBuff_t      *txbuf = m_txbuff[instance];

    /* process if module is enabled and transmitter is enabled */
    if ((0 != ctl->modebits.module_on) && (0 != ctl->statusbits.tx_enable))
    {
        result = 0;

        while ((0 != txbuf) && (0 != cbuf_GetUsedBytes(txbuf)))
        {
            ;   /* wait for buffer to be emptied */
        }

        while (0 == ctl->statusbits.tx_sr_empty)
        {
            ;   /* wait for transmitter shift register to empty */
        }
    }

    return result;
} /* uart_TxFlush */

/**
 * @function uart_RxFlush
 * @detail   This function invokes ClearError to flush the receiver UART and
 *           clear all error flags.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @return   zero on success, else error code.
 */
static int uart_RxFlush(int const instance)
{
    int        result = ERR_FAIL;
    UartCtl_t *ctl = m_sfr[instance];

    if (0 != ctl->modebits.module_on)
    {
        result = 0;
        uart_ClearError(instance);
    }

    return result;
} /* uart_RxFlush */

/**
 * @function uart_GetStatus
 * @detail   This function returns status information about the selected UART.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @return   UART status information.
 */
static UartStatus_t uart_GetStatus(int const instance)
{
    UartCtl_t    *ctl = m_sfr[instance];
    CBuff_t      *txbuf = m_txbuff[instance];
    CBuff_t      *rxbuf = m_rxbuff[instance];
    UartStatus_t *stat = &m_status[instance];

    if (0 == ctl->modebits.module_on)
    {
        memset(stat, 0, sizeof(UartStatus_t));
    }
    else
    {
        UartSCR_t status = ctl->statusbits;
        stat->loopback_en = ctl->modebits.loopback_enable;

        if (0 != status.tx_enable)
        {
            if (0 == txbuf)
            {
                /* unbuffered transmission */
                if (!status.tx_full)
                {
                    /* at least 1 space is available; exact space is not known */
                    stat->tx_space = 1;
                }
                else
                {
                    stat->tx_space = 0;
                }
            }
            else
            {
                /* buffered transmission */
                int tx_space = cbuf_GetUnusedBytes(txbuf);

                if (tx_space > 4095)
                {
                    /* status can only report a MAX 4095 bytes used or available */
                    stat->tx_space = 4095;
                }
                else
                {
                    stat->tx_space = 0xFFF & ((unsigned int) tx_space);
                }
            }
        }
        else
        {
            stat->tx_space = 0;
        }

        if (0 != status.rx_enable)
        {
            /* check and set frame error; flag may only be cleared by ClearError */
            if (0 != status.rx_err_frame)
            {
                stat->err_frame = 1;
            }

            /* check and set parity error; flag may only be cleared by ClearError */
            if (0 != status.rx_err_parity)
            {
                stat->err_parity = 1;
            }

            /* check and set overflow error; flag may only be cleared by ClearError */
            if (0 != status.rx_err_overflow)
            {
                stat->err_rxoverflow = 1;
            }

            if (0 == rxbuf)
            {
                /* unbuffered receiver */
                if (status.rx_data_avail)
                {
                    /* at least 1 space is available; exact space is not known */
                    stat->rx_chars = 1;
                }
                else
                {
                    stat->rx_chars = 0;
                }
            }
            else
            {
                /* buffered receiver */
                int rx_chars = cbuf_GetUsedBytes(rxbuf);

                if (rx_chars > 4095)
                {
                    stat->rx_chars = 4095;
                }
                else
                {
                    stat->rx_chars = 0xFFFU & ((unsigned int) rx_chars);
                }
            }
        }
        else
        {
            stat->rx_chars = 0;
            stat->err_frame = 0;
            stat->err_parity = 0;
            stat->err_rxoverflow = 0;
        }
    }

    return (*stat);
} /* uart_GetStatus */

/**
 * @function uart_ClearError
 * @detail   This function clears the receiver buffer and error flags.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 */
static void uart_ClearError(int const instance)
{
    UartCtl_t *ctl = m_sfr[instance];

    if (0 != ctl->modebits.module_on)
    {
        memset(&m_status[instance], 0, sizeof(UartStatus_t));

        if (0 != m_rxbuff[instance])
        {
            (void) cbuf_Init(m_rxbuff[instance]);
            ctl->statusbits.rx_err_overflow = 0;
        }
    }

    return;
} /* uart_ClearError */

/**
 * @function uart_Loopback
 * @detail   This function enables or disables the hardware loopback feature.
 *           The current state of the feature may be retrived via <b>GetStatus<\b>.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @param    enable   [IN] true to enable, false to disable.
 * @return   zero on success, else error code.
 */
static int uart_Loopback(int const instance, bool const enable)
{
    int        result = ERR_FAIL;
    UartCtl_t *ctl = m_sfr[instance];

    if (0 != ctl->modebits.module_on)
    {
        result = 0;
        bool enabled = (bool)ctl->modebits.loopback_enable;

        if (enabled != enable)
        {
            while (0 != ctl->modebits.active)
            {
                ;   /* wait until module active flag is cleared */
            }

            if (enable)
            {
                ctl->modebits.loopback_enable = 1;
            }
            else
            {
                ctl->modebits.loopback_enable = 0;
            }
        }
    }

    return result;
} /* uart_Loopback */

/**
 * @function uart_CalcBrg
 * @detail   Calculates a Bit Rate Generator divisor which yields a bit
 *           rate frequency closest to the desired frequency. Note that
 *           the resulting frequency is not necessarily suitable for
 *           communications; it is up to the end user to determine the
 *           suitability based on the intended use.
 *
 * @param    config [IN]     : is a pointer to the desired UART parameters
 * @param    brg    [IN,OUT] : is a pointer to receive the 
 * @return   0 if a valid value can be obtained, else error code.
 */
static int uart_CalcBrg(UartCfg_t const *config, uint32_t *brg)
{
    int      result = 0;
    uint32_t clkFreq = 0;

    if (0 != brg)
    {
        *brg = 0;
    }

    if ((0 == brg) || (0 == config) || ((CFG_BITRATE_MIN) > config->bitrate))
    {
        result = ERR_PARAM;
    }
    else
    {
        switch (config->clock_select)
        {
        case 1: /* SYSCLK */
        {
            clkFreq = SysClk.GetFrequency();
            break;
        }

        case 2: /* FRC */
        {
            clkFreq = (CFG_FRC_FREQ);
            break;
        }

        default: /* cases 0 and 3 : PBCLK2 */
        {
            clkFreq = PbClk2.GetFrequency();
            break;
        }
        }

        if (0 == clkFreq)
        {
            /* no valid frequency */
            result = ERR_FAIL;
        }
    }

    if (0 == result)
    {
        /* calculate closest frequency attainable */
        uint32_t uart_freq = config->bitrate;
        uint32_t div = ((clkFreq + (uart_freq << 3)) / (uart_freq << 4)) - 1UL;

        if ((CFG_BRG_MAX) < div)
        {
            /* bit rate is unattainable (requires a divisor not in range) */
            result = ERR_RANGE;
        }
        else
        {
            *brg = div;
        }
    }

    return result;
}

/**
 * @function uart_TxInterruptEnable
 * @detail   This function enables or disables the specified transmitter interrupt.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 * @param    enable   [IN] true to enable, false to disable.
 */
static void uart_TxInterruptEnable(int const instance, bool const enable)
{
    UartIntbits_t const *intreg = &m_intparams[instance];

    if (enable)
    {
        if (0 == ((*intreg->iec) & (intreg->tx_flag)))
        {
            /* Clear interrupt flag */
            *intreg->ifs = (*intreg->ifs) & (~(intreg->tx_flag));
            /* Set Tx Interrupt Enable */
            *intreg->iec = (*intreg->iec) | (intreg->tx_flag);
        }
    }
    else
    {
        /* clear Tx Interrupt Enable */
        *intreg->iec = (*intreg->iec) & (~(intreg->tx_flag));
    }

    return;
} /* uart_TxInterruptEnable */


/*******************************************************************************
 *              UART Interrupt Handlers
 ******************************************************************************/

#if (1 == CFG_UART1_TXBUF) || (1 == CFG_UART2_TXBUF) || (1 == CFG_UART3_TXBUF) || \
    (1 == CFG_UART4_TXBUF) || (1 == CFG_UART5_TXBUF) || (1 == CFG_UART6_TXBUF)
/**
 * @function uart_HandleTx
 * @detail   This function is the generic transmit interrupt handler; it is invoked
 *           by the individual UART Interrupt Service Routines to handle buffered
 *           data transmission. The interrupt is disabled if the transmission
 *           buffer is empty at the time the interrupt occurs.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 */
static void uart_HandleTx(int const instance)
{
    UartCtl_t *ctl = m_sfr[instance];
    CBuff_t   *txbuf = m_txbuff[instance];
    UartIntbits_t const *intreg = &m_intparams[instance];
    int        bytesToSend = cbuf_GetUsedBytes(txbuf);   
    int        bytesSent = 0;

    if (0 == bytesToSend)
    {
        /* FIFO is empty and there is no data remaining to queue; disable the interrupt */
        *intreg->iec = (*intreg->iec) & (~(intreg->tx_flag));
    }
    else
    {
        /* queue bytes until the buffer is full or no data remains to queue */
        while ((0 == ctl->statusbits.tx_full) && (bytesSent < bytesToSend))
        {
            uint8_t token;
            (void) cbuf_PopByte(txbuf, &token);
            ctl->txreg = token;
            ++bytesSent;
        }
    }

    /* Clear interrupt flag */
    *intreg->ifs = (*intreg->ifs) & (~(intreg->tx_flag));
    return;
}
#endif

#if (1 == CFG_UART1_RXBUF) || (1 == CFG_UART2_RXBUF) || (1 == CFG_UART3_RXBUF) || \
    (1 == CFG_UART4_RXBUF) || (1 == CFG_UART5_RXBUF) || (1 == CFG_UART6_RXBUF)
/**
 * @function uart_HandleRx
 * @detail   This function is the generic receiver interrupt handler; it is invoked
 *           by the individual UART Interrupt Service Routines to handle buffered
 *           data reception.
 *
 * @param    instance [IN] is an index to the UART instance (0..5 = UART1..UART6)
 */
static void uart_HandleRx(int const instance)
{
    /* Operational note: the status must always be read prior to retrieving the data
     * since reading data clears the Frame and Parity flags.
     */
    UartCtl_t *ctl = m_sfr[instance];
    CBuff_t   *rxbuf = m_rxbuff[instance];
    UartIntbits_t const
              *intreg = &m_intparams[instance];
    int        bytesFree = cbuf_GetUnusedBytes(rxbuf);
    UartSCR_t  status = ctl->statusbits;
    UartStatus_t
              *pstat = &m_status[instance];

    while (status.rx_data_avail)
    {
        /* read one byte */
        uint8_t token = (uint8_t)ctl->rxreg;

        /* check for any reception errors */
        if (status.rx_err_overflow)
        {
            pstat->err_rxoverflow = 1;
        }

        if (status.rx_err_frame)
        {
            pstat->err_frame = 1;
        }

        if (status.rx_err_parity)
        {
            pstat->err_parity = 1;
        }

        /* store the byte if there is space in the buffer and no errors have been encountered */
        if ((0 == pstat->err_rxoverflow) && (0 == pstat->err_frame) && (0 == pstat->err_parity))
        {
            if (0 < bytesFree)
            {
                --bytesFree;
                cbuf_PushByte(rxbuf, token);
            }
            else
            {
                /* user buffer has overflowed */
                pstat->err_rxoverflow = 1;
            }
        }

        /* update the status */
        status = ctl->statusbits;
    }

    /* Clear interrupt flag */
    *intreg->ifs = (*intreg->ifs) & (~(intreg->rx_flag));
    return;
}
#endif

/* UART Transmit Interrupt Handlers : do not declare __ISR functions as static */
#if (1 == CFG_UART1_TXBUF)
void __ISR(_UART1_TX_VECTOR, CFG_U1TX_SRS) U1TXHandler(void)
{
    uart_HandleTx(0);
}
#endif

#if (1 == CFG_UART2_TXBUF)
void __ISR(_UART2_TX_VECTOR, CFG_U2TX_SRS) U2TXHandler(void)
{
    uart_HandleTx(1);
}
#endif

#if (1 == CFG_UART3_TXBUF)
void __ISR(_UART3_TX_VECTOR, CFG_U3TX_SRS) U3TXHandler(void)
{
    uart_HandleTx(2);
}
#endif

#if (1 == CFG_UART4_TXBUF)
void __ISR(_UART4_TX_VECTOR, CFG_U4TX_SRS) U4TXHandler(void)
{
    uart_HandleTx(3);
}
#endif

#if (1 == CFG_UART5_TXBUF)
void __ISR(_UART5_TX_VECTOR, CFG_U5TX_SRS) U5TXHandler(void)
{
    uart_HandleTx(4);
}
#endif

#if (1 == CFG_UART6_TXBUF)
void __ISR(_UART6_TX_VECTOR, CFG_U6TX_SRS) U6TXHandler(void)
{
    uart_HandleTx(5);
}
#endif

/* UART Receive Interrupt Handlers : do not declare __ISR functions as static */
#if (1 == CFG_UART1_RXBUF)
void __ISR(_UART1_RX_VECTOR, CFG_U1RX_SRS) U1RXHandler(void)
{
    uart_HandleRx(0);
}
#endif

#if (1 == CFG_UART2_RXBUF)
void __ISR(_UART2_RX_VECTOR, CFG_U2RX_SRS) U2RXHandler(void)
{
    uart_HandleRx(1);
}
#endif

#if (1 == CFG_UART3_RXBUF)
void __ISR(_UART3_RX_VECTOR, CFG_U3RX_SRS) U3RXHandler(void)
{
    uart_HandleRx(2);
}
#endif

#if (1 == CFG_UART4_RXBUF)
void __ISR(_UART4_RX_VECTOR, CFG_U4RX_SRS) U4RXHandler(void)
{
    uart_HandleRx(3);
}
#endif

#if (1 == CFG_UART5_RXBUF)
void __ISR(_UART5_RX_VECTOR, CFG_U5RX_SRS) U5RXHandler(void)
{
    uart_HandleRx(4);
}
#endif

#if (1 == CFG_UART6_RXBUF)
void __ISR(_UART6_RX_VECTOR, CFG_U6RX_SRS) U6RXHandler(void)
{
    uart_HandleRx(5);
}
#endif
