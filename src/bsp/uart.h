/**
 * File : uart.h
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Declares the generic PIC32M UART driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef UART_H
#define UART_H

#include <stdbool.h>
#include <stdint.h>
#include "cfg_uart.h"
#include "cbuff.h"

/* Implementation notes:
 *
 * + Pin control:
 *   All lines which may be used by a specific module must be set up
 *   via the Input/Output Pin Select mechanism in startup.c.
 *
 * + Power Modes:
 *   In IDLE mode the CPU is halted but all clocks continue to run;
 *   all UARTS operate normally if the SIDL (Stop in IDLe mode)
 *   flag is not set. In SLEEP mode the UARTS may only run if the
 *   selected clock is UART_CK_FRC.
 *
 * + Parity, Data, and Stop bits:
 *   _ only 8-bit data, with or without parity, is supported
 *   _ only 1 stop bit is supported
 *
 * + IRDA:
 *   For simplicity, IRDA operation is not incorporated.
 *   For IRDA support, a dedicated IRDA driver is recommended.
 *
 * + Duplex:
 *   For simplicity, only full duplex operation is supported.
 *   For RS485 support, a dedicated RS485 driver is recommended.
 *
 * + Receiver Errors:
 *   _ Framing Error: the character is set to 0x00. If a receiver buffer is used,
 *     all further incoming bytes are dropped until the condition is cleared.
 *   _ Parity Error: the character is set to 0x00. If a receiver buffer is used,
 *     all further incoming bytes are dropped until the condition is cleared.
 *   _ Overflow Error: A call to <b>RxFlush<\b> or <b>ClearError<\b> will discard
 *     receiver data and clear the overflow flag. The user may process any receiver
 *     data prior to invoking <b>RxFlush<\b>; all further incoming data will be
 *     discarded until the overflow condition is cleared.
 */


/*******************************************************************************
 *              PUBLIC UART TYPES
 ******************************************************************************/
        
typedef enum
{
    UART_CK_PBCLK2_LP = 0,          /* PBCLK2, off in Sleep mode */
    UART_CK_SYS = 1,                /* SYSCLK, off in Sleep mode */
    UART_CK_FRC = 2,                /* Fast RC, can receive in Sleep mode */
    UART_CK_PBCLK2 = 3,             /* PBCLK2, can receive in Idle mode; same as PBCLK2_LP */
    UART_CK_PBCLK2_END_MARKER       /* end marker, not a valid value */
} UartClock_e;

typedef enum
{
    UART_PARITY_NONE = 0,           /* 8 data bits, no parity */
    UART_PARITY_EVEN = 1,           /* 8 data bits, even parity */
    UART_PARITY_ODD = 2,            /* 8 data bits, odd parity */
    /* Note: 9 bit data with no parity is supported in hardware but not firmware */
    UART_PARITY_END_MARKER          /* end marker, not a valid value */
} UartParity_e;

typedef struct
{
    uint32_t tx_space        :12;   /* space remaining in Tx Buffer*/
    uint32_t rx_chars        :12;   /* number of bytes in Rx Buffer*/
    uint32_t err_rxoverflow  :1;    /* receiver overflow; clear via ClearError or RxFlush */
    uint32_t err_parity      :1;    /* parity error on unspecified byte; clear via ClearError */
    uint32_t err_frame       :1;    /* framing error on unspecified byte; clear via ClearError */
    uint32_t loopback_en     :1;    /* set if loopback mode is enabled */
    uint32_t reserved        :4;    /* currently unused */
} UartStatus_t;

typedef struct
{
    uint32_t enable_tx       : 1;    /* enable transmitter */
    uint32_t enable_rx       : 1;    /* enable receiver */
    uint32_t invert_tx       : 1;    /* invert transmitter polarity */
    uint32_t invert_rx       : 1;    /* invert receiver polarity */
    uint32_t enable_hwflow   : 1;    /* enable RTS/CTS flow control */
    uint32_t parity_select   : 2;    /* parity select; see UartParity_e */
    uint32_t clock_select    : 2;    /* clock source; see UartClock_e */
    uint32_t wake_from_sleep : 1;    /* enable wake from sleep; requires FRC source clock */
    uint32_t reserved        : 2;    /* not currently used */
    uint32_t bitrate         : 20;   /* 75..1048575 bps; limits depend on clock source etc. */
} UartCfg_t;


/*******************************************************************************
 *              UART API
 ******************************************************************************/

/* API Summary:
 * ------------
 * int           (*Init)            (UartCfg_t const *config);
 * void          (*Shutdown)        (void);
 * int           (*SendByte)        (int const byte);
 * int           (*SendString)      (void const *buffer, int const length);
 * int           (*ReceiveByte)     (void *byte);
 * int           (*ReceiveString)   (void *buffer, int *length);
 * int           (*TxFlush)         (void);
 * int           (*RxFlush)         (void);
 * UartStatus_t  (*GetStatus)       (void);
 * void          (*ClearError)      (void);
 * int           (*Loopback)        (bool const enable);
 */

typedef struct
{
    /**
     * @function Init
     * @detail   Initializes the UART to operate with the specified configuration.
     *           If buffered operation is desired, the Tx and Rx buffers must be
     *           set prior to invoking this function. <b>Init</b> can be called again if
     *           there is a need to enable/disable the receiver or transmitter or
     *           to change the bitrate.
     *
     * @param    config [IN] : a pointer to the configuration information.
     * @return   0 on success, else error code.
     */
    int           (*Init)            (UartCfg_t const *config);

    /**
     * @function Shutdown
     * @detail   Disables the UART; data in the receiver or transmitter buffers
     *           are discarded and the module is switched off. If queued data must be
     *           transmitted then call TxFlush prior to Shutdown.
     */
    void          (*Shutdown)        (void);

    /**
     * @function SendByte
     * @detail   Queues a single byte for transmission; if the transmit buffer is full
     *           this function will block.
     *
     * @param    byte [IN] is the byte to be transmitted
     * @return   0 on success, else error code.
     */
    int           (*SendByte)        (int const byte);

    /**
     * @function SendString
     * @detail   Queues <b>length</b> bytes for transmission; if the transmit buffer is
     *           filled this function will block until the entire buffer has been queued.
     *
     * @param    buffer [IN] is a pointer to the data to be transmitted
     * @param    length [IN] is the number of bytes to be transmitted
     * @return   0 on success, else error code.
     */
    int           (*SendString)      (void const *buffer, int const length);

    /**
     * @function ReceiveByte
     * @detail   Receives a single byte if data is available. The number of bytes currently
     *           available can be determined by a call to <b>GetStatus</b>.
     *
     * @param    byte [IN] is a pointer to store the retrieved byte.
     * @return   0 on success, ERR_AGAIN if no data available, else error code.
     */
    int           (*ReceiveByte)     (void *byte);

    /**
     * @function ReceiveString
     * @detail   Receives up to <b>*length</b> bytes of data. The number of bytes received
     *           is returned in <b>*length</b>. Data is not assumed to be text and is not
     *           zero terminated.
     *
     * @param    buffer [IN] is a pointer a buffer to receive data.
     * @param    length [IN, OUT] on entry <b>length</b> must contain the MAXIMUM number
     *           of bytes to be received. The caller is responsible for zero termination
     *           if required. On exit <b>length</b> will contain the number of bytes received.
     * @return   0 on success, ERR_AGAIN if no data available, else error code.
     */
    int           (*ReceiveString)   (void *buffer, int *length);

    /**
     * @function TxFlush
     * @detail   Blocks until all data in the transmitter buffer is sent.
     *
     * @return   0 on success, else error code.
     */
    int           (*TxFlush)         (void);

    /**
     * @function RxFlush
     * @detail   Discards all data in the receiver buffer. Note: data in the receiver
     *           shift register may subsequently be put into the buffer.
     *
     * @return   0 on success, else error code.
     */
    int           (*RxFlush)         (void);

    /**
     * @function GetStatus
     *
     * @return   UART status information.
     */
    UartStatus_t  (*GetStatus)       (void);

    /**
     * @function ClearError
     *
     * @return   Clears error flags.
     */
    void          (*ClearError)      (void);

    /**
     * @function GetStatus
     * @detail   Enables or disables the hardware loopback feature. The current state of
     *           the loopback feature is reported in the status information.
     *
     * @param    enable [IN] true to enable the loopback feature, false to disable.
     * @return   0 on success, else error code.
     */
    int           (*Loopback)        (bool const enable);
} const DrvUart_t;


/*******************************************************************************
 *              PUBLIC UART INSTANCES
 ******************************************************************************/
#if (1 == CFG_UART1_ENABLE)
extern DrvUart_t Uart1;
#endif

#if (1 == CFG_UART2_ENABLE)
extern DrvUart_t Uart2;
#endif

#if (1 == CFG_UART3_ENABLE)
extern DrvUart_t Uart3;
#endif

#if (1 == CFG_UART4_ENABLE)
extern DrvUart_t Uart4;
#endif

#if (1 == CFG_UART5_ENABLE)
extern DrvUart_t Uart5;
#endif

#if (1 == CFG_UART6_ENABLE)
extern DrvUart_t Uart6;
#endif

#endif /* UART_H */
