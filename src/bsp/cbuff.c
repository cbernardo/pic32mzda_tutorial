/**
 * File : cbuff.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : circular character buffer.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <stdint.h>
#include <string.h>

#include "cbuff.h"
#include "syserr.h"

/**
 * @function cbuf_Init
 * @detail   Initializes a circular buffer.
 *
 * @param    buffer [IN,OUT] : a pointer to the buffer to be initialized.
 * @return   0 on success, else error code.
 */
int  cbuf_Init(CBuff_t *buffer)
{
    int result = 0;

    if (0 == buffer)
    {
        result = ERR_PARAM;
    }
    else
    {
        buffer->size = 1024;
        buffer->mask = 1023;
        buffer->head = 0;
        buffer->tail = 0;
    }

    return result;
} /* cbuf_Init */


/**
 * @function cbuf_PushByte
 * @detail   Pushes a byte onto the end of the buffer. If the
 *           buffer is full then the byte is not added.
 *
 * @param    buffer [IN,OUT] : a pointer to the buffer to operate on.
 * @param    token  [IN]     : the byte to add to the buffer.
 * @return   0         : success
 *           ERR_PARAM : buffer is NULL
 *           ERR_FAIL  : buffer is full
 */
int  cbuf_PushByte(CBuff_t *buffer, int const token)
{
    int result = 0;

    if (0 == buffer)
    {
        result = ERR_PARAM;
    }
    else if (buffer->tail == ((buffer->head + 1) & buffer->mask))
    {
        result = ERR_FAIL;
    }
    else
    {
        buffer->data[buffer->head] = (uint8_t)token;
        buffer->head = (buffer->head + 1) & buffer->mask;
    }

    return result;
} /* cbuf_PushByte */


/**
 * @function cbuf_PushString
 * @detail   Pushes a string onto the end of the buffer. If the
 *           buffer cannot hold the entire string is full then
 *           the buffer is not modified.
 *
 * @param    buffer   [IN,OUT] : a pointer to the buffer to operate on.
 * @param    source       [IN] : a pointer to the string to be stored.
 * @param    sourceLength [IN] : number of bytes to store.
 * @return   0         : success
 *           ERR_PARAM : buffer is NULL
 *           ERR_FAIL  : insufficient space in the buffer
 */
int  cbuf_PushString(CBuff_t *buffer, void const *source, int const sourceLength)
{
    int result = 0;

    if ((0 == buffer) || (0 == source) || (0 > sourceLength))
    {
        result = ERR_PARAM;
    }
    else if ((sourceLength > buffer->mask)
    || (sourceLength > (((buffer->tail - buffer->head) - 1) & buffer->mask)))
    {
        /* insufficient space for the data */
        result = ERR_FAIL;
    }
    else if (0 < sourceLength)
    {
        uint8_t const *userBuf = (uint8_t const *) source;

        if ((sourceLength + buffer->head) > buffer->size)
        {
            /* copy data in 2 stages */
            size_t chunk1 = (size_t) (buffer->size - buffer->head);
            memcpy(&buffer->data[buffer->head], userBuf, chunk1);
            memcpy(buffer->data, &userBuf[chunk1], (((size_t) sourceLength) - chunk1));
            buffer->head = (buffer->head + sourceLength) & buffer->mask;
        }
        else
        {
            memcpy(&buffer->data[buffer->head], userBuf, (size_t) sourceLength);
            buffer->head = (buffer->head + sourceLength) & buffer->mask;
        }
    }

    return result;
} /* cbuf_PushString */


/**
 * @function cbuf_PeekByte
 * @detail   Retrieves the byte at the tail of the buffer without
 *           modifying the buffer.
 *
 * @param    buffer [IN]  : a pointer to the buffer to operate on.
 * @param    token  [OUT] : a pointer to receive a byte.
 * @return   0         : success
 *           ERR_PARAM : buffer and/or token is NULL
 *           ERR_FAIL  : buffer is empty
 */
int cbuf_PeekByte(CBuff_t const *buffer, void *token)
{
    int result = 0;

    if ((0 == buffer) || (0 == token))
    {
        result = ERR_PARAM;
    }
    else if (buffer->head == buffer->tail)
    {
        /* no data */
        result = ERR_FAIL;
    }
    else
    {
        *((uint8_t *)token) = buffer->data[buffer->tail];
    }

    return result;
} /* cbuf_PeekByte */


/**
 * @function cbuf_PopByte
 * @detail   Retrieves and removes the byte at the tail of the buffer.
 *
 * @param    buffer [IN]  : a pointer to the buffer to operate on.
 * @param    token  [OUT] : a pointer to receive a byte (may be NULL).
 * @return   0         : success
 *           ERR_PARAM : buffer and/or token is NULL
 *           ERR_FAIL  : buffer is empty
 */
int  cbuf_PopByte(CBuff_t *buffer, void *token)
{
    int result = 0;

    if (0 == buffer)
    {
        result = ERR_PARAM;
    }
    else if (buffer->head == buffer->tail)
    {
        /* no data */
        result = ERR_FAIL;
    }
    else
    {
        if (0 != token)
        {
            *((uint8_t *)token) = buffer->data[buffer->tail];
        }

        buffer->tail = (buffer->tail + 1) & buffer->mask;
    }

    return result;
} /* cbuf_PopByte */

/**
 * @function cbuf_PopString
 * @detail   Transfers the contents of the buffer to destination address
 *           and removes the contents from the buffer.
 *
 * @param    buffer           [IN] : a pointer to the buffer to operate on.
 * @param    dest         [IN,OUT] : a pointer to a buffer to receive data.
 * @param    destLength       [IN] : MAX number of bytes to transfer.
 * @param    stringLength [IN,OUT] : number of bytes transferred.
 * @return   0         : success
 *           ERR_PARAM : at least one of buffer, dest, or stringLength are NULL.
 */
int  cbuf_PopString(CBuff_t *buffer,
                    void *dest,
                    int const destLength,
                    int *stringLength)
{
    int result = 0;

    if ((0 == buffer) || (0 == dest) || (0 == stringLength))
    {
        result = ERR_PARAM;
    }
    else if ((0 == destLength) || (buffer->head == buffer->tail))
    {
        /* not a data transfer (0 bytes), but not necessarily an error */
        *stringLength = 0;
    }
    else
    {
        uint8_t *userBuf = (uint8_t *)dest;
        int      bytesToTransfer = (buffer->head - buffer->tail) & buffer->mask;

        if (bytesToTransfer > destLength)
        {
            bytesToTransfer = destLength;
        }

        if ((buffer->tail + bytesToTransfer) > buffer->size)
        {
            /* transfer in 2 chunks */
            size_t chunk1 = (size_t)(buffer->size - buffer->tail);
            memcpy(userBuf, &buffer->data[buffer->tail], chunk1);
            memcpy(&userBuf[chunk1], buffer->data, (((size_t) bytesToTransfer) - chunk1));
            buffer->tail = (buffer->tail + bytesToTransfer) & buffer->mask;
        }
        else
        {
            /* transfer in a single transaction */
            memcpy(userBuf, &buffer->data[buffer->tail], (size_t)bytesToTransfer);
            buffer->tail = (buffer->tail + bytesToTransfer) & buffer->mask;
        }

        *stringLength = bytesToTransfer;
    }

    return result;
} /* cbuf_PopString */


/**
 * @function cbuf_GetUnusedBytes
 * @detail   Returns the number of unused bytes in the buffer, or 0 on failure.
 *
 * @param    buffer [IN] : a pointer to the buffer to operate on.
 * @return   0 = buffer is NULL or buffer is full, else number of free bytes in the buffer.
 */
int __attribute__ ((noinline)) cbuf_GetUnusedBytes(CBuff_t const *buffer)
{
    int result = 0;

    if (0 != buffer)
    {
        result = (((buffer->tail - buffer->head) - 1) & buffer->mask);
    }

    return result;
} /* cbuf_GetUnusedBytes */


/**
 * @function cbuf_GetUsedBytes
 * @detail   Returns the number bytes stored in the buffer.
 *
 * @param    buffer [IN] : a pointer to the buffer to operate on.
 * @return   0 = buffer is NULL or buffer is empty, else number of used bytes in the buffer.
 */
int __attribute__ ((noinline)) cbuf_GetUsedBytes(CBuff_t const *buffer)
{
    int result = 0;

    if (0 != buffer)
    {
        result = ((buffer->head - buffer->tail) & buffer->mask);
    }

    return result;
} /* cbuf_GetUsedBytes */
