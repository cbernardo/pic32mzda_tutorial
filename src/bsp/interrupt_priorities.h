/**
 * File : interrupt_priorities.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : mapping of interrupt priority levels and shadow registers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/*
 * Design notes:
 * + all interrupts shall use Shadow Register Sets, IPLxSRS
 * + default values (not yet assigned by the user) are IPL1
 * + IPL values are 0..7 (interrupt priority level)
 * + ISL values are 0..3 (interrupt subpriority level)
 */

#ifndef INTERRUPT_PRIORITIES_H
#define INTERRUPT_PRIORITIES_H

/* TIMERS 1..9 */
#define CFG_T1_IPL  (7UL)           /* Timer 1: system millisecond timer */
#define CFG_T1_ISL  (0UL)
#define CFG_T1_SRS  IPL7SRS
#define CFG_T2_IPL  (1UL)           /* Timer 2: currently unused */
#define CFG_T2_ISL  (0UL)
#define CFG_T2_SRS  IPL1SRS
#define CFG_T3_IPL  (1UL)           /* Timer 3: currently unused */
#define CFG_T3_ISL  (0UL)
#define CFG_T3_SRS  IPL1SRS
#define CFG_T4_IPL  (1UL)           /* Timer 4: currently unused */
#define CFG_T4_ISL  (0UL)
#define CFG_T4_SRS  IPL1SRS
#define CFG_T5_IPL  (1UL)           /* Timer 5: currently unused */
#define CFG_T5_ISL  (0UL)
#define CFG_T5_SRS  IPL1SRS
#define CFG_T6_IPL  (1UL)           /* Timer 6: currently unused */
#define CFG_T6_ISL  (0UL)
#define CFG_T6_SRS  IPL1SRS
#define CFG_T7_IPL  (1UL)           /* Timer 7: currently unused */
#define CFG_T7_ISL  (0UL)
#define CFG_T7_SRS  IPL1SRS
#define CFG_T8_IPL  (1UL)           /* Timer 8: currently unused */
#define CFG_T8_ISL  (0UL)
#define CFG_T8_SRS  IPL1SRS
#define CFG_T9_IPL  (1UL)           /* Timer 9: currently unused */
#define CFG_T9_ISL  (0UL)
#define CFG_T9_SRS  IPL1SRS

/* UARTS 1..6 */
#define CFG_U1RX_IPL  (1UL)         /* UART 1: unused */
#define CFG_U1RX_ISL  (0UL)
#define CFG_U1RX_SRS  IPL1SRS
#define CFG_U1TX_IPL  (1UL)
#define CFG_U1TX_ISL  (0UL)
#define CFG_U1TX_SRS  IPL1SRS

#define CFG_U2RX_IPL  (1UL)         /* UART 2: serial debug port */
#define CFG_U2RX_ISL  (3UL)
#define CFG_U2RX_SRS  IPL1SRS
#define CFG_U2TX_IPL  (1UL)
#define CFG_U2TX_ISL  (2UL)
#define CFG_U2TX_SRS  IPL1SRS

#define CFG_U3RX_IPL  (1UL)         /* UART 3: unused */
#define CFG_U3RX_ISL  (0UL)
#define CFG_U3RX_SRS  IPL1SRS
#define CFG_U3TX_IPL  (1UL)
#define CFG_U3TX_ISL  (0UL)
#define CFG_U3TX_SRS  IPL1SRS

#define CFG_U4RX_IPL  (1UL)         /* UART 4: unused */
#define CFG_U4RX_ISL  (0UL)
#define CFG_U4RX_SRS  IPL1SRS
#define CFG_U4TX_IPL  (1UL)
#define CFG_U4TX_ISL  (0UL)
#define CFG_U4TX_SRS  IPL1SRS

#define CFG_U5RX_IPL  (1UL)         /* UART 5: unused */
#define CFG_U5RX_ISL  (0UL)
#define CFG_U5RX_SRS  IPL1SRS
#define CFG_U5TX_IPL  (1UL)
#define CFG_U5TX_ISL  (0UL)
#define CFG_U5TX_SRS  IPL1SRS

#define CFG_U6RX_IPL  (1UL)         /* UART 6: unused */
#define CFG_U6RX_ISL  (0UL)
#define CFG_U6RX_SRS  IPL1SRS
#define CFG_U6TX_IPL  (1UL)
#define CFG_U6TX_ISL  (0UL)
#define CFG_U6TX_SRS  IPL1SRS

#endif /* INTERRUPT_PRIORITIES_H */
