/**
 * File : refclk.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides control of the Reference Clocks.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#include "mcu_reg.h"
#include "refclk.h"
#include "sysclk.h"
#include "pbclk.h"


/*******************************************************************************
 *              PRIVATE MACROS
 ******************************************************************************/
#define REFCK_ON        (1UL << 15)     /* 1 = ON, 0 = OFF */
#define REFCK_SIDL      (1UL << 13)     /* 1 = STOP when in IDLE mode */
#define REFCK_OUT       (1UL << 12)     /* 1 = drive REFCLKOx pin */
#define REFCK_RSLP      (1UL << 11)     /* 1 = RUN when in SLEEP mode */
#define REFCK_DIVSWEN   (1UL << 9)      /* 1 = initiate DIvisor Switch (cleared by HW) */ 
#define REFCK_ACTIVE    (1UL << 8)      /* RO, 1 = clock request is active; do not write CTRL REG */
#define CKDIV_TO_REG(ARG)   ((((uint32_t)(ARG)) & 0x7FFFUL) << 16)  /* CKDIV to register value */
#define CKDIV_FROM_REG(ARG) ((((uint32_t)(ARG)) >> 16) & 0x7FFFUL)  /* register value to CKDIV */
#define CKDIV_MASK      (0x7FFFUL << 16)                            /* mask for CKDIV register */
#define ROSEL_TO_REG(ARG)   (((uint32_t)(ARG)) & 0x0FUL)            /* ROSEL to register value */
#define ROSEL_FROM_REG(ARG) (((uint32_t)(ARG)) & 0x0FUL)            /* register value to ROSEL */
#define ROSEL_MASK      (0x0FUL)                                    /* mask for ROSEL register */


/*******************************************************************************
 *              PRIVATE VARIABLES AND CONSTANTS
 ******************************************************************************/
static uint32_t const m_clkInFreq[5] =
{
    (CFG_REFCLK1_FREQ),
    (CFG_REFCLK2_FREQ),
    (CFG_REFCLK3_FREQ),
    (CFG_REFCLK4_FREQ),
    (CFG_REFCLK5_FREQ),
};

static Register_t * const m_ctrl[5] =
{
    (Register_t *)&REFO1CON,
    (Register_t *)&REFO2CON,
    (Register_t *)&REFO3CON,
    (Register_t *)&REFO4CON,
    (Register_t *)&REFO5CON,
};

static Register_t * const m_trim[5] =
{
    (Register_t *)&REFO1TRIM,
    (Register_t *)&REFO2TRIM,
    (Register_t *)&REFO3TRIM,
    (Register_t *)&REFO4TRIM,
    (Register_t *)&REFO5TRIM,
};


/*******************************************************************************
 *              PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/

/* GENERAL REFCLK FUNCTIONS */
static void      refck_wait_busy(int const idx, bool *ck_on);
static int       refck_enable(int const idx, bool const enable);
static bool      refck_enabled(int const idx);
static uint32_t  refck_get_frequency(int const idx);
static int       refck_set_div(int const idx, uint32_t const div, uint32_t const trim);
static int       refck_set_src(int const idx, uint32_t const sourceClk);
static int       refck_enable_out(int const idx, bool const enable);

/* REFCLK 1 */
static int       refck_enable1(bool const enable);
static bool      refck_enabled1(void);
static uint32_t  refck_get_frequency1(void);
static int       refck_set_div1(uint32_t const div, uint32_t const trim);
static int       refck_set_src1(uint32_t const sourceClk);
static int       refck_enable_out1(bool const enable);

/* REFCLK 2 */
static int       refck_enable2(bool const enable);
static bool      refck_enabled2(void);
static uint32_t  refck_get_frequency2(void);
static int       refck_set_div2(uint32_t const div, uint32_t const trim);
static int       refck_set_src2(uint32_t const sourceClk);
static int       refck_enable_out2(bool const enable);

/* REFCLK 3 */
static int       refck_enable3(bool const enable);
static bool      refck_enabled3(void);
static uint32_t  refck_get_frequency3(void);
static int       refck_set_div3(uint32_t const div, uint32_t const trim);
static int       refck_set_src3(uint32_t const sourceClk);
static int       refck_enable_out3(bool const enable);

/* REFCLK 4 */
static int       refck_enable4(bool const enable);
static bool      refck_enabled4(void);
static uint32_t  refck_get_frequency4(void);
static int       refck_set_div4(uint32_t const div, uint32_t const trim);
static int       refck_set_src4(uint32_t const sourceClk);
static int       refck_enable_out4(bool const enable);

/* REFCLK 5 */
static int       refck_enable5(bool const enable);
static bool      refck_enabled5(void);
static uint32_t  refck_get_frequency5(void);
static int       refck_set_div5(uint32_t const div, uint32_t const trim);
static int       refck_set_src5(uint32_t const sourceClk);
static int       refck_enable_out5(bool const enable);


/*******************************************************************************
 *              REFCLK API
 ******************************************************************************/

DrvRefClk_t RefClk1 =
{
    .Enable = refck_enable1,
    .Enabled = refck_enabled1,
    .GetFrequency = refck_get_frequency1,
    .SetDivisor = refck_set_div1,
    .SetSourceClk = refck_set_src1,
    .EnableClkOut = refck_enable_out1,
};

DrvRefClk_t RefClk2 =
{
    .Enable = refck_enable2,
    .Enabled = refck_enabled2,
    .GetFrequency = refck_get_frequency2,
    .SetDivisor = refck_set_div2,
    .SetSourceClk = refck_set_src2,
    .EnableClkOut = refck_enable_out2,
};

DrvRefClk_t RefClk3 =
{
    .Enable = refck_enable3,
    .Enabled = refck_enabled3,
    .GetFrequency = refck_get_frequency3,
    .SetDivisor = refck_set_div3,
    .SetSourceClk = refck_set_src3,
    .EnableClkOut = refck_enable_out3,
};

DrvRefClk_t RefClk4 =
{
    .Enable = refck_enable4,
    .Enabled = refck_enabled4,
    .GetFrequency = refck_get_frequency4,
    .SetDivisor = refck_set_div4,
    .SetSourceClk = refck_set_src4,
    .EnableClkOut = refck_enable_out4,
};

DrvRefClk_t RefClk5 =
{
    .Enable = refck_enable5,
    .Enabled = refck_enabled5,
    .GetFrequency = refck_get_frequency5,
    .SetDivisor = refck_set_div5,
    .SetSourceClk = refck_set_src5,
    .EnableClkOut = refck_enable_out5,
};


/*******************************************************************************
 *              PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/

/* GENERAL REFCLK FUNCTIONS */
static void  refck_wait_busy(int const idx, bool *clkOn)
{
    uint32_t ck_on;
    uint32_t ck_active;

    do
    {
        uint32_t regval = m_ctrl[idx]->reg;
        ck_on = (regval >> 15) & 1UL;
        ck_active = (regval >> 8) & 1UL;
    } while (ck_on != ck_active);

    if (0 != clkOn)
    {
        *clkOn = (1 == ck_on);
    }

    return;
}

static int  refck_enable(int const idx, bool const enable)
{
    bool ck_on;
    refck_wait_busy(idx, &ck_on);

    if ((!ck_on) && (enable))
    {
        m_ctrl[idx]->set = REFCK_ON;
    }
    else if ((ck_on) && (!enable))
    {
        m_ctrl[idx]->clr = REFCK_ON;
    }

    return 0;
}

static bool  refck_enabled(int const idx)
{
    bool ck_on;
    refck_wait_busy(idx, &ck_on);
    return ck_on;
}

static uint32_t  refck_get_frequency(int const idx)
{
    bool ck_on;
    uint32_t div;
    uint32_t freq = 0;
    refck_wait_busy(idx, &ck_on);

    if (ck_on)
    {
        while (0 != (m_ctrl[idx]->reg & (REFCK_DIVSWEN)))
        {
            ; /* wait for the divisor switch to complete */
        }
    }

    div = CKDIV_FROM_REG(m_ctrl[idx]->reg);

    switch (ROSEL_FROM_REG(m_ctrl[idx]->reg))
    {
    case REFCLK_SRC_SYS:
    {
        freq = SysClk.GetFrequency();
        break;
    }

    case REFCLK_SRC_PB1:
    {
        freq = PbClk1.GetFrequency();
        break;
    }

    case REFCLK_SRC_POSC:
    {
        freq = (CFG_POSC_FREQ);
        break;
    }

    case REFCLK_SRC_FRC:
    {
        freq = (CFG_FRC_FREQ);
        break;
    }

    case REFCLK_SRC_LPRC:
    {
        freq = (CFG_LPRC_FREQ);
        break;
    }

    case REFCLK_SRC_SOSC:
    {
        freq = (CFG_SOSC_FREQ);
        break;
    }

    case REFCLK_SRC_SPLL:
    {
        freq = SysClk.GetSPLLFrequency();
        break;
    }

    case REFCLK_SRC_CLKIN:
    {
        freq = m_clkInFreq[idx];
        break;
    }

    case REFCLK_SRC_BFRC:
    {
        freq = (CFG_BFRC_FREQ);
        break;
    }

    default:
    {
        /* unhandled case; return will be 0Hz */
        break;
    }
    }

    if (0 != freq)
    {
        /* calculate frequency rounded to nearest integer value */
        uint32_t trim = m_trim[idx]->reg >> 23;

        /* note: ROTRIM has no effect if RODIV = 0 */
        if ((0 == trim) || (0 == div))
        {
            freq += div;

            if (0 == div)
            {
                div = 1UL;
            }
            else
            {
                div <<= 1UL;
            }

            freq /= div;
        }
        else
        {
            uint64_t tmpf = ((uint64_t) freq) << 9;
            uint64_t tmpd = (((uint64_t) div) << 9) + (uint64_t)trim;
            tmpf += tmpd;
            tmpd <<= 1;
            freq = (uint32_t)(tmpf / tmpd);
        }
    }

    return freq;
}

static int  refck_set_div(int const idx, uint32_t const div, uint32_t const trim)
{
    int result = -1;

    if ((32767UL >= div) && (trim < 512))
    {
        bool ck_on;
        refck_wait_busy(idx, &ck_on);
        result = 0;

        if (ck_on)
        {
            while (0 != (m_ctrl[idx]->reg & (REFCK_DIVSWEN)))
            {
                ; /* wait for the divisor switch to complete */
            }
        }

        uint32_t currentDiv = CKDIV_FROM_REG(m_ctrl[idx]->reg);
        bool     fswitch = false;

        if (div != currentDiv)
        {
            /* set divisor */
            fswitch = true;
            uint32_t regval = m_ctrl[idx]->reg & (~CKDIV_MASK);
            regval |= CKDIV_TO_REG(div);
            m_ctrl[idx]->reg = regval;
        }

        if (trim != (m_trim[idx]->reg >> 23))
        {
            fswitch = true;
            m_trim[idx]->reg = (trim << 23);
        }

        if ((fswitch) && (ck_on))
        {
            m_ctrl[idx]->set = REFCK_DIVSWEN;
        }
    }

    return result;
}

static int  refck_set_src(int const idx, uint32_t const sourceClk)
{
    int result = -1;

    if ((9 >= sourceClk) && (6 != sourceClk))
    {
        // if ON switch OFF and plan to switch on after altering setting
        bool ck_on;
        refck_wait_busy(idx, &ck_on);
        result = 0;

        if (ck_on)
        {
            while (0 != (m_ctrl[idx]->reg & (REFCK_DIVSWEN)))
            {
                ; /* wait for the divisor switch to complete */
            }
        }

        uint32_t regval = m_ctrl[idx]->reg;

        if (ROSEL_FROM_REG(regval) != sourceClk)
        {
            regval &= (~(ROSEL_MASK));
            regval |= ROSEL_TO_REG(sourceClk);
            m_ctrl[idx]->reg = regval;

            if (ck_on)
            {
                m_ctrl[idx]->set = REFCK_DIVSWEN;
            }
        }
    }

    return result;
}

static int  refck_enable_out(int const idx, bool const enable)
{
    if (enable)
    {
        m_ctrl[idx]->set = (REFCK_OUT);
    }
    else
    {
        m_ctrl[idx]->clr = (REFCK_OUT);
    }

    return 0;
}


/* REFCLK 1 */
static int  refck_enable1(bool const enable)
{
    return refck_enable(0, enable);
}

static bool  refck_enabled1(void)
{
    return refck_enabled(0);
}

static uint32_t  refck_get_frequency1(void)
{
    return refck_get_frequency(0);
}

static int  refck_set_div1(uint32_t const div, uint32_t const trim)
{
    return refck_set_div(0, div, trim);
}

static int  refck_set_src1(uint32_t const sourceClk)
{
    return refck_set_src(0, sourceClk);
}

static int  refck_enable_out1(bool const enable)
{
    return refck_enable_out(0, enable);
}


/* REFCLK 2 */
static int  refck_enable2(bool const enable)
{
    return refck_enable(1, enable);
}

static bool  refck_enabled2(void)
{
    return refck_enabled(1);
}

static uint32_t  refck_get_frequency2(void)
{
    return refck_get_frequency(1);
}

static int  refck_set_div2(uint32_t const div, uint32_t const trim)
{
    return refck_set_div(1, div, trim);
}

static int  refck_set_src2(uint32_t const sourceClk)
{
    return refck_set_src(1, sourceClk);
}

static int  refck_enable_out2(bool const enable)
{
    return refck_enable_out(1, enable);
}


/* REFCLK 3 */
static int  refck_enable3(bool const enable)
{
    return refck_enable(2, enable);
}

static bool  refck_enabled3(void)
{
    return refck_enabled(2);
}

static uint32_t  refck_get_frequency3(void)
{
    return refck_get_frequency(2);
}

static int  refck_set_div3(uint32_t const div, uint32_t const trim)
{
    return refck_set_div(2, div, trim);
}

static int  refck_set_src3(uint32_t const sourceClk)
{
    return refck_set_src(2, sourceClk);
}

static int  refck_enable_out3(bool const enable)
{
    return refck_enable_out(2, enable);
}


/* REFCLK 4 */
static int  refck_enable4(bool const enable)
{
    return refck_enable(3, enable);
}

static bool  refck_enabled4(void)
{
    return refck_enabled(3);
}

static uint32_t  refck_get_frequency4(void)
{
    return refck_get_frequency(3);
}

static int  refck_set_div4(uint32_t const div, uint32_t const trim)
{
    return refck_set_div(3, div, trim);
}

static int  refck_set_src4(uint32_t const sourceClk)
{
    return refck_set_src(3, sourceClk);
}

static int  refck_enable_out4(bool const enable)
{
    return refck_enable_out(3, enable);
}


/* REFCLK 5 */
static int  refck_enable5(bool const enable)
{
    return refck_enable(4, enable);
}

static bool  refck_enabled5(void)
{
    return refck_enabled(4);
}

static uint32_t  refck_get_frequency5(void)
{
    return refck_get_frequency(4);
}

static int  refck_set_div5(uint32_t const div, uint32_t const trim)
{
    return refck_set_div(4, div, trim);
}

static int  refck_set_src5(uint32_t const sourceClk)
{
    return refck_set_src(4, sourceClk);
}

static int  refck_enable_out5(bool const enable)
{
    return refck_enable_out(4, enable);
}
