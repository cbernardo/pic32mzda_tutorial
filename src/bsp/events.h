/**
 * File : events.h
 * Copyright (C) 2022 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Declares the EVENT signaling system.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* Implementation notes:
 *
 * 1. Defining the global macro CFG_PERF_EVENTS = 1 will build additional
 *    code to collect performance data on events. This data can be
 *    useful for identifying problems such as an event being set
 *    multiple times (the function which responds is missing some events)
 *    or excessive delays before responding to an event. It is the
 *    32-bit core counter which is used to time events; at a clock
 *    frequency of 200MHz this counter will overflow every
 *    42.94967296 seconds.
 *
 * 2. The implementation uses a single bit to represent each event;
 *    this results in less RAM being used to represent the events
 *    but slower execution times and more Program Flash being used
 *    due to the additional instructions.
 */

#define CFG_PERF_EVENTS (1)

#ifndef SYS_EVENT_H
#define SYS_EVENT_H

#include <stdbool.h>


/*******************************************************************************
 *              PERFORMANCE DATA
 ******************************************************************************/
#if defined(CFG_PERF_EVENTS) && (1 == (CFG_PERF_EVENTS))
typedef struct
{
    unsigned int count;         /* number of times the event was set */
    unsigned int missed;        /* number of times the event was set without being cleared */
    unsigned int min_latency;   /* minimum number of core counter changes between set and test */
    unsigned int max_latency;   /* maximum number of core counter changes between set and test */
} EventPerf_t;
#endif

/*******************************************************************************
 *              API
 ******************************************************************************/

typedef struct
{
    /**
     * @function Set
     * @detail   Sets the given event flag; invalid arguments are ignored.
     *           Performance data (<b>count</b>, <b>missed</b>) is updated.
     *
     * @param    event [IN] must be a positive integer from Event_e,
     *           excluding EVT_END_MARKER.
     */
    void   (*Set)  (int const event);

    /**
     * @function Test
     * @detail   Tests and clears the given event flag. Invalid arguments
     *           are ignored. Latency data is updated.
     *
     * @param    event [IN] must be a positive integer from Event_e,
     *           excluding EVT_END_MARKER.
     * @return   true if the specified event is valid and set, otherwise false.
     */
    bool   (*Test) (int const event);

    /**
     * @function Peek
     * @detail   Checks the given event flag and returns true if it is set;
     *           invalid arguments are ignored. The flag is not modified
     *           by this function and performance data is not updated.
     *
     * @param    event [IN] must be a positive integer from Event_e,
     *           excluding EVT_END_MARKER.
     * @return   true if the specified event is valid and set, otherwise false.
     */
    bool   (*Peek) (int const event);

    #if defined(CFG_PERF_EVENTS) && (1 == (CFG_PERF_EVENTS))
    /**
     * @function GetPerfData
     * @detail   Returns the event performance data and maximum number of
     *           event entries.
     *
     * @param    performanceData [IN, OUT] will receive a pointer to the first
     *           event performance structure.
     * @param    numberOfEntries [IN, OUT] will receive the number of event entries.
     */
    void   (*GetPerfData) (EventPerf_t const **performanceData, int *numberOfEntries);
    #endif
} const SysEvent_t;

extern SysEvent_t const Event;


/*******************************************************************************
 *              EVENT DEFINITIONS
 ******************************************************************************/

typedef enum
{
    EVT_EXAMPLE = 0,    /* */
    EVT_END_MARKER      /* end marker only, not a valid event */
} Event_e;

#endif /* SYS_EVENT_H */
