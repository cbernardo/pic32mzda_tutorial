/**
 * File : console.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Defines the debug console API.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "console.h"
#include "uart.h"
#include "syserr.h"

/*******************************************************************************
 *              PRIVATE MACROS
 ******************************************************************************/
#define uart                (Uart2)     /* UART used by the debug console */
#define CFG_CONSOLE_BITRATE (115200)    /* UART bit rate */

/*******************************************************************************
 *              PRIVATE VARIABLES
 ******************************************************************************/
static bool m_init;
static bool m_shutdown;


/*******************************************************************************
 *              EXTERNAL FUNCTION DECLARATIONS
 ******************************************************************************/
void _mon_putc(char c);             /* required for printf() */


/*******************************************************************************
 *              PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/
static int           con_Init(void);
static void          con_Shutdown(void);
static int           con_SendByte(int const byte);
static int           con_SendString(void const *buffer, int const length);
static int           con_ReceiveByte(void *byte);
static int           con_ReceiveString(void *buffer, int *length);
static int           con_TxFlush(void);
static int           con_RxFlush(void);
static UartStatus_t  con_GetStatus(void);
static void          con_ClearError(void);
static int           con_Loopback(bool const enable);

/*******************************************************************************
 *              EXTERNAL FUNCTION DEFINITIONS
 ******************************************************************************/

/* @function _mon_putc
 * @detail   transmits a single character. End of line translations are performed;
 *           '\n' is translated to "\r\n". This operation blocks until the character
 *           is queued for transmission.
 */
void _mon_putc(char c)
{
    static bool has_lf = false; /* true if preceeding char was '\r' */

    if ((!m_init) && (!m_shutdown))
    {
        (void) con_Init();
    }

    if ((m_init) && (!m_shutdown))
    {
        if ('\n' == c)
        {
            if (!has_lf)
            {
                (void) uart.SendByte('\r');
                (void) uart.SendByte(c);
            }

            has_lf = false;
        }
        else if ('\r' == c)
        {
            has_lf = true;
            (void) uart.SendByte(c);
            (void) uart.SendByte('\n');
        }
        else
        {
            has_lf = false;
            (void) uart.SendByte(c);
        }
    }

    return;
}

DrvConsole_t Console =
{
    .Init = con_Init,
    .Shutdown = con_Shutdown,
    .SendByte = con_SendByte,
    .SendString = con_SendString,
    .ReceiveByte = con_ReceiveByte,
    .ReceiveString = con_ReceiveString,
    .TxFlush = con_TxFlush,
    .RxFlush = con_RxFlush,
    .GetStatus = con_GetStatus,
    .ClearError = con_ClearError,
    .Loopback = con_Loopback,
};


/*******************************************************************************
 *              PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/

/**
 * @function con_Init
 * @detail   initializes the UART for operation at 115200bps,8N1
 */
static int con_Init(void)
{
    int result = 0;
    m_shutdown = false; /* clear to allow normal operation */

    if (!m_init)
    {
        UartCfg_t cfg =
        {
            .enable_tx = 1,
            .enable_rx = 1,
            .invert_tx = 0,
            .invert_rx = 0,
            .enable_hwflow = 0,
            .parity_select = UART_PARITY_NONE,
            .clock_select = UART_CK_PBCLK2,
            .wake_from_sleep = 0,
            .reserved = 0,
            .bitrate = CFG_CONSOLE_BITRATE,
        };

        result = uart.Init(&cfg);

        if (0 == result)
        {
            m_init = true;
        }
    }

    return result;
} /* con_Init */

/**
 * @function con_Shutdown
 * @detail   shuts down the debug console and prevents further input/output
 *           unless an explicit call to con_Init is performed.
 */
static void con_Shutdown(void)
{
    if ((m_init) && (!m_shutdown))
    {
        uart.Shutdown();
    }

    m_shutdown = true;
    return;
} /* con_Shutdown */

/**
 * @function con_SendByte
 * @detail   Queues a single byte for transmission.
 */
static int con_SendByte(int const byte)
{
    int result = ERR_FAIL;

    if ((!m_init) && (!m_shutdown))
    {
        result = con_Init();
    }

    if (m_init)
    {
        result = uart.SendByte(byte);
    }

    return result;
} /* con_SendByte */

/**
 * @function con_SendString
 * @detail   Queues a byte string for transmission; this function may block if
 *           the transmission buffer is filled.
 */
static int con_SendString(void const *buffer, int const length)
{
    int result = ERR_FAIL;

    if ((!m_init) && (!m_shutdown))
    {
        result = con_Init();
    }

    if (m_init)
    {
        result = uart.SendString(buffer, length);
    }

    return result;
} /* con_SendString */

/**
 * @function con_ReceiveByte
 * @detail   Receives a single byte if data is available.
 */
static int con_ReceiveByte(void *byte)
{
    int result = ERR_FAIL;

    if ((!m_init) && (!m_shutdown))
    {
        result = con_Init();
    }

    if (m_init)
    {
        result = uart.ReceiveByte(byte);
    }

    return result;
}

/**
 * @function con_ReceiveString
 * @detail   Receives up to <b>length</b> bytes of data.
 */
static int con_ReceiveString(void *buffer, int *length)
{
    int result = ERR_FAIL;

    if ((!m_init) && (!m_shutdown))
    {
        result = con_Init();
    }

    if (m_init)
    {
        result = uart.ReceiveString(buffer, length);
    }

    return result;
} /* con_ReceiveString */

/**
 * @function con_TxFlush
 * @detail   Flushes the console transmit buffer.
 */
static int con_TxFlush(void)
{
    int result = 0;

    if (m_init)
    {
        result = uart.TxFlush();
    }

    return result;
} /* con_TxFlush */

/**
 * @function con_RxFlush
 * @detail   Flushes the console receive buffer.
 */
static int con_RxFlush(void)
{
    int result = 0;

    if (m_init)
    {
        result = uart.RxFlush();
    }

    return result;
} /* con_RxFlush */

/**
 * @function con_GetStatus
 * @detail   Retrieves the console status.
 */
static UartStatus_t con_GetStatus(void)
{
    return uart.GetStatus();
}

/**
 * @function con_ClearError
 * @detail   Clears the console error status.
 */
static void con_ClearError(void)
{
    if (m_init)
    {
        uart.ClearError();
    }

    return;
} /* con_ClearError */

/**
 * @function con_Loopback
 * @detail   Enables or disables the hardware loopback function.
 */
static int con_Loopback(bool const enable)
{
    int result = ERR_FAIL;

    if ((!m_init) && (!m_shutdown))
    {
        result = con_Init();
    }

    if (m_init)
    {
        result = uart.Loopback(enable);
    }

    return result;
} /* con_Loopback */
