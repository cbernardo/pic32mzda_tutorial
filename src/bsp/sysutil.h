/**
 * File : sysutil.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Miscellaneous system tools.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */


#ifndef SYSUTIL_H
#define SYSUTIL_H

#include <stdbool.h>
#include <stdint.h>

typedef unsigned int SYSTICK;
typedef unsigned int SYSCYCLE;

/* *INDENT-OFF* */
/* Sys API:
 *  int32_t   (*Init)(void);
 *  int32_t   (*UnInit)(void);
 *  SYSCYCLE  (*CycleCounter)(void);
 *  SYSTICK   (*MillisecTime)(void);
 *  void      (*WaitMicrosec)(unsigned int delay);
 *  void      (*WaitMillisec)(unsigned int delay);
 *  bool      (*TimeoutMicrosec)(unsigned int const start, unsigned int const microsec);
 *  bool      (*TimeoutMillisec)(unsigned int const start, unsigned int const millisec);
 *  unsigned int  (*DisableInterrupts)(void);
 *  void      (*EnableInterrupts)(unsigned int const enable);
 *  void      (*Reset)(void) __attribute__((__noreturn__));
 */
typedef struct
{
    /**
     * @function Init
     * @detail   Initializes resources required to support functions in the Sys category.
     *           The _on_start function must be invoked once prior to invoking this function.
     */
    void      (*Init)(void);

    /**
     * @function UnInit
     * @detail   Disables resources used by the Sys category.
     */
    void      (*UnInit)(void);

    /**
     * @function CycleCounter
     * @return   CP0 cycle counter (frequency = SYSCLK / 2).
     */
    SYSCYCLE  (*CycleCounter)(void);

    /**
     * @function MillisecTime
     * @detail   Requires a single successful call to Sys->Init; not valid after Sys->UnInit.
     * @return   system millisecond timer value.
     */
    SYSTICK   (*MillisecTime)(void);

    /**
     * @function WaitMicrosec
     * @detail   Blocks for at least the requested number of microseconds; granularity is
     *           approximately 0.5 microseconds at SYSCLK = 200MHz and greater for slower
     *           clock speeds. For clock speeds less than 1MHz this function becomes
     *           unreliable.
     *
     * @param    delay [IN] is the number of microseconds to wait.
     */
    void      (*WaitMicrosec)(unsigned int delay);

    /**
     * @function WaitMillisec
     * @detail   Blocks for at the specified number of milliseconds.
     *
     * @param    delay [IN] is the number of milliseconds to wait.
     */
    void      (*WaitMillisec)(unsigned int delay);

    /**
     * @function TimeoutMicrosec
     *
     * @param    start    [IN] is the Cycle Count at the start of the interval.
     * @param    microsec [IN] is the timeout period.
     * @return   true if the specified number of microseconds has lapsed since the
     *           specified start.
     */
    bool      (*TimeoutMicrosec)(unsigned int const start, unsigned int const microsec);

    /**
     * @function TimeoutMillisec
     *
     * @param    start    [IN] is the Cycle Count at the start of the interval.
     * @param    millisec [IN] is the timeout period.
     * @return   true if the specified number of milliseconds has lapsed since the
     *           specified start.
     */
    bool      (*TimeoutMillisec)(unsigned int const start, unsigned int const millisec);

    /**
     * @function DisableInterrupts
     * @detail   Disables global interrupts and returns the previous global interrupt setting.
     */
    unsigned int  (*DisableInterrupts)(void);

    /**
     * @function EnableInterrupts
     * @detail   Enables global interrupts if the enable flag is non-zero.
     *
     * @param    enable [IN] is true to enable and false to disable global interrupts.
     */
    void      (*EnableInterrupts)(unsigned int const enable);

    /**
     * @function Reset
     * @detail   Initiates a Software Reset; the function does not return.
     */
    void      __attribute__((__noreturn__)) (*Reset)(void);
} const DrvSys_t;
/* *INDENT-ON* */

extern DrvSys_t Sys;

#endif /* SYSUTIL_H */
