/**
 * File : syserr.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : error values for the firmware being implemented
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

/*
 * Implementation note:
 * + zero indicates no error
 * + all errors are negative numbers
 * + application specific errors begin at -20000
 */

#ifndef SYSERR_H
#define SYSERR_H

typedef enum
{
    ERR_NONE = 0,                               /* no error */
    ERR_FAIL = -1,                              /* generic failure flag */
    ERR_BUSY = -2,                              /* operation cannot be performed at this time */
    ERR_PARAM = -3,                             /* invalid parameter (except for out-of-range) */
    ERR_NODEV = -4,                             /* no such device / feature disabled */
    ERR_AGAIN = -5,                             /* no data available; try again later */
    ERR_RANGE = -6,                             /* parameter out of range */
    ERR_OVERFLOW = -7,                          /* buffer overflow */
    ERR_PARITY = -8,                            /* parity mismatch */
    ERR_SYNC = -9,                              /* synchronization or framing error */

    ERR_APP_BASE = -20000,                      /* start value for app-specific errors */
} SysError_e;

#endif /* SYSERR_H */
