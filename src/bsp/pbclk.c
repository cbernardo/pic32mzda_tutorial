/**
 * File : pbclk.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Provides control of the Peripheral Block Clocks.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#include "cfg_clocks.h"
#include "pbclk.h"
#include "sysclk.h"
#include "mcu_reg.h"


/*******************************************************************************
 *              PRIVATE MACROS
 ******************************************************************************/
#define PBCLK_ON    (1UL << 15)
#define PBCLK_READY (1UL << 11)
#define PBCLK_DIV   (0x7FUL)


/*******************************************************************************
 *              PRIVATE VARIABLES AND CONSTANTS
 ******************************************************************************/
static uint32_t const m_clocks = ((CFG_USE_PBCLK_1) || ((CFG_USE_PBCLK_2) << 1)
                                  || ((CFG_USE_PBCLK_3) << 2) || ((CFG_USE_PBCLK_4) << 3)
                                  || ((CFG_USE_PBCLK_5) << 4) || ((CFG_USE_PBCLK_6) << 5)
                                  || ((CFG_USE_PBCLK_7) << 6));

static Register_t * const m_ctrl[] =
{
    (Register_t *)&PB1DIV,
    (Register_t *)&PB2DIV,
    (Register_t *)&PB3DIV,
    (Register_t *)&PB4DIV,
    (Register_t *)&PB5DIV,
    (Register_t *)&PB6DIV,
    (Register_t *)&PB7DIV
};


/*******************************************************************************
 *              PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/

int       pb_enable(int const idx, bool const enable);
bool      pb_enabled(int const idx);
uint32_t  pb_get_frequency(int const idx);
int       pb_set_divisor(int const idx, uint32_t const div);

/* PBClk 1 */
#if defined(CFG_USE_PBCLK_1) && (0 != CFG_USE_PBCLK_1)
int       pb1_enable(bool const enable);
bool      pb1_enabled(void);
uint32_t  pb1_get_frequency(void);
int       pb1_set_divisor(uint32_t const div);
#endif

/* PBClk 2 */
#if defined(CFG_USE_PBCLK_2) && (0 != CFG_USE_PBCLK_2)
int       pb2_enable(bool const enable);
bool      pb2_enabled(void);
uint32_t  pb2_get_frequency(void);
int       pb2_set_divisor(uint32_t const div);
#endif

/* PBClk 3 */
#if defined(CFG_USE_PBCLK_3) && (0 != CFG_USE_PBCLK_3)
int       pb3_enable(bool const enable);
bool      pb3_enabled(void);
uint32_t  pb3_get_frequency(void);
int       pb3_set_divisor(uint32_t const div);
#endif

/* PBClk 4 */
#if defined(CFG_USE_PBCLK_4) && (0 != CFG_USE_PBCLK_4)
int       pb4_enable(bool const enable);
bool      pb4_enabled(void);
uint32_t  pb4_get_frequency(void);
int       pb4_set_divisor(uint32_t const div);
#endif

/* PBClk 5 */
#if defined(CFG_USE_PBCLK_5) && (0 != CFG_USE_PBCLK_5)
int       pb5_enable(bool const enable);
bool      pb5_enabled(void);
uint32_t  pb5_get_frequency(void);
int       pb5_set_divisor(uint32_t const div);
#endif

/* PBClk 6 */
#if defined(CFG_USE_PBCLK_6) && (0 != CFG_USE_PBCLK_6)
int       pb6_enable(bool const enable);
bool      pb6_enabled(void);
uint32_t  pb6_get_frequency(void);
int       pb6_set_divisor(uint32_t const div);
#endif

/* PBClk 7 */
#if defined(CFG_USE_PBCLK_7) && (0 != CFG_USE_PBCLK_7)
int       pb7_enable(bool const enable);
bool      pb7_enabled(void);
uint32_t  pb7_get_frequency(void);
int       pb7_set_divisor(uint32_t const div);
#endif


/*******************************************************************************
 *              PBCLK API
 ******************************************************************************/

#if defined(CFG_USE_PBCLK_1) && (0 != CFG_USE_PBCLK_1)
DrvPbClk_t PbClk1 =
{
    .Enable = pb1_enable,
    .Enabled = pb1_enabled,
    .GetFrequency = pb1_get_frequency,
    .SetDivisor = pb1_set_divisor,
};
#endif


#if defined(CFG_USE_PBCLK_2) && (0 != CFG_USE_PBCLK_2)
DrvPbClk_t PbClk2 =
{
    .Enable = pb2_enable,
    .Enabled = pb2_enabled,
    .GetFrequency = pb2_get_frequency,
    .SetDivisor = pb2_set_divisor,
};
#endif


#if defined(CFG_USE_PBCLK_3) && (0 != CFG_USE_PBCLK_3)
DrvPbClk_t PbClk3 =
{
    .Enable = pb3_enable,
    .Enabled = pb3_enabled,
    .GetFrequency = pb3_get_frequency,
    .SetDivisor = pb3_set_divisor,
};
#endif


#if defined(CFG_USE_PBCLK_4) && (0 != CFG_USE_PBCLK_4)
DrvPbClk_t PbClk4 =
{
    .Enable = pb4_enable,
    .Enabled = pb4_enabled,
    .GetFrequency = pb4_get_frequency,
    .SetDivisor = pb4_set_divisor,
};
#endif


#if defined(CFG_USE_PBCLK_5) && (0 != CFG_USE_PBCLK_5)
DrvPbClk_t PbClk5 =
{
    .Enable = pb5_enable,
    .Enabled = pb5_enabled,
    .GetFrequency = pb5_get_frequency,
    .SetDivisor = pb5_set_divisor,
};
#endif

#if defined(CFG_USE_PBCLK_6) && (0 != CFG_USE_PBCLK_6)
DrvPbClk_t PbClk6 =
{
    .Enable = pb6_enable,
    .Enabled = pb6_enabled,
    .GetFrequency = pb6_get_frequency,
    .SetDivisor = pb6_set_divisor,
};
#endif

#if defined(CFG_USE_PBCLK_7) && (0 != CFG_USE_PBCLK_7)
DrvPbClk_t PbClk7 =
{
    .Enable = pb7_enable,
    .Enabled = pb7_enabled,
    .GetFrequency = pb7_get_frequency,
    .SetDivisor = pb7_set_divisor,
};
#endif


/*******************************************************************************
 *              PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/

int  pb_enable(int32_t const idx, bool const enable)
{
    int result = -1;

    if (0 == idx)
    {
        /* PBCLK1 cannot be turned off */
        if (true == enable)
        {
            result = 0;
        }
        else
        {
            result = -1;
        }
    }
    else if ((1 <= idx) && (7 > idx) && (0 != (m_clocks & (1UL << idx))))
    {
        if (enable)
        {
            m_ctrl[idx]->set = (PBCLK_ON);
        }
        else
        {
            m_ctrl[idx]->clr = (PBCLK_ON);
        }

        result = 0;
    }

    return result;
} /* pb_enable */


bool  pb_enabled(int const idx)
{
    bool result = false;

    if (0 == idx)
    {
        result = true;
    }
    else if ((1 <= idx) && (7 > idx))
    {
        if (0 == (m_clocks & (1UL << idx)))
        {
            result = true;
        }
        else if (0 != (m_ctrl[idx]->reg & (PBCLK_ON)))
        {
            result = true;
        }
    }

    return result;
} /* pb_enabled */


uint32_t  pb_get_frequency(int const idx)
{
    uint32_t result = 0;

    if ((0 <= idx) && (7 > idx))
    {
        /* report frequency to nearest integer value */
        uint32_t divisor = (1UL + (m_ctrl[idx]->reg & (PBCLK_DIV)));
        result = (SysClk.GetFrequency() + (divisor >> 1)) / divisor;
    }

    return result;
} /* pb_get_frequency */


int  pb_set_divisor(int const idx, uint32_t const div)
{
    int result = -1;

    if ((0 <= idx) && (7 > idx) && (0 != (m_clocks & (1UL << idx))) && (0 < div) && (129UL > div))
    {
        while (0 == (m_ctrl[idx]->reg & (PBCLK_READY)))
        {
            ;   /* wait for a clock switch to complete */
        }

        uint32_t val = m_ctrl[idx]->reg;
        val &= ~(PBCLK_DIV);
        val |= (div - 1UL);
        m_ctrl[idx]->reg = val;
        result = 0;
    }

    return result;
}


/* PBClk 1 */
#if defined(CFG_USE_PBCLK_1) && (0 != CFG_USE_PBCLK_1)
int  pb1_enable(bool const enable)
{
    return pb_enable(0, enable);
}

bool  pb1_enabled(void)
{
    return pb_enabled(0);
}

uint32_t  pb1_get_frequency(void)
{
    return pb_get_frequency(0);
}

int  pb1_set_divisor(uint32_t const div)
{
    return pb_set_divisor(0, div);
}
#endif


/* PBClk 2 */
#if defined(CFG_USE_PBCLK_2) && (0 != CFG_USE_PBCLK_2)
int  pb2_enable(bool const enable)
{
    return pb_enable(1, enable);
}

bool  pb2_enabled(void)
{
    return pb_enabled(1);
}

uint32_t  pb2_get_frequency(void)
{
    return pb_get_frequency(1);
}

int  pb2_set_divisor(uint32_t const div)
{
    return pb_set_divisor(1, div);
}
#endif


/* PBClk 3 */
#if defined(CFG_USE_PBCLK_3) && (0 != CFG_USE_PBCLK_3)
int  pb3_enable(bool const enable)
{
    return pb_enable(2, enable);
}

bool  pb3_enabled(void)
{
    return pb_enabled(2);
}

uint32_t  pb3_get_frequency(void)
{
    return pb_get_frequency(2);
}

int  pb3_set_divisor(uint32_t const div)
{
    return pb_set_divisor(2, div);
}
#endif


/* PBClk 4 */
#if defined(CFG_USE_PBCLK_4) && (0 != CFG_USE_PBCLK_4)
int  pb4_enable(bool const enable)
{
    return pb_enable(3, enable);
}

bool  pb4_enabled(void)
{
    return pb_enabled(3);
}

uint32_t  pb4_get_frequency(void)
{
    return pb_get_frequency(3);
}

int  pb4_set_divisor(uint32_t const div)
{
    return pb_set_divisor(3, div);
}
#endif


/* PBClk 5 */
#if defined(CFG_USE_PBCLK_5) && (0 != CFG_USE_PBCLK_5)
int  pb5_enable(bool const enable)
{
    return pb_enable(4, enable);
}

bool  pb5_enabled(void)
{
    return pb_enabled(4);
}

uint32_t  pb5_get_frequency(void)
{
    return pb_get_frequency(4);
}

int  pb5_set_divisor(uint32_t const div)
{
    return pb_set_divisor(4, div);
}
#endif


/* PBClk 6 */
#if defined(CFG_USE_PBCLK_6) && (0 != CFG_USE_PBCLK_6)
int  pb6_enable(bool const enable)
{
    return pb_enable(5, enable);
}

bool  pb6_enabled(void)
{
    return pb_enabled(5);
}

uint32_t  pb6_get_frequency(void)
{
    return pb_get_frequency(5);
}

int  pb6_set_divisor(uint32_t const div)
{
    return pb_set_divisor(5, div);
}
#endif


/* PBClk 7 */
#if defined(CFG_USE_PBCLK_7) && (0 != CFG_USE_PBCLK_7)
int  pb7_enable(bool const enable)
{
    return pb_enable(6, enable);
}

bool  pb7_enabled(void)
{
    return pb_enabled(6);
}

uint32_t  pb7_get_frequency(void)
{
    return pb_get_frequency(6);
}

int  pb7_set_divisor(uint32_t const div)
{
    return pb_set_divisor(6, div);
}
#endif
