/**
 * File : main.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Completes system initialization and invokes the scheduler.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdint.h>

#include "startup.h"

int  __attribute__((noreturn)) main(void)
{
    _on_start();                /* complete microprocessor init */

    U2BRG = 53; /* Target speed: 115200; actual is 115740.741 (0.47%) */
    U2STAbits.UTXEN = 1;
    U2MODEbits.ON = 1;
    int32_t i;
    for (i = 0; i < 100; ++i)
    {
        while (!U2STAbits.TRMT)
        {
            ; /* waste time */
        }
        U2TXREG = '?';
    }

    while(1)
    {
        ; /* do nothing */
    }
}
