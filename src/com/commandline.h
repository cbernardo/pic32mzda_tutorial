/**
 * File : commandline.h
 * Copyright (C) 2023 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Extensible command line interface.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#ifndef COMMANDLINE_H
#define COMMANDLINE_H

/*******************************************************************************
 *              COMMAND LINE CONFIGURATION PARAMETERS
 ******************************************************************************/
#define CFG_CMD_MAXLEN  (128)   /* max line length [16..255] */
#define CFG_CMD_MAXARG  (5)     /* max arguments in a line, including command name [1..10] */
#define CFG_CMD_HISTORY (10)    /* size of history list (0 for none) [0..20] */


/*******************************************************************************
 *              PUBLIC TYPES
 ******************************************************************************/
typedef enum
{
    CLI_NOT_AVAIL = 0,      /* command cannot be run at this time */
    CLI_NO_ACCESS,          /* insufficient permission; acts as though command does not exist */
    CLI_ACCESS_OK           /* command can be run */
} CmdPermission_e;

typedef struct
{
    /* command text */
    char const       *name;
    /* obscure all but command arg if non-zero */
    int               obscure;
    /* [optional] function to determine if conditions allow access (access control, power state) */
    CmdPermission_e (*condition)(void);
    /* command function; return non-zero to suppress history (example, for security command) */
    int             (*function)(int const argc, char const **argv);
} const ConsoleCmd_t;


/*******************************************************************************
 *              PUBLIC FUNCTIONS
 ******************************************************************************/

/**
 * @function cmd_task
 * @detail   Persistent task to processes incoming text and dispatch commands.
 */
void cmd_task(void);

/**
 * @function cmd_divert
 * @detail   Diverts the processing of incoming console data from
 *           the command processor to a nominated function.
 *
 * @param    func : pointer to the new processing function;
 *           NULL to revert to command processor.
 */
void cmd_divert(void (*func)(void));

/*******************************************************************************
 *              USER-DEFINED GLOBAL COMMAND LIST
 ******************************************************************************/
extern ConsoleCmd_t g_serial_commands[];    /* use NULL name in entry to indicate end of list */

#endif
