/**
 * File : commandline.c
 * Copyright (C) 2023 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Extensible command line interface.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "commandline.h"
#include "console.h"


/*******************************************************************************
 *          CHECKS ON CONFIGURATION PARAMETERS
 ******************************************************************************/
#if !defined(CFG_CMD_MAXLEN) || ((CFG_CMD_MAXLEN) < 16) || ((CFG_CMD_MAXLEN) > 255)
#error CFG_CMD_MAXLEN must be defined and within the range 16..255
#endif

#if !defined(CFG_CMD_MAXARG) || ((CFG_CMD_MAXARG) < 1) || ((CFG_CMD_MAXARG) > 10)
#error CFG_CMD_MAXARG must be defined and within the range 1..10
#endif

#if !defined(CFG_CMD_HISTORY) || ((CFG_CMD_HISTORY) < 0) || ((CFG_CMD_HISTORY) > 20)
#error CFG_CMD_HISTORY must be defined and within the range 0..20
#endif


/*******************************************************************************
 *          PRIVATE DEFINITIONS
 ******************************************************************************/
#define TERM_BACK   (0x08)      /* ^H = backspace */
#define TERM_DEL    (0x7F)      /* delete (not typically coded by a terminal) */
#define TERM_CANCEL (0x18)      /* ^X = cancel */
#define TERM_PREV   (0x12)      /* ^R = history, previous line [DC2] */
#define TERM_NEXT   (0x14)      /* ^T = history, next line [DC4] */

/*******************************************************************************
 *          PRIVATE CONSTANTS
 ******************************************************************************/
static char const   m_cmdprompt[] = "> ";

/*******************************************************************************
 *          PRIVATE VARIABLES
 ******************************************************************************/
static void       (*m_diversion_func) (void);           /* console diversion function */
static char         m_cmdline[(CFG_CMD_MAXLEN) + 1];    /* incoming command line */
static char const  *m_argv[CFG_CMD_MAXARG];             /* pointers to command args */
static int          m_argc;                             /* number of command args */
static int          m_cmdlen;                           /* length of incoming command line */
#if defined(CFG_CMD_HISTORY) && (0 < (CFG_CMD_HISTORY))
static char         m_history[CFG_CMD_HISTORY][CFG_CMD_MAXLEN + 1];
static int          m_histSize; /* number of entries in history */
static int          m_histBase; /* earliest entry in history */
static int          m_histIdx;  /* index to entry being recalled */
static bool         m_recall;   /* true if current command is unmodified recall */
#endif


/*******************************************************************************
 *          PRIVATE FUNCTION DECLARATIONS
 ******************************************************************************/
static void cmd_process(void);
static void cmd_parse(void);
#if defined(CFG_CMD_HISTORY) && (0 < (CFG_CMD_HISTORY))
static void cmd_history_prev(void);
static void cmd_history_next(void);
#endif


/*******************************************************************************
 *          PUBLIC FUNCTIONS
 ******************************************************************************/
void cmd_divert(void (*func)(void))
{
    m_diversion_func = func;

    if (0 == func)
    {
        /* assume we're at the start of a line and issue a prompt */
        printf("%s", m_cmdprompt);
    }

    return;
} /* cmd_divert */

void cmd_task(void)
{
    if (0 != m_diversion_func)
    {
        m_diversion_func();
    }
    else
    {
        cmd_process();
    }

    return;
} /* cmd_task */


/*******************************************************************************
 *          PRIVATE FUNCTION DEFINITIONS
 ******************************************************************************/

/**
 * @function cmd_process
 * @detail   takes input from the console and dispatches commands
 */
static void cmd_process(void)
{
    static bool obscure = false;    /* obscure arguments to command */
    static bool hasCmd = false;     /* true if the command name has been entered */
    static int  cmdNameLen = 0;     /* length of command name */
    char tok;
    bool processed = false;

    while ((! processed) && (0 == Console.ReceiveByte(&tok)))
    {
        if (m_cmdlen == (CFG_CMD_MAXLEN))
        {
            /* token must be '\r' or '\n' else command too long */
            if (('\r' != tok) && ('\n' != tok))
            {
                printf("\n* [error] command too long; limit is %d bytes.\n* ", (CFG_CMD_MAXLEN));
                m_cmdlen = 0;
            }
        }

        if (('\r' == tok) || ('\n' == tok))
        {
            /* end of line; process any non-empty command */
            if (0 != m_cmdlen)
            {
                printf("\n");   /* start a new terminal line for command output */
                processed = true;
                m_cmdline[m_cmdlen] = 0;
                cmd_parse();
                obscure = false;
                hasCmd = 0;
                cmdNameLen = 0;

                if (0 == m_diversion_func)
                {
                    /* issue a prompt */
                    printf("%s", m_cmdprompt);
                }
            }
        }
        else if ((32 > tok) || (127 == tok))
        {
            /* ASCII control character; ignored if not processed */
            #if defined(CFG_CMD_HISTORY) && (0 < (CFG_CMD_HISTORY))
            m_recall = false;           /* command modified */
            #endif

            if (((TERM_BACK) == tok) || ((TERM_DEL) == tok))
            {
                /* backspace / delete */
                if (0 < m_cmdlen)
                {
                    --m_cmdlen;
                    Console.SendByte(TERM_BACK);

                    /* manage case of changed command */
                    if (m_cmdlen <= cmdNameLen)
                    {
                        cmdNameLen = m_cmdlen;
                        hasCmd = false;
                    }
                }
            }
            else if ((TERM_CANCEL) == tok)
            {
                /* drop the command and issue a new prompt */
                if (0 != m_cmdlen)
                {
                    m_cmdlen = 0;
                    printf("%s", m_cmdprompt);
                    obscure = false;
                    hasCmd = false;
                    cmdNameLen = false;
                }
            }
            #if defined(CFG_CMD_HISTORY) && (0 < (CFG_CMD_HISTORY))
            else if ((TERM_PREV) == tok)
            {
                cmd_history_prev();
            }
            else if ((TERM_NEXT) == tok)
            {
                cmd_history_next();
            }

            if (m_recall)
            {
                /* fill in hasCmd / cmdNameLen to cover case where
                 * user uses backspace to change command.
                 */
                char *cmdTok = m_cmdline;
                hasCmd = true;
                obscure = false;    /* obscured commands are never stored in history */
                cmdNameLen = 0;

                while ((' ' != *cmdTok) && (0 != *cmdTok))
                {
                    ++cmdTok;
                    ++cmdNameLen;
                }
            }
            #endif /* enable history */
        }
        else if (' ' == tok)
        {
            /* process space if it is not at the start */
            if (0 != m_cmdlen)
            {
                Console.SendByte(tok);
                m_cmdline[m_cmdlen] = tok;
                ++m_cmdlen;

                if (!hasCmd)
                {
                    hasCmd = true;

                    for (int i = 0; ((0 != g_serial_commands[i].name) && (0 != *g_serial_commands[i].name)); ++i)
                    {
                        if (!strncmp(g_serial_commands[i].name, m_cmdline, (size_t)cmdNameLen))
                        {
                            /* command was matched; check if arguments are obscured */
                            if (0 != g_serial_commands[i].obscure)
                            {
                                obscure = true;
                            }

                            break;
                        }
                    }
                }
            }
        }
        else
        {
            /* UTF-8 compatible code; echo and put in buffer */
            if (!obscure)
            {
                Console.SendByte(tok);
            }
            else
            {
                Console.SendByte('*');
            }

            m_cmdline[m_cmdlen] = tok;
            ++m_cmdlen;

            #if defined(CFG_CMD_HISTORY) && (0 < (CFG_CMD_HISTORY))
            m_recall = false;
            #endif

            if (!hasCmd)
            {
                ++cmdNameLen;
            }
        }
    }

    return;
} /* cmd_process */

/**
 * @function cmd_parse
 * @detail   Parses the current command line, creates the canonical form,
 *           dispatches the command (if appropriate) and adds it to
 *           the history if it is a new command.
 */
static void cmd_parse(void)
{
    char buff[(CFG_CMD_MAXLEN) + 1];    /* mutable buffer for command arguments */
    bool quote = false;
    bool valid = true;                  /* set false if there are parsing errors */
    int  bufIdx = 0;
    int  argLen = 0;
    char const *cmdTok = m_cmdline;
    char const *argPtr = buff;
    m_argc = 0;
    memset (m_argv, 0, sizeof(m_argv));

    for (int i = 0; (i < m_cmdlen) && (valid); ++i)
    {
        char tok = *cmdTok;
        ++cmdTok;

        if ('"' == tok)
        {
            if (!quote)
            {
                quote = true;
            }
            else
            {
                quote = false;
                argLen = 0;

                if ((0 != *cmdTok) && (' ' != *cmdTok))
                {
                    valid = false;
                    printf("[error] double-quote within a string\n");
                }
                else
                {
                    /* end of quoted string */
                    if (m_argc < (CFG_CMD_MAXARG))
                    {
                        buff[bufIdx] = 0;
                        m_argv[m_argc] = argPtr;
                        argLen = 0;
                        ++bufIdx;
                        ++m_argc;
                    }
                    else
                    {
                        valid = false;
                        printf("[error] too many arguments (max %d)\n", ((CFG_CMD_MAXARG) - 1));
                    }
                }
            }
        }
        else if (' ' == tok)
        {
            if (quote)
            {
                /* part of a quoted string */
                if (0 == argLen)
                {
                    argPtr = &buff[bufIdx];
                }

                buff[bufIdx] = ' ';
                ++bufIdx;
                ++argLen;
            }
            else
            {
                /* end of an argument string */
                if (m_argc < (CFG_CMD_MAXARG))
                {
                    buff[bufIdx] = 0;
                    m_argv[m_argc] = argPtr;
                    argLen = 0;
                    ++bufIdx;
                    ++m_argc;
                }
                else
                {
                    valid = false;
                    printf("[error] too many arguments (max %d)\n", ((CFG_CMD_MAXARG) - 1));
                }
            }
        }
        else
        {
            /* add character to current string */
            if (0 == argLen)
            {
                argPtr = &buff[bufIdx];
            }

            buff[bufIdx] = tok;
            ++bufIdx;
            ++argLen;
        }
    }

    if (quote)
    {
        valid = false;
        printf("[error] unmatched double-quote\n");
    }

    if (valid)
    {
        buff[bufIdx] = 0;

        if (0 != argLen)
        {
            if (m_argc < (CFG_CMD_MAXARG))
            {
                m_argv[m_argc] = argPtr;
                ++m_argc;
            }
            else
            {
                valid = false;
                printf("[error] too many arguments (max %d)\n", ((CFG_CMD_MAXARG) - 1));
            }
        }

        /* look up and dispatch command */
        ConsoleCmd_t const *cmd = 0;

        for (int i = 0; ((0 != g_serial_commands[i].name) && (0 != *g_serial_commands[i].name)); ++i)
        {
            if (!strcmp(g_serial_commands[i].name, buff))
            {
                /* command was matched */
                bool handled = false;   /* true if command was handled */
                cmd = &g_serial_commands[i];

                if ((0 != cmd) && (0 != cmd->condition))
                {
                    int status = cmd->condition();

                    switch (status)
                    {
                    case CLI_NOT_AVAIL:
                    {
                        /* condition function should have printed the reason for the
                         * command not being available
                         */
                        cmd = 0;
                        handled = true;
                        break;
                    }

                    case CLI_ACCESS_OK:
                    {
                        /* command function will be executed later */
                        break;
                    }

                    default:
                    {
                        printf("[bug] unhandled condition: %d\n", status);
                        break;
                    }
                    }
                }

                if (0 != cmd)
                {
                    handled = true;

                    if (0 == cmd->function)
                    {
                        printf("[bug] command handler not specified\n");
                    }
                    else
                    {
                        #if !defined(CFG_CMD_HISTORY) || (1 > (CFG_CMD_HISTORY))
                        cmd->function(m_argc, m_argv);
                        #else
                        /* add to the command history if not obscured, not a recall,
                         * and function returns zero
                         */
                        if ((0 == cmd->function(m_argc, m_argv)) && (0 == cmd->obscure) && (!m_recall))
                        {
                            int idx = m_histBase + m_histSize;

                            if (m_histSize < (CFG_CMD_HISTORY))
                            {
                                ++m_histSize;
                            }
                            else
                            {
                                ++m_histBase;

                                while (m_histBase >= (CFG_CMD_HISTORY))
                                {
                                    m_histBase -= (CFG_CMD_HISTORY);
                                }
                            }

                            while (idx >= (CFG_CMD_HISTORY))
                            {
                                idx -= (CFG_CMD_HISTORY);
                            }

                            /* build normalized command */
                            char *hist = m_history[idx];
                            int   histLen = snprintf(hist, (CFG_CMD_MAXLEN), "%s", m_argv[0]);

                            for (int argIdx = 1; argIdx < m_argc; ++argIdx)
                            {
                                if (histLen < CFG_CMD_MAXLEN)
                                {
                                    /* add a string */
                                    if (0 != strchr(m_argv[argIdx], ' '))
                                    {
                                        histLen += snprintf(
                                                &hist[histLen],
                                                (size_t)(((CFG_CMD_MAXLEN) + 1) - histLen),
                                                " \"%s\"",
                                                m_argv[argIdx]);
                                    }
                                    else
                                    {
                                        histLen += snprintf(
                                                &hist[histLen],
                                                (size_t)(((CFG_CMD_MAXLEN) + 1) - histLen),
                                                " %s",
                                                m_argv[argIdx]);
                                    }
                                }
                            }
                        }

                        m_histIdx = m_histSize;
                        #endif
                    }
                }

                if ((0 == cmd) && (!handled))
                {
                    printf("* command not found\n");
                }

                break;
            }
        }
    }

    m_cmdlen = 0;
    m_cmdline[0] = 0;
    m_argc = 0;
    memset (m_argv, 0, sizeof(m_argv));
    return;
} /* cmd_parse */

#if defined(CFG_CMD_HISTORY) && (0 < (CFG_CMD_HISTORY))
static void cmd_history_prev(void)
{
    if (0 < m_histSize)
    {
        if (0 == m_histIdx)
        {
            m_histIdx = m_histSize - 1;
        }
        else
        {
            --m_histIdx;
        }

        /* clear current cmdline */
        for (int i = 0; i < m_cmdlen; ++i)
        {
            Console.SendByte(0x08);
        }

        /* display current indexed command */
        int idx = m_histBase + m_histIdx;

        while (idx >= (CFG_CMD_HISTORY))
        {
            idx -= (CFG_CMD_HISTORY);
        }

        m_cmdlen = snprintf(m_cmdline, ((CFG_CMD_MAXLEN) + 1), "%s", m_history[idx]);
        printf("%s", m_cmdline);
    }

    return;
} /* cmd_history_prev */

static void cmd_history_next(void)
{
    if (0 < m_histSize)
    {
        ++m_histIdx;

        if (m_histIdx >= m_histSize)
        {
            m_histIdx = 0;
        }

        /* clear current cmdline */
        for (int i = 0; i < m_cmdlen; ++i)
        {
            Console.SendByte(0x08);
        }

        /* display current indexed command */
        int idx = m_histBase + m_histIdx;

        while (idx >= (CFG_CMD_HISTORY))
        {
            idx -= (CFG_CMD_HISTORY);
        }

        m_cmdlen = snprintf(m_cmdline, ((CFG_CMD_MAXLEN) + 1), "%s", m_history[idx]);
        printf("%s", m_cmdline);
    }

    return;
} /* cmd_history_next */
#endif /* enable history */
