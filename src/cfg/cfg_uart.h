/**
 * File : cfg_uart.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Implements the generic PIC32M UART driver.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef CFG_UART_H
#define CFG_UART_H

#define CFG_UART1_ENABLE    (0)     /* 0 = disable, 1 = enable */
#define CFG_UART1_TXBUF     (0)     /* 0 = no Tx buffer, 1 = use Tx buffer */
#define CFG_UART1_RXBUF     (0)     /* 0 = no Rx buffer, 1 = use Rx buffer */

/* Debug UART: requires Tx and Rx buffers */
#define CFG_UART2_ENABLE    (1)     /* 0 = disable, 1 = enable */
#define CFG_UART2_TXBUF     (1)     /* 0 = no Tx buffer, 1 = use Tx buffer */
#define CFG_UART2_RXBUF     (1)     /* 0 = no Rx buffer, 1 = use Rx buffer */

#define CFG_UART3_ENABLE    (0)     /* 0 = disable, 1 = enable */
#define CFG_UART3_TXBUF     (0)     /* 0 = no Tx buffer, 1 = use Tx buffer */
#define CFG_UART3_RXBUF     (0)     /* 0 = no Rx buffer, 1 = use Rx buffer */

#define CFG_UART4_ENABLE    (0)     /* 0 = disable, 1 = enable */
#define CFG_UART4_TXBUF     (0)     /* 0 = no Tx buffer, 1 = use Tx buffer */
#define CFG_UART4_RXBUF     (0)     /* 0 = no Rx buffer, 1 = use Rx buffer */

#define CFG_UART5_ENABLE    (0)     /* 0 = disable, 1 = enable */
#define CFG_UART5_TXBUF     (0)     /* 0 = no Tx buffer, 1 = use Tx buffer */
#define CFG_UART5_RXBUF     (0)     /* 0 = no Rx buffer, 1 = use Rx buffer */

#define CFG_UART6_ENABLE    (0)     /* 0 = disable, 1 = enable */
#define CFG_UART6_TXBUF     (0)     /* 0 = no Tx buffer, 1 = use Tx buffer */
#define CFG_UART6_RXBUF     (0)     /* 0 = no Rx buffer, 1 = use Rx buffer */

#endif /* CFG_UART_H */
