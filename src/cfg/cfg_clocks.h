/**
 * File : cfg_clocks.h
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : configuration file to define clock source frequencies and
 *               instantiate PBClk drivers.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#ifndef CFG_CLOCKS_H
#define CFG_CLOCKS_H

/* Clock Inputs */
#define CFG_POSC_FREQ       (24000000UL)            /* Primary Oscillator Frequency */
#define CFG_SOSC_FREQ       (0UL)                   /* Secondary Oscillator Frequency */
#define CFG_FRC_FREQ        (8000000UL)             /* Internal Fast RC (8%) */
#define CFG_BFRC_FREQ       (8000000UL)             /* Backup Fast RC (30%) */
#define CFG_LPRC_FREQ       (31250UL)               /* Low-Power RC, 31.25KHz (25%) */
#define CFG_REFCLK1_FREQ    (0UL)                   /* RefClk1 input pin */
#define CFG_REFCLK2_FREQ    (0UL)                   /* RefClk1 input pin */
#define CFG_REFCLK3_FREQ    (0UL)                   /* RefClk1 input pin */
#define CFG_REFCLK4_FREQ    (0UL)                   /* RefClk1 input pin */
#define CFG_REFCLK5_FREQ    (0UL)                   /* RefClk1 input pin */

/* Peripheral Block Clocks, set value to 1 to instantiate the PBClk driver */
#define CFG_USE_PBCLK_1         (1)
#define CFG_USE_PBCLK_2         (1)
#define CFG_USE_PBCLK_3         (1)
#define CFG_USE_PBCLK_4         (1)
#define CFG_USE_PBCLK_5         (1)
#define CFG_USE_PBCLK_6         (1)
#define CFG_USE_PBCLK_7         (0) /* PBClk7 is the system clock; do not expose it */

#endif /* CFG_CLOCKS_H */
