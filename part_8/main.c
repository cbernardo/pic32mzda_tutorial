/**
 * File : main.c
 * Copyright (C) 2021 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Runs a few test cases for the 4096 byte Circular Buffer
 * which will be used for various I/O buffers in subsequent projects.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "cbuff.h"
#include "startup.h"
#include "uart_simple.h"

CBuff_t  myBuffer;
uint16_t dummyVars[512];
uint8_t  retrieved[1024];
int32_t  numTests;
int32_t  numPassed;

void TestBuf(int const initTail, int const size, int const expectedFree);

void TestBuf(int const initTail, int const size, int const expectedFree)
{
    bool contentsOk = true;
    bool indicesOk = false;
    memset(myBuffer.data, 0, 1024);
    myBuffer.head = initTail;
    myBuffer.tail = initTail;
    cbuf_PushString(&myBuffer, dummyVars, size);
    ++numTests;
    int bytesUsed = cbuf_GetUsedBytes(&myBuffer);
    int bytesFree = cbuf_GetUnusedBytes(&myBuffer);
    printf("    (tail: %d), (head: %d), (used: %d), (free: %d) ",
            myBuffer.tail, myBuffer.head, bytesUsed, bytesFree);

    if ((myBuffer.tail == ((myBuffer.head - size) & myBuffer.mask))
    && (bytesUsed == size) && (bytesFree == (myBuffer.mask - size))
    && (bytesFree == expectedFree))
    {
        indicesOk = true;
    }

    /* destructive retrieval and comparison of buffer data */
    if (0 < size)
    {
        int length = 0;
        memset(retrieved, 0, sizeof(retrieved));
        cbuf_PopString(&myBuffer, retrieved, size, &length);

        if (size != length)
        {
            contentsOk = false;
            printf("<size: %d vs %d>", size, length);
        }
        
        if (0 != memcmp(dummyVars, retrieved, (size_t)size))
        {
            contentsOk = false;
            printf("<content mismatch>");
        }
    }

    if ((indicesOk) & (contentsOk))
    {
        ++numPassed;
        printf("[PASS]\n");
    }
    else
    {
        printf("[FAIL]\n");
    }

    return;
}

int main(void)
{
    _on_start();    /* complete microprocessor init */
    uart_init();    /* set up the UART for use by the printf() function */
    printf("\nDemonstration: circular buffer.\n");
    cbuf_Init(&myBuffer);
    numTests = 0;
    numPassed = 0;

    for (int val = 0; val < 512; ++val)
    {
        dummyVars[val] = (uint16_t)(val + 1);
    }

    printf("* Test set 1, initial tail = 0\n");
    printf("  + empty buffer, expect head == tail, 0 used, 1023 free.\n");
    TestBuf(0, 0, 1023);
    printf("  + single byte, expect head == (tail + 1) & 1023, 1 used, 1022 free.\n");
    TestBuf(0, 1, 1022);
    printf("  + 1022 bytes, expect head == (tail - 2) & 1023, 1022 used, 1 free.\n");
    TestBuf(0, 1022, 1);
    printf("  + full buffer, expect head == (tail - 1) & 1023, 1023 used, 0 free.\n");
    TestBuf(0, 1023, 0);

    printf("* Test set 2, initial tail = 1\n");
    printf("  + empty buffer, expect head == tail, 0 used, 1023 free.\n");
    TestBuf(1, 0, 1023);
    printf("  + single byte, expect head == (tail + 1) & 1023, 1 used, 1022 free.\n");
    TestBuf(1, 1, 1022);
    printf("  + 1022 bytes, expect head == (tail - 2) & 1023, 1022 used, 1 free.\n");
    TestBuf(1, 1022, 1);
    printf("  + full buffer, expect head == (tail - 1) & 1023, 1023 used, 0 free.\n");
    TestBuf(1, 1023, 0);

    printf("* Test set 3, initial tail = 511\n");
    printf("  + empty buffer, expect head == tail, 0 used, 1023 free.\n");
    TestBuf(511, 0, 1023);
    printf("  + single byte, expect head == (tail + 1) & 1023, 1 used, 1022 free.\n");
    TestBuf(511, 1, 1022);
    printf("  + 1022 bytes, expect head == (tail - 2) & 1023, 1022 used, 1 free.\n");
    TestBuf(511, 1022, 1);
    printf("  + full buffer, expect head == (tail - 1) & 1023, 1023 used, 0 free.\n");
    TestBuf(511, 1023, 0);

    printf("* Test set 4, initial tail = 1022\n");
    printf("  + empty buffer, expect head == tail, 0 used, 1023 free.\n");
    TestBuf(1022, 0, 1023);
    printf("  + single byte, expect head == (tail + 1) & 1023, 1 used, 1022 free.\n");
    TestBuf(1022, 1, 1022);
    printf("  + 1022 bytes, expect head == (tail - 2) & 1023, 1022 used, 1 free.\n");
    TestBuf(1022, 1022, 1);
    printf("  + full buffer, expect head == (tail - 1) & 1023, 1023 used, 0 free.\n");
    TestBuf(1022, 1023, 0);

    printf("* Test set 5, initial tail = 1023\n");
    printf("  + empty buffer, expect head == tail, 0 used, 1023 free.\n");
    TestBuf(1023, 0, 1023);
    printf("  + single byte, expect head == (tail + 1) & 1023, 1 used, 1022 free.\n");
    TestBuf(1023, 1, 1022);
    printf("  + 1022 bytes, expect head == (tail - 2) & 1023, 1022 used, 1 free.\n");
    TestBuf(1023, 1022, 1);
    printf("  + full buffer, expect head == (tail - 1) & 1023, 1023 used, 0 free.\n");
    TestBuf(1023, 1023, 0);

    printf("** SUMMARY:  Passed %d of %d tests.\n", numPassed, numTests);

    while(1)
    {
        static uint32_t counter = 0;
        ++counter;

        if (5000000 < counter)
        {
            counter = 0;
            LATHINV = (1UL << 11);
        }
    }

    __builtin_unreachable();
    return 0;
}
