/**
 * File : main.c
 * Copyright (C) 2020 Cirilo Bernardo <cirilo.bernardo@gmail.com>
 * Description : Implements a simple UART driver to demonstrate
 * how printf() can be supported. Occasionally toggles MEB2 LED5
 * and sends "." on the UART via printf().
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <xc.h>
#include <stdint.h>
#include <stdio.h>

#include "startup.h"
#include "uart_simple.h"

int main(void)
{
    _on_start();    /* complete microprocessor init */
    uart_init();    /* set up the UART for use by the printf() function */
    printf("* This demonstrates the use of the %s function.\n", "printf()");

    do
    {
        int32_t i;

        for (i = 0; i < 50000000; ++i)
        {
            ; /* waste time */
        }

        LATHINV = (1UL << 11);  /* toggle LED 5 */
        printf(".");
    } while (1);

    while(1)
    {
        ; /* do nothing */
    }

    __builtin_unreachable();
    return 0;
}
